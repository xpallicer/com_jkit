/**
 * @package     JKit
 * @subpackage  com_jkit
 * @copyright   Copyright (C) 2013 - 2014 CloudHotelier. All rights reserved.
 * @license     GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 * @link        http://www.cloudjkitier.com
 * @author      Xavier Pallicer <xpallicer@gmail.com>
 */

/*
 * JKit JS 
 */

(function ($, window, document) {

    // JKit scope
    var JKit = {
        // init script
        init: function () {

            // screen
            this.desktop = $(window).width() > 979;
            this.tablet = $(window).width() <= 979;
            this.phone = $(window).width() < 768;

            // common tasks
            this.common();

            if ($('#com-jkit-gallery').length) {
                this.gallery();
            }
        }

        // common tasks
        , common: function () {

            var self = this;

            // overlay
            if (self.desktop) {
                $('.jkit-overlay').hover(function () {
                    $(this).children('.jkit-overlay-top').fadeIn(250);
                }, function () {
                    $(this).children('.jkit-overlay-top').fadeOut(250);
                });
            }

            // fancybox
            if ($('.jkit-fancybox').length) {
                $('.jkit-fancybox').fancybox();
            }

            // fancybox media
            if ($('.jkit-fancybox-media').length) {
                $('.jkit-fancybox-media').fancybox({
                    openEffect: 'none',
                    closeEffect: 'none',
                    helpers: {
                        media: {}
                    }
                });
            }

            // imagelightbox
            self.imagelightbox();
        }

        // imagelightbox

        , imagelightbox: function (selector) {

            var selector = selector || false;

            // get default selectors
            if (!selector) {
                var instances = ['.jkit-imagelightbox'];
                var last_instance = '';
                var $selectors = $('[data-jkit-imagelightbox]');
                if ($selectors.length) {
                    $selectors.each(function (i, el) {
                        var name = $(el).data('jkit-imagelightbox');
                        if (name !== last_instance) {
                            instances.push('[data-jkit-imagelightbox="' + name + '"]');
                            last_instance = name;
                        }
                    });
                }
            } else {
                instances = [selector];
            }

            // launch instances
            $(instances).each(function (i, selector) {
                var instance = $(selector).imageLightbox({
                    onStart: function () {
                        overlayOn();
                        closeButtonOn(instance);
                        arrowsOn(instance, selector);
                    },
                    onEnd: function () {
                        overlayOff();
                        captionOff();
                        closeButtonOff();
                        arrowsOff();
                        activityIndicatorOff();
                    },
                    onLoadStart: function () {
                        captionOff();
                        activityIndicatorOn();
                    },
                    onLoadEnd: function () {
                        captionOn();
                        activityIndicatorOff();
                        $('.imagelightbox-arrow').css('display', 'block');
                    }
                });
            });

            // layout helpers
            var activityIndicatorOn = function ()
            {
                $('<div id="imagelightbox-loading"><div></div></div>').appendTo('body');
            }
            , activityIndicatorOff = function ()
            {
                $('#imagelightbox-loading').remove();
            }
            , overlayOn = function ()
            {
                $('<div id="imagelightbox-overlay"></div>').appendTo('body');
            }
            , overlayOff = function ()
            {
                $('#imagelightbox-overlay').remove();
            }
            , closeButtonOn = function (instance)
            {
                $('<button type="button" id="imagelightbox-close" title="Close"></button>').appendTo('body').on('click touchend', function () {
                    $(this).remove();
                    instance.quitImageLightbox();
                    return false;
                });
            }
            , closeButtonOff = function ()
            {
                $('#imagelightbox-close').remove();
            }
            , captionOn = function ()
            {
                var description = $('a[href="' + $('#imagelightbox').attr('src') + '"]').attr('title') || false;
                if (description && description.length > 0)
                    $('<div id="imagelightbox-caption">' + description + '</div>').appendTo('body');
            }
            , captionOff = function ()
            {
                $('#imagelightbox-caption').remove();
            }
            , navigationOn = function (instance, selector)
            {
                var images = $(selector);
                if (images.length)
                {
                    var nav = $('<div id="imagelightbox-nav"></div>');
                    for (var i = 0; i < images.length; i++)
                        nav.append('<button type="button"></button>');

                    nav.appendTo('body');
                    nav.on('click touchend', function () {
                        return false;
                    });

                    var navItems = nav.find('button');
                    navItems.on('click touchend', function ()
                    {
                        var $this = $(this);
                        if (images.eq($this.index()).attr('href') !== $('#imagelightbox').attr('src'))
                            instance.switchImageLightbox($this.index());

                        navItems.removeClass('active');
                        navItems.eq($this.index()).addClass('active');

                        return false;
                    })
                            .on('touchend', function () {
                                return false;
                            });
                }
            }
            , navigationUpdate = function (selector)
            {
                var items = $('#imagelightbox-nav button');
                items.removeClass('active');
                items.eq($(selector).filter('[href="' + $('#imagelightbox').attr('src') + '"]').index(selector)).addClass('active');
            }
            , navigationOff = function ()
            {
                $('#imagelightbox-nav').remove();
            }
            , arrowsOn = function (instance, selector)
            {
                var $arrows = $('<button type="button" class="imagelightbox-arrow imagelightbox-arrow-left"></button><button type="button" class="imagelightbox-arrow imagelightbox-arrow-right"></button>');

                $arrows.appendTo('body');

                $arrows.on('click touchend', function (e)
                {
                    e.preventDefault();

                    var $this = $(this),
                            $target = $(selector + '[href="' + $('#imagelightbox').attr('src') + '"]'),
                            index = $target.index(selector);

                    if ($this.hasClass('imagelightbox-arrow-left'))
                    {
                        index = index - 1;
                        if (!$(selector).eq(index).length)
                            index = $(selector).length;
                    }
                    else
                    {
                        index = index + 1;
                        if (!$(selector).eq(index).length)
                            index = 0;
                    }

                    instance.switchImageLightbox(index);
                    return false;
                });
            }
            , arrowsOff = function ()
            {
                $('.imagelightbox-arrow').remove();
            };

        }


        // gallery view
        , gallery: function () {

            var self = this;

            // launch default imagelightbox
            self.gallery_imagelightbox = '.jkit-gallery-imagelightbox';
            self.imagelightbox('.jkit-gallery-imagelightbox');

            // prepare isotope vars
            var $container = $('.jkit-gallery-images');
            var filters = {};

            // launch isotope
            $container.isotope({
                itemSelector: '.jkit-gallery-image',
                layoutMode: 'fitRows'
            });

            // filter buttons
            $('.option-set a').click(function () {

                var $this = $(this);

                // don't proceed if already active
                if ($this.hasClass('active')) {
                    return;
                }

                var $optionSet = $this.parents('.option-set');

                // change active class
                $optionSet.find('.active').removeClass('active');
                $this.addClass('active');

                // store filter value in object
                var group = $optionSet.attr('data-filter-group');
                filters[group] = $this.attr('data-filter-value');

                // update lightbox
                $(document).off('click', self.gallery_imagelightbox);
                self.gallery_imagelightbox = filters[group].length ? filters[group] + ' .jkit-gallery-imagelightbox' : '.jkit-gallery-imagelightbox';
                self.imagelightbox(self.gallery_imagelightbox);

                // convert object into array
                var isoFilters = [];
                for (var prop in filters) {
                    isoFilters.push(filters[ prop ]);
                }
                var selector = isoFilters.join('');
                $container.isotope({filter: selector});

                return false;
            });
        }

    };

    // init on ready
    $(function () {
        JKit.init();
    });

}(window.jQuery, window, document));
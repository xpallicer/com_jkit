<?php
/**
 * @package     JKit
 * @subpackage  com_jkit
 * @copyright   Copyright (C) 2013 - 2014 CloudHotelier. All rights reserved.
 * @license     GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 * @link        http://www.cloudhotelier.com
 * @author      Xavier Pallicer <xpallicer@gmail.com>
 */
// no direct access
defined('_JEXEC') or die;

// behavior
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.keepalive');

// prepare params
$params = $this->form->getFieldset('params');
?>

<?php if ($this->item->id && $this->item->id < 100): ?>
    <div class="alert alert-block"><?php echo JText::_('COM_JKIT_TYPE_ERROR_DEFAULT');?></div>
<?php endif; ?>

<!-- adminForm -->
<form action="<?php echo JRoute::_('index.php?option=com_jkit&view=type&layout=edit&id=' . (int) $this->item->id); ?>" method="post" name="adminForm" id="adminForm" class="form-validate" enctype="multipart/form-data">

    <!-- form fields -->
    <input type="hidden" name="task" value="" />
    <?php echo JHtml::_('form.token'); ?>

    <!-- main -->
    <div class="row-fluid" id="jkit-view-item">

        <div class="span9">

            <!-- info -->
            <div class="form-horizontal">
                <fieldset>

                    <?php echo $this->form->getControlGroup('title'); ?>

                    <?php
                    foreach (array('intro', 'body', 'link', 'video', 'attachment', 'event', 'pricing', 'quote', 'contact', 'image', 'tags', 'data') as $field) {
                        echo JKitHelper::getControlGroup($params, $field);
                    }
                    ?>
                </fieldset>
            </div>

        </div>

        <!-- sidebar -->
        <div class="span3">

            <!-- publishing -->
            <div class="well">
                <?php echo $this->form->getControlGroup('id'); ?>
            </div>

        </div>
        <!-- /sidebar -->

    </div>
    <!-- /main -->

</form>
<!-- /adminForm -->

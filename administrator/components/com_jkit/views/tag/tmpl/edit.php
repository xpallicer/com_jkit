<?php
/**
 * @package     JKit
 * @subpackage  com_jkit
 * @copyright   Copyright (C) 2013 - 2014 CloudHotelier. All rights reserved.
 * @license     GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 * @link        http://www.cloudhotelier.com
 * @author      Xavier Pallicer <xpallicer@gmail.com>
 */
// no direct access
defined('_JEXEC') or die;

// behavior
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.keepalive');

$api_key = $this->params->get('api_key', '');
?>

<input type="hidden" id="translate_key" value="<?php echo $api_key; ?>" />
<input type="hidden" id="translate_id" value="" />

<!-- adminForm -->
<form action="<?php echo JRoute::_('index.php?option=com_jkit&view=tag&layout=edit&id=' . (int) $this->item->id); ?>" method="post" name="adminForm" id="adminForm" class="form-validate" enctype="multipart/form-data">

    <!-- form fields -->
    <input type="hidden" name="task" value="" />
    <?php echo JHtml::_('form.token'); ?>

    <!-- main -->
    <div class="row-fluid" id="jkit-view-tag">

        <div class="span9">

            <!-- info -->
            <div class="form-horizontal" id="jkit-info" >
                <fieldset>

                    <!-- title -->
                    <?php echo $this->form->getControlGroup('title'); ?>

                    <!-- translations -->
                    <?php foreach ($this->lists->langs as $lang): ?>
                        <?php
                        $hide = $lang->lang_code == $this->item->language || in_array($this->item->language, array('', '*')) ? 'hide' : '';
                        $title = isset($this->lists->translations[$lang->lang_code]) ? $this->lists->translations[$lang->lang_code]->title : '';
                        ?>
                        <div class="control-group jkit-lang-version <?php echo $hide; ?>" id="jkit-lang-<?php echo $lang->lang_code; ?>">
                            <label class="control-label hasTip" for="title_<?php echo $lang->lang_code; ?>" title="<?php echo JText::sprintf('COM_JKIT_TAG_TRANSLATION_TITLE', $lang->title); ?>::<?php echo JText::_('COM_JKIT_TAG_TRANSLATION_DESC'); ?>">
                                <?php echo JText::sprintf('COM_JKIT_TAG_FIELDSET_TRANSLATION', $lang->title); ?>
                            </label>
                            <div class="controls">
                                <input type="text" name="translation_title_<?php echo $lang->lang_code; ?>" id="title_<?php echo $lang->lang_code; ?>" value="<?php echo $title; ?>" class="input-xlarge" size="60">
                                <span class="help-inline">
                                    <a href="javascript:;" class="btn btn-mini copy_tag" rel="title_<?php echo $lang->lang_code; ?>"><?php echo JText::_('COM_JKIT_TRANSLATION_COPY'); ?></a>
                                    <?php if ($api_key) : ?><a href="javascript:;" class="btn btn-mini translate_tag" rel="title_<?php echo $lang->lang_code; ?>" data-original="title" data-lang="<?php echo substr($lang->lang_code, 0, 2); ?>"><?php echo JText::_('COM_JKIT_TRANSLATION_TRANSLATE'); ?></a><?php endif; ?>
                                </span>
                            </div>
                        </div>
                    <?php endforeach; ?>    

                </fieldset>
            </div>

        </div>

        <!-- sidebar -->
        <div class="span3">

            <!-- publishing -->
            <div class="well">
                <h3><?php echo JText::_('COM_JKIT_ITEM_FIELDSET_PUBLISH') ?></h3>
                <?php foreach (array('state', 'language', 'id') as $field): ?>
                    <?php echo $this->form->getControlGroup($field); ?>
                <?php endforeach; ?>
            </div>

        </div>
        <!-- /sidebar -->

    </div>
    <!-- /main -->

</form>
<!-- /adminForm -->

<?php

/**
 * @package     JKit
 * @subpackage  com_jkit
 * @copyright   Copyright (C) 2013 - 2014 CloudHotelier. All rights reserved.
 * @license     GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 * @link        http://www.cloudhotelier.com
 * @author      Xavier Pallicer <xpallicer@gmail.com>
 */
// no direct access
defined('_JEXEC') or die;

/**
 * Tag View
 */
class JKitViewTag extends JViewLegacy {

    /**
     * Display the view
     */
    public function display($tpl = null) {

        // get the data
        $this->form = $this->get('Form');
        $this->item = $this->get('Item');
        $this->state = $this->get('State');
        $this->params = JComponentHelper::getParams('com_jkit');

        // lists
        $this->lists = new stdClass();
        $this->lists->langs = JKitHelperLangs::getLangs();
        $this->lists->translations = array();

        // get tag translations
        if (isset($this->item->id)) {
            $translations = JKitHelperLangs::getTranslations(array($this->item), 'tags');
            if (count($translations)) {
                foreach ($translations as $translation) {
                    $this->lists->translations[$translation->lang] = new stdClass();
                    $this->lists->translations[$translation->lang]->title = $translation->title;
                    $this->lists->translations[$translation->lang]->alias = $translation->alias;
                }
            }
        }

        // create the toolbar
        JKitHelper::getToolbar(false, $this->item->id, $this->item->title);

        // display the view template
        parent::display($tpl);
    }

}

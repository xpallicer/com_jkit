<?php
/**
 * @package     JKit
 * @subpackage  com_jkit
 * @copyright   Copyright (C) 2013 - 2014 CloudHotelier. All rights reserved.
 * @license     GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 * @link        http://www.cloudhotelier.com
 * @author      Xavier Pallicer <xpallicer@gmail.com>
 */
// no direct access
defined('_JEXEC') or die;

// chosen
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.keepalive');

// prepare params
$params = $this->form->getFieldset('params');

//tags
$tags = array();
if (isset($this->item->params['tags'])) {
    $tags = $this->item->params['tags'];
}
?>

<!-- adminForm -->
<form action="<?php echo JRoute::_('index.php?option=com_jkit&view=image&layout=edit&id=' . (int) $this->item->id); ?>" method="post" name="adminForm" id="adminForm" class="form-validate" enctype="multipart/form-data">

    <!-- form fields -->
    <input type="hidden" name="task" value="" />
    <?php echo JHtml::_('form.token'); ?>

    <!-- main -->
    <div class="row-fluid">

        <div class="span9">

            <!-- info -->
            <div class="form-horizontal" id="jkit-info" >
                <fieldset>
                    <?php foreach (array('title', 'alias', 'info') as $field): ?>
                        <?php echo $this->form->getControlGroup($field); ?>
                    <?php endforeach; ?>
                </fieldset>
            </div>

            <!-- link --> 
            <div class="form-horizontal" id="jkit-link">
                <fieldset>
                    <legend><?php echo JText::_('COM_JKIT_ITEM_FIELDSET_LINK') ?></legend>
                    <?php
                    foreach (array('link', 'link_text', 'link_target') as $field) {
                        echo JKitHelper::getControlGroup($params, $field);
                    }
                    ?>
                </fieldset>
            </div>

            <!-- tags --> 
            <?php if (count($this->tags)): ?>
                <div class="form-inline" id="jkit-tags">
                    <fieldset>
                        <legend><?php echo JText::_('COM_JKIT_ITEM_FIELDSET_TAGS') ?></legend>
                        <?php foreach ($this->tags as $tag): ?>
                            <?php $checked = isset($tags[$tag->id]) ? 'checked="checked"' : ''; ?>
                            <div class="control-group">
                                <label class="checkbox">
                                    <input type="checkbox" <?php echo $checked; ?> name="jform[params][tags][<?php echo $tag->id; ?>]" id="jform_tag_<?php echo $tag->id; ?>" value="1">
                                    <?php echo $tag->title; ?>
                                </label>
                            </div>
                        <?php endforeach; ?>
                    </fieldset>
                </div>
            <?php endif; ?>

        </div>

        <!-- sidebar -->
        <div class="span3">

            <!-- image -->
            <div class="well">
                <h3><?php echo JText::_('COM_JKIT_ITEM_FIELDSET_IMAGE') ?></h3>
                <div class="control-group muted">
                    <?php echo JText::sprintf('COM_JKIT_ANY_IMAGE_DESC', $this->params->get('img_upload', 1500)); ?>
                </div>
                <?php if ($this->image): ?>
                    <div class="control-group">
                        <?php echo Jhtml::image(JURI::root() . 'images/jkit/images/' . $this->item->id . '-small.jpg?rand=' . rand(1, 999), $this->item->title, 'class="thumbnail"'); ?>
                    </div>
                    <div class="control-group">
                        <label class="checkbox hasTooltip" title="<?php echo JText::_('COM_JKIT_ANY_IMAGE_DELETE'); ?>::<?php echo JText::_('COM_JKIT_ANY_IMAGE_DELETE_DESC'); ?>">
                            <input type="checkbox" name="image_delete" value="1"><?php echo JText::_('COM_JKIT_ANY_IMAGE_DELETE'); ?>
                        </label>
                    </div>
                <?php endif; ?>
                <div class="control-group">
                    <input type="file" name="image" id="jkit-image" value="" class="">
                </div>
            </div>

            <!-- publishing -->
            <div class="well">
                <h3><?php echo JText::_('COM_JKIT_ITEM_FIELDSET_PUBLISH') ?></h3>
                <?php foreach (array('state', 'item', 'item_id', 'created', 'id') as $field): ?>
                    <?php echo $this->form->getControlGroup($field); ?>
                <?php endforeach; ?>
            </div>

        </div>
        <!-- /sidebar -->

    </div>
    <!-- /main -->

</form>
<!-- /adminForm -->

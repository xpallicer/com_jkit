<?php
/**
 * @package     JKit
 * @subpackage  com_jkit
 * @copyright   Copyright (C) 2013 - 2014 CloudHotelier. All rights reserved.
 * @license     GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 * @link        http://www.cloudhotelier.com
 * @author      Xavier Pallicer <xpallicer@gmail.com>
 */
// no direct access
defined('_JEXEC') or die;

JHtml::_('formbehavior.chosen', 'select');

$context = 'items';
$listOrder = $this->escape($this->state->get('list.ordering'));
$listDirn = $this->escape($this->state->get('list.direction'));
$saveOrder = $listOrder == 'a.ordering';

if ($saveOrder) {
    JHtml::_('sortablelist.sortable', 'articleList', 'adminForm', strtolower($listDirn), 'index.php?option=com_jkit&task=' . $context . '.saveOrderAjax&tmpl=component');
}

$archived = $this->state->get('filter.state') == 2 ? true : false;
$trashed = $this->state->get('filter.state') == -2 ? true : false;

$imageHelper = new JKitHelperImage(JComponentHelper::getParams('com_jkit'));
$image_path = JURI::root() . "/images/jkit/$context/";
$types = JKitHelper::defaultTypes();
?>

<!-- adminForm -->
<form action="<?php echo JRoute::_('index.php?option=com_jkit&view=' . $context); ?>" method="post" name="adminForm" id="adminForm">

    <!-- sidebar -->
    <div id="j-sidebar-container" class="span2">
        <?php echo $this->sidebar; ?>
    </div>
    <!-- /sidebar -->

    <!-- main -->
    <div id="j-main-container" class="span10">

        <!-- search tools -->
        <?php echo JLayoutHelper::render('joomla.searchtools.default', array('view' => $this)); ?>

        <!-- no items -->
        <?php if (empty($this->items)) : ?>
            <div class="alert alert-no-items">
                <?php echo JText::_('JGLOBAL_NO_MATCHING_RESULTS'); ?>
            </div>
        <?php endif; ?>
        <!-- /no items -->

        <!-- items table -->
        <?php if (!empty($this->items)) : ?>

            <table class="table table-striped" id="articleList">

                <thead>
                    <tr>
                        <th width="1%" class="nowrap hidden-phone">
                            <?php echo JHtml::_('searchtools.sort', '', 'a.ordering', $listDirn, $listOrder, null, 'asc', 'JGRID_HEADING_ORDERING', 'icon-menu-2'); ?>
                        </th>
                        <th width="1%" class="nowrap hidden-phone">
                            <?php echo JHtml::_('grid.checkall'); ?>
                        </th> 
                        <th class="nowrap" width="1%" style="min-width:55px">
                            <?php echo JHtml::_('searchtools.sort', 'JSTATUS', 'a.state', $listDirn, $listOrder); ?>
                        </th>
                        <th class="nowrap center" width="8%">
                            <?php echo JHtml::_('searchtools.sort', 'COM_JKIT_ANY_CREATED', 'a.created', $listDirn, $listOrder); ?>
                        </th>
                        <th class="nowrap">
                            <?php echo JHtml::_('searchtools.sort', 'COM_JKIT_ITEM_TITLE', 'a.title', $listDirn, $listOrder); ?>
                        </th>

                        <th class="nowrap center" width="8%">
                            <?php echo JText::_('COM_JKIT_ITEMS_IMAGES'); ?>
                        </th>   
                        <th class="nowrap center" width="8%">
                            <?php echo JHtml::_('searchtools.sort', 'COM_JKIT_ANY_LANGUAGE', 'l.title', $listDirn, $listOrder); ?>
                        </th> 

                        <?php echo JKitHelperLangs::listHeader($this->lists->langs); ?>

                        <th class="nowrap center">
                            <?php echo JHtml::_('searchtools.sort', 'COM_JKIT_ANY_USER', 'u.title', $listDirn, $listOrder); ?>
                        </th>      
                        <th class="nowrap center" width="8%">
                            <?php echo JHtml::_('searchtools.sort', 'COM_JKIT_ANY_EDITED', 'a.modified', $listDirn, $listOrder); ?>
                        </th>
                        <th class="nowrap center" width="1%">
                            <?php echo JHtml::_('searchtools.sort', 'JGRID_HEADING_ID', 'a.id', $listDirn, $listOrder); ?>
                        </th>
                    </tr>
                </thead>

                <tfoot>
                    <tr>
                        <td colspan="30">
                            <?php echo $this->pagination->getListFooter(); ?>
                        </td>
                    </tr>
                </tfoot>

                <tbody>

                    <?php foreach ($this->items as $i => $item) : ?>

                        <?php
                        $canChange = true;
                        $canCheckin = true;
                        $images = $item->images > 0 ? '1' : '0';
                        $link_images = JRoute::_('index.php?option=com_jkit&view=images&filter[item]=' . $item->id . '&filter[catid]=' . $item->catid . '&filter[user]=&filter[state]=');
                        ?>

                        <tr class="row<?php echo $i % 2; ?>" sortable-group-id="<?php echo $item->catid; ?>">

                            <td class="order nowrap center hidden-phone">
                                <?php
                                $iconClass = $saveOrder ? '' : ' inactive tip-top hasTooltip" title="' . JHtml::tooltipText('JORDERINGDISABLED');
                                ?>
                                <span class="sortable-handler <?php echo $iconClass ?>"><i class="icon-menu"></i></span>
                                <?php if ($saveOrder) : ?>
                                    <input type="text" style="display:none" name="order[]" size="5" value="<?php echo $item->ordering; ?>" class="width-20 text-area-order " />
                                <?php endif; ?>
                            </td>

                            <td class="nowrap center hidden-phone">
                                <?php echo JHtml::_('grid.id', $i, $item->id); ?>
                            </td>

                            <td class="nowrap center">
                                <div class="btn-group">
                                    <?php echo JHtml::_('jgrid.published', $item->state, $i, $context . '.', $canChange, 'cb'); ?>
                                    <?php echo JKitHelper::renderFeatured($item->featured, $i); ?>
                                    <?php
                                    // Create dropdown items
                                    $action = $archived ? 'unarchive' : 'archive';
                                    JHtml::_('actionsdropdown.' . $action, 'cb' . $i, $context);

                                    $action = $trashed ? 'untrash' : 'trash';
                                    JHtml::_('actionsdropdown.' . $action, 'cb' . $i, $context);

                                    // Render dropdown list
                                    echo JHtml::_('actionsdropdown.render', $this->escape($item->title));
                                    ?>
                                </div>
                            </td>

                            <td class="nowrap center small">
                                <?php echo JHtml::_('date', $item->created, JText::_('DATE_FORMAT_LC4')); ?>
                            </td>

                            <td class="nowrap has-context">
                                <div class="pull-left">
                                    <?php if ($item->checked_out) : ?>
                                        <?php echo JHtml::_('jgrid.checkedout', $i, $item->editor, $item->checked_out_time, $context . '.', $canCheckin); ?>
                                    <?php endif; ?>
                                    <a href="<?php echo JRoute::_('index.php?option=com_jkit&task=item.edit&id=' . $item->id); ?>">
                                        <?php if ($imageHelper->getImage($item->id, $context)) : ?>
                                            <?php echo '<img class="thumbnail pull-left jkit-images-thumb" src="' . $image_path . $item->id . '-tiny.jpg?' . rand(1, 999) . '" />'; ?>
                                        <?php endif; ?>
                                        <?php echo $this->escape($item->title); ?>
                                    </a>
                                    <span class="small"><?php echo JText::sprintf('JGLOBAL_LIST_ALIAS', $this->escape($item->alias)); ?></span>
                                    <div class="small">
                                        <?php echo JText::_('JCATEGORY') . ": " . $this->escape($item->category); ?>
                                        \ <?php echo JText::_('COM_JKIT_TYPE') . ": " . ($item->type_id > 100 ? $item->type : JText::_('COM_JKIT_TYPE_' . $types[$item->type_id])); ?>
                                    </div>
                                </div>
                            </td>

                            <td class="nowrap center small">
                                <a class="btn btn-mini jkit-item-gallery gallery-<?php echo $images; ?>" href="<?php echo $link_images; ?>">
                                    <i class="icon-image"></i> <?php echo JText::_('COM_JKIT_ITEMS_IMAGES_VIEW'); ?>
                                </a>
                            </td>

                            <td class="nowrap center small">
                                <?php if ($item->language == '*'): ?>
                                    <?php echo JText::alt('JALL', 'language'); ?>
                                <?php else: ?>
                                    <?php echo $item->language_title ? $this->escape($item->language_title) : JText::_('JUNDEFINED'); ?>
                                <?php endif; ?>
                            </td>

                            <!-- languages -->
                            <?php echo JKitHelperLangs::listItem($this->lists->langs, $this->lists->translations, $item); ?>

                            <td class="nowrap center small">
                                <?php echo $item->user ? $item->user : '-'; ?>
                            </td>

                            <td class="nowrap center small">
                                <?php echo JHtml::_('date', $item->modified, JText::_('DATE_FORMAT_LC4') . ' H:m'); ?>
                            </td>

                            <td class="nowrap center small">
                                <?php echo $item->id; ?>
                            </td>

                        </tr>

                    <?php endforeach; ?>

                </tbody>


            </table>
        <?php endif; ?>
        <!-- /items table -->

    </div>
    <!-- /main -->

    <!-- form fields -->
    <input type="hidden" name="task" value="" />
    <input type="hidden" name="boxchecked" value="0" />
    <?php echo JHtml::_('form.token'); ?>
    <!-- /form fields -->

    <!-- batch form -->
    <?php echo $this->loadTemplate('batch'); ?>

</form>
<!-- /adminForm -->
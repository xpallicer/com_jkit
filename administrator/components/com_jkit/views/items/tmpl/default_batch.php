<?php
/**
 * @package     JKit
 * @subpackage  com_jkit
 * @copyright   Copyright (C) 2013 - 2014 CloudHotelier. All rights reserved.
 * @license     GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 * @link        http://www.cloudhotelier.com
 * @author      Xavier Pallicer <xpallicer@gmail.com>
 */
// no direct access
defined('_JEXEC') or die;

JHtml::_('bootstrap.modal', 'collapseModal');
?>

<div class="modal hide fade" id="collapseModal">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&#215;</button>
        <h3><?php echo JText::_('COM_JKIT_BATCH_OPTIONS'); ?></h3>
    </div>
    <div class="modal-body modal-batch">
        <div class="row-fluid">
            <div class="control-group span6">
                <div class="controls">
                    <?php echo JHtml::_('batch.item', 'com_jkit'); ?>
                </div>
            </div>
            <div class="control-group span6">
                <div class="controls">
                    <?php echo JHtml::_('batch.language'); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button id="jkit-items-batch-cancel" class="btn" type="button" data-dismiss="modal"><?php echo JText::_('JCANCEL'); ?></button>
        <button id="jkit-items-batch-submit" class="btn btn-primary" type="button"><?php echo JText::_('JGLOBAL_BATCH_PROCESS'); ?></button>
    </div>
</div>

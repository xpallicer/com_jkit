<?php

/**
 * @package     JKit
 * @subpackage  com_jkit
 * @copyright   Copyright (C) 2013 - 2014 CloudHotelier. All rights reserved.
 * @license     GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 * @link        http://www.cloudhotelier.com
 * @author      Xavier Pallicer <xpallicer@gmail.com>
 */
// no direct access
defined('_JEXEC') or die;

/**
 * Items View
 */
class JKitViewItems extends JViewLegacy {

    /**
     * Display the view
     */
    public function display($tpl = null) {

        // get the data from the model
        $this->items = $this->get('Items');
        $this->pagination = $this->get('Pagination');
        $this->state = $this->get('State');

        // filters
        $this->filterForm = $this->get('FilterForm');
        $this->activeFilters = $this->get('ActiveFilters');

        // lists
        $this->lists = new stdClass();
        $this->lists->langs = JKitHelperLangs::getLangs();
        $this->lists->translations = JKitHelperLangs::getTranslations($this->items);

        // toolbar and sidbar
        if ($this->getLayout() !== 'modal') {
            JKitHelper::getToolbar('items', $this->state->get('filter.state') == -2);
            $this->sidebar = JHtmlSidebar::render();
        }

        // display the view layout
        parent::display($tpl);
    }

}

<?php
/**
 * @package     JKit
 * @subpackage  com_jkit
 * @copyright   Copyright (C) 2013 - 2014 CloudHotelier. All rights reserved.
 * @license     GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 * @link        http://www.cloudhotelier.com
 * @author      Xavier Pallicer <xpallicer@gmail.com>
 */
// no direct access
defined('_JEXEC') or die;

JHtml::_('formbehavior.chosen', 'select');

$context = 'users';
$listOrder = $this->escape($this->state->get('list.ordering'));
$listDirn = $this->escape($this->state->get('list.direction'));
$archived = $this->state->get('filter.state') == 2 ? true : false;
$trashed = $this->state->get('filter.state') == -2 ? true : false;

$imageHelper = new JKitHelperImage(JComponentHelper::getParams('com_jkit'));
$image_path = JURI::root() . "/images/jkit/$context/";
?>

<!-- adminForm -->
<form action="<?php echo JRoute::_('index.php?option=com_jkit&view=' . $context); ?>" method="post" name="adminForm" id="adminForm">

    <!-- sidebar -->
    <div id="j-sidebar-container" class="span2">
        <?php echo $this->sidebar; ?>
    </div>
    <!-- /sidebar -->    

    <!-- main -->
    <div id="j-main-container" class="span10">

        <!-- search tools -->
        <?php echo JLayoutHelper::render('joomla.searchtools.default', array('view' => $this)); ?>

        <!-- no items -->
        <?php if (empty($this->items)) : ?>
            <div class="alert alert-no-items">
                <?php echo JText::_('JGLOBAL_NO_MATCHING_RESULTS'); ?>
            </div>
        <?php endif; ?>
        <!-- /no items -->

        <!-- items table -->
        <?php if (!empty($this->items)) : ?>

            <table class="table table-striped" id="articleList">

                <thead>
                    <tr>
                        <th width="1%" class="nowrap hidden-phone">
                            <?php echo JHtml::_('grid.checkall'); ?>
                        </th> 
                        <th class="nowrap" width="1%" style="min-width:55px">
                            <?php echo JHtml::_('searchtools.sort', 'JSTATUS', 'a.state', $listDirn, $listOrder); ?>
                        </th>
                        <th class="nowrap center" width="8%">
                            <?php echo JHtml::_('searchtools.sort', 'COM_JKIT_ANY_CREATED', 'a.created', $listDirn, $listOrder); ?>
                        </th>
                        <th class="nowrap">
                            <?php echo JHtml::_('searchtools.sort', 'COM_JKIT_USER_NAME', 'a.title', $listDirn, $listOrder); ?>
                        </th>
                        <th class="nowrap">
                            <?php echo JHtml::_('searchtools.sort', 'COM_JKIT_USER_EMAIL', 'b.email', $listDirn, $listOrder); ?>
                        </th>                       
                        <th class="nowrap center" width="8%">
                            <?php echo JHtml::_('searchtools.sort', 'COM_JKIT_USER_LASTVISIT', 'b.lastvisitDate', $listDirn, $listOrder); ?>
                        </th>
                        <th class="nowrap center" width="1%">
                            <?php echo JHtml::_('searchtools.sort', 'JGRID_HEADING_ID', 'a.id', $listDirn, $listOrder); ?>
                        </th>
                    </tr>
                </thead>

                <tfoot>
                    <tr>
                        <td colspan="30">
                            <?php echo $this->pagination->getListFooter(); ?>
                        </td>
                    </tr>
                </tfoot>

                <tbody>

                    <?php foreach ($this->items as $i => $item) : ?>

                        <?php
                        $canChange = true;
                        $canCheckin = true;
                        ?>

                        <tr class="row<?php echo $i % 2; ?>" sortable-group-id="<?php echo $item->catid; ?>">

                            <td class="nowrap center hidden-phone">
                                <?php echo JHtml::_('grid.id', $i, $item->id); ?>
                            </td>

                            <td class="nowrap center">
                                <div class="btn-group">
                                    <?php echo JHtml::_('jgrid.published', $item->state, $i, $context . '.', $canChange, 'cb'); ?>
                                    <?php
                                    // Create dropdown items
                                    $action = $archived ? 'unarchive' : 'archive';
                                    JHtml::_('actionsdropdown.' . $action, 'cb' . $i, $context);

                                    $action = $trashed ? 'untrash' : 'trash';
                                    JHtml::_('actionsdropdown.' . $action, 'cb' . $i, $context);

                                    // Render dropdown list
                                    echo JHtml::_('actionsdropdown.render', $this->escape($item->title));
                                    ?>
                                </div>
                            </td>

                            <td class="nowrap center small">
                                <?php echo JHtml::_('date', $item->created, JText::_('DATE_FORMAT_LC4')); ?>
                            </td>

                            <td class="nowrap has-context">
                                <div class="pull-left">
                                    <?php if ($item->checked_out) : ?>
                                        <?php echo JHtml::_('jgrid.checkedout', $i, $item->editor, $item->checked_out_time, $context . '.', $canCheckin); ?>
                                    <?php endif; ?>
                                    <a href="<?php echo JRoute::_('index.php?option=com_jkit&task=user.edit&id=' . $item->id); ?>">
                                        <?php if ($imageHelper->getImage($item->id, $context)) : ?>
                                            <?php echo '<img class="thumbnail pull-left jkit-images-thumb" src="' . $image_path . $item->id . '-tiny.jpg?' . rand(1, 999) . '" />'; ?>
                                        <?php endif; ?>
                                        <?php echo $this->escape($item->title); ?>
                                    </a>
                                    <div class="small"><?php echo $this->escape($item->first_name) . ' ' . $this->escape($item->last_name); ?></div>
                                </div>
                            </td>

                            <td class="nowrap small">
                                <?php echo $this->escape($item->email); ?>
                            </td> 

                            <td class="nowrap center small">
                                <?php echo JHtml::_('date', $item->lastvisitDate, JText::_('DATE_FORMAT_LC4') . ' H:m'); ?>
                            </td>

                            <td class="nowrap center small">
                                <?php echo $item->id; ?>
                            </td>

                        </tr>

                    <?php endforeach; ?>

                </tbody>


            </table>
        <?php endif; ?>
        <!-- /items table -->

    </div>
    <!-- /main -->

    <!-- form fields -->
    <input type="hidden" name="task" value="" />
    <input type="hidden" name="boxchecked" value="0" />
    <?php echo JHtml::_('form.token'); ?>
    <!-- /form fields -->

</form>
<!-- /adminForm -->
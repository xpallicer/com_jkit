<?php
/**
 * @package     JKit
 * @subpackage  com_jkit
 * @copyright   Copyright (C) 2013 - 2014 CloudHotelier. All rights reserved.
 * @license     GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 * @link        http://www.cloudhotelier.com
 * @author      Xavier Pallicer <xpallicer@gmail.com>
 */
// no direct access
defined('_JEXEC') or die;

JHtml::_('formbehavior.chosen', 'select');

$context = 'types';
$listOrder = $this->escape($this->state->get('list.ordering'));
$listDirn = $this->escape($this->state->get('list.direction'));

$types = JKitHelper::defaultTypes();
?>

<!-- adminForm -->
<form action="<?php echo JRoute::_('index.php?option=com_jkit&view=' . $context); ?>" method="post" name="adminForm" id="adminForm">

    <!-- sidebar -->
    <div id="j-sidebar-container" class="span2">
        <?php echo $this->sidebar; ?>
    </div>
    <!-- /sidebar -->

    <!-- main -->
    <div id="j-main-container" class="span10">

        <!-- search tools -->
        <?php echo JLayoutHelper::render('joomla.searchtools.default', array('view' => $this)); ?>

        <!-- no items -->
        <?php if (empty($this->items)) : ?>
            <div class="alert alert-no-items">
                <?php echo JText::_('JGLOBAL_NO_MATCHING_RESULTS'); ?>
            </div>
        <?php endif; ?>
        <!-- /no items -->

        <!-- items table -->
        <?php if (!empty($this->items)) : ?>

            <table class="table table-striped table-hover" id="articleList">

                <thead>
                    <tr>
                        <th width="1%" class="nowrap hidden-phone">
                            <?php // echo JHtml::_('grid.checkall'); ?>
                        </th> 
                        <th class="nowrap">
                            <?php echo JHtml::_('searchtools.sort', 'COM_JKIT_ITEM_TITLE', 'a.title', $listDirn, $listOrder); ?>
                        </th>
                        <th class="nowrap center" width="15%">
                            <?php echo JText::_('COM_JKIT_TYPE'); ?>
                        </th>
                        <th class="nowrap center" width="1%">
                            <?php echo JHtml::_('searchtools.sort', 'JGRID_HEADING_ID', 'a.id', $listDirn, $listOrder); ?>
                        </th>
                    </tr>
                </thead>

                <tfoot>
                    <tr>
                        <td colspan="30">
                            <?php echo $this->pagination->getListFooter(); ?>
                        </td>
                    </tr>
                </tfoot>

                <tbody>

                    <?php foreach ($this->items as $i => $item) : ?>

                        <?php $default = $item->id < 100; ?>
                        <?php $disabled = $default ? 'disabled' : ''; ?>

                        <tr class="row<?php echo $i % 2; ?>" sortable-group-id="<?php echo $item->catid; ?>">

                            <td class="nowrap center hidden-phone">
                                <?php echo '<input ' . $disabled . ' type="checkbox" id="cb' . $i . '" name="cid[]" value="' . $item->id . '" onclick="Joomla.isChecked(this.checked);" />'; ?>
                            </td>

                            <td class="nowrap">
                                <div class="pull-left">
                                    <?php if (!$default): ?>
                                        <a href="<?php echo JRoute::_('index.php?option=com_jkit&task=type.edit&id=' . $item->id); ?>">
                                            <i class="icon-wrench"></i> <?php echo $this->escape($item->title); ?>
                                        </a>
                                    <?php else: ?>
                                        <i class="icon-wrench"></i> <?php echo JText::_('COM_JKIT_TYPE_' . $types[$item->id]); ?>
                                    <?php endif; ?>
                                </div>
                            </td>

                            <td class="nowrap center small">
                                <?php
                                $type = $item->id < 100 ? 'DEFAULT' : 'CUSTOM';
                                echo JText::_('COM_JKIT_TYPE_' . $type);
                                ?>
                            </td>                            

                            <td class="nowrap center small">
                                <?php echo $item->id; ?>
                            </td>

                        </tr>

                    <?php endforeach; ?>

                </tbody>


            </table>
        <?php endif; ?>
        <!-- /items table -->

    </div>
    <!-- /main -->

    <!-- form fields -->
    <input type="hidden" name="task" value="" />
    <input type="hidden" name="boxchecked" value="0" />
    <?php echo JHtml::_('form.token'); ?>
    <!-- /form fields -->

</form>
<!-- /adminForm -->
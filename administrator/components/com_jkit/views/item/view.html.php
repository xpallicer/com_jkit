<?php

/**
 * @package     JKit
 * @subpackage  com_jkit
 * @copyright   Copyright (C) 2013 - 2014 CloudHotelier. All rights reserved.
 * @license     GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 * @link        http://www.cloudhotelier.com
 * @author      Xavier Pallicer <xpallicer@gmail.com>
 */
// no direct access
defined('_JEXEC') or die;

/**
 * Item View
 */
class JKitViewItem extends JViewLegacy {

    /**
     * Display the view
     */
    public function display($tpl = null) {

        // get the data from the model
        $this->form = $this->get('Form');
        $this->item = $this->get('Item');
        $this->state = $this->get('State');

        // get the tags
        $this->tags = $this->get('Tags');
        $this->tag_list = $this->get('TagList');
        $this->item_tags = $this->get('ItemTags');
        $this->item_tag_list = $this->get('ItemTagList');

        // get the types
        $this->types = $this->get('Types');
        $this->item_type = $this->get('ItemType');

        // get params
        $this->params = JComponentHelper::getParams('com_jkit');

        // get the image
        $imageHelper = new JKitHelperImage(JComponentHelper::getParams('com_jkit'));
        $this->image = $imageHelper->getImage($this->item->id, 'items');

        // create the toolbar
        JKitHelper::getToolbar(false, $this->item->id, $this->item->title);

        // display the view template
        parent::display($tpl);
    }

}

<?php
/**
 * @package     JKit
 * @subpackage  com_jkit
 * @copyright   Copyright (C) 2013 - 2014 CloudHotelier. All rights reserved.
 * @license     GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 * @link        http://www.cloudhotelier.com
 * @author      Xavier Pallicer <xpallicer@gmail.com>
 */
// no direct access
defined('_JEXEC') or die;

// chosen
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.keepalive');

// load tagsinput
JHtml::script(JUri::base() . 'components/com_jkit/assets/tagsinput/bootstrap-tagsinput.min.js');

// load tags typeahead
JFactory::getDocument()->addScriptDeclaration('window.jkit_tags = ' . json_encode($this->tag_list) . ';');
JFactory::getDocument()->addScriptDeclaration('window.jkit_types = ' . json_encode($this->types) . ';');

// prepare params
$params = $this->form->getFieldset('params');
$type_params = $this->item_type->params;
?>

<!-- adminForm -->
<form action="<?php echo JRoute::_('index.php?option=com_jkit&view=item&layout=edit&id=' . (int) $this->item->id); ?>" method="post" name="adminForm" id="adminForm" class="form-validate" enctype="multipart/form-data">

    <!-- form fields -->
    <input type="hidden" name="task" value="" />
    <input type="hidden" name="original_language" id="jkit-original-language" value="<?php echo $this->item->language; ?>" />
    <?php echo JHtml::_('form.token'); ?>

    <!-- main -->
    <div class="row-fluid" id="jkit-view-item">

        <div class="span9">

            <!-- info -->
            <div class="form-horizontal">
                <fieldset>
                    <?php foreach (array('title', 'alias', 'info') as $field): ?>
                        <?php echo $this->form->getControlGroup($field); ?>
                    <?php endforeach; ?>
                </fieldset>
            </div>

            <!-- intro -->
            <fieldset id="jkit-fieldset-intro" class="<?php echo $type_params->intro ? '' : 'hide'; ?>">
                <legend><?php echo JText::_('COM_JKIT_ITEM_INTRO') ?></legend>
                <?php echo $this->form->getInput('intro'); ?>
            </fieldset>

            <!-- body -->
            <fieldset id="jkit-fieldset-body" class="<?php echo $type_params->body ? '' : 'hide'; ?>">
                <legend><?php echo JText::_('COM_JKIT_ITEM_BODY') ?></legend>
                <?php echo $this->form->getInput('body'); ?>
            </fieldset>

            <!-- link --> 
            <div class="form-horizontal <?php echo $type_params->link ? '' : 'hide'; ?>" id="jkit-fieldset-link">
                <fieldset>
                    <legend><?php echo JText::_('COM_JKIT_ITEM_FIELDSET_LINK') ?></legend>
                    <?php echo $this->form->getControlGroup('link'); ?>
                    <?php
                    foreach (array('link_text', 'link_target') as $field) {
                        echo JKitHelper::getControlGroup($params, $field);
                    }
                    ?>
                </fieldset>
            </div>

            <!-- video --> 
            <div class="form-horizontal <?php echo $type_params->video ? '' : 'hide'; ?>" id="jkit-fieldset-video">
                <fieldset>
                    <legend><?php echo JText::_('COM_JKIT_ITEM_FIELDSET_VIDEO') ?></legend>
                    <?php echo $this->form->getControlGroup('video'); ?>
                    <?php echo JKitHelper::getControlGroup($params, 'video_provider'); ?>
                </fieldset>
            </div>

            <!-- attachment --> 
            <div class="form-horizontal <?php echo $type_params->attachment ? '' : 'hide'; ?>" id="jkit-fieldset-attachment">
                <fieldset>
                    <legend><?php echo JText::_('COM_JKIT_ITEM_FIELDSET_ATTACHMENT') ?></legend>
                    <?php echo $this->form->getControlGroup('attachment'); ?>
                    <?php echo JKitHelper::getControlGroup($params, 'attachment_file'); ?>
                </fieldset>
            </div>

            <!-- event --> 
            <div class="form-horizontal <?php echo $type_params->event ? '' : 'hide'; ?>" id="jkit-fieldset-event">
                <fieldset>
                    <legend><?php echo JText::_('COM_JKIT_ITEM_FIELDSET_EVENT') ?></legend>
                    <?php echo $this->form->getControlGroup('datetime'); ?>
                    <?php echo JKitHelper::getControlGroup($params, 'location'); ?>
                </fieldset>
            </div>

            <!-- pricing --> 
            <div class="form-horizontal <?php echo $type_params->pricing ? '' : 'hide'; ?>" id="jkit-fieldset-pricing">
                <fieldset>
                    <legend><?php echo JText::_('COM_JKIT_ITEM_FIELDSET_PRICING') ?></legend>
                    <?php echo $this->form->getControlGroup('units'); ?>
                    <?php echo $this->form->getControlGroup('amount'); ?>
                </fieldset>
            </div>

            <!-- quote --> 
            <div class="form-horizontal <?php echo $type_params->quote ? '' : 'hide'; ?>" id="jkit-fieldset-quote">
                <fieldset>
                    <legend><?php echo JText::_('COM_JKIT_ITEM_FIELDSET_QUOTE') ?></legend>
                    <?php echo JKitHelper::getControlGroup($params, 'author'); ?>
                    <?php echo JKitHelper::getControlGroup($params, 'source'); ?>
                </fieldset>
            </div>

            <!-- contact --> 
            <div class="form-horizontal <?php echo $type_params->contact ? '' : 'hide'; ?>" id="jkit-fieldset-contact">
                <fieldset>
                    <legend><?php echo JText::_('COM_JKIT_ITEM_FIELDSET_CONTACT') ?></legend>
                    <?php foreach (array('address', 'zip', 'city', 'region', 'country', 'phone', 'email') as $field): ?>
                        <?php echo $this->form->getControlGroup($field); ?>
                    <?php endforeach; ?>
                </fieldset>
            </div>

            <!-- data --> 
            <div class="form-horizontal <?php echo $type_params->data ? '' : 'hide'; ?>" id="jkit-fieldset-data">
                <fieldset>
                    <legend><?php echo JText::_('COM_JKIT_ITEM_FIELDSET_DATA') ?></legend>
                    <?php echo JKitHelper::getControlGroup($params, 'data'); ?>
                </fieldset>
            </div>

        </div>

        <!-- sidebar -->
        <div class="span3">

            <!-- image -->
            <div class="well <?php echo $type_params->image ? '' : 'hide'; ?>" id="jkit-fieldset-image">
                <h3><?php echo JText::_('COM_JKIT_ITEM_FIELDSET_IMAGE') ?></h3>
                <div class="control-group muted">
                    <?php echo JText::sprintf('COM_JKIT_ANY_IMAGE_DESC', $this->params->get('img_upload', 1500)); ?>
                </div>
                <?php if ($this->image): ?>
                    <div class="control-group">
                        <?php echo Jhtml::image(JURI::root() . 'images/jkit/items/' . $this->item->id . '-small.jpg?rand=' . rand(1, 999), $this->item->title, 'class="thumbnail"'); ?>
                    </div>
                    <div class="control-group">
                        <label class="checkbox hasTooltip" title="<?php echo JText::_('COM_JKIT_ANY_IMAGE_DELETE'); ?>::<?php echo JText::_('COM_JKIT_ANY_IMAGE_DELETE_DESC'); ?>">
                            <input type="checkbox" name="image_delete" value="1"><?php echo JText::_('COM_JKIT_ANY_IMAGE_DELETE'); ?>
                        </label>
                    </div>
                <?php endif; ?>
                <div class="control-group">
                    <input type="file" name="image" id="jkit-image" value="" class="">
                </div>
            </div>

            <!-- classification -->
            <div class="well">
                <h3><?php echo JText::_('COM_JKIT_ITEM_FIELDSET_CLASSIFICATION') ?></h3>
                <?php echo $this->form->getControlGroup('catid'); ?>
                <div id="jkit-fieldset-tags" class="<?php echo $type_params->tags ? '' : 'hide'; ?>">
                    <div id="jkit-tags-field">
                        <?php echo $this->form->getControlGroup('tags'); ?>
                    </div>
                    <div id="jkit-tags-lang" class="hide">
                        <div class="control-group">
                            <label class="control-label"><?php echo JText::_('COM_JKIT_ITEM_TAGS') ?></label>
                            <div class="alert alert-info">
                                <?php echo JText::_('COM_JKIT_ITEM_TAGS_LANG') ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- publishing -->
            <div class="well">
                <h3><?php echo JText::_('COM_JKIT_ITEM_FIELDSET_PUBLISH') ?></h3>
                <?php foreach (array('state', 'featured', 'language', 'user_id', 'created', 'id', 'type_id') as $field): ?>
                    <?php echo $this->form->getControlGroup($field); ?>
                <?php endforeach; ?>
            </div>

        </div>
        <!-- /sidebar -->

    </div>
    <!-- /main -->

</form>
<!-- /adminForm -->

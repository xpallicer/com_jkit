<?php

/**
 * @package     JKit
 * @subpackage  com_jkit
 * @copyright   Copyright (C) 2013 - 2014 CloudHotelier. All rights reserved.
 * @license     GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 * @link        http://www.cloudhotelier.com
 * @author      Xavier Pallicer <xpallicer@gmail.com>
 */
// no direct access
defined('_JEXEC') or die;

/**
 * Translation View
 */
class JKitViewTranslation extends JViewLegacy {

    /**
     * Display the view
     */
    public function display($tpl = null) {

        // get the data from the model
        $this->item = $this->get('Item');
        $this->translation = $this->get('Translation');
        $this->language = JKitHelperLangs::getLangTitle(JRequest::getCmd('lang'));
        $this->params = JComponentHelper::getParams('com_jkit');

        // create the toolbar
        JKitHelper::getToolbar(false, true, $this->item->title . ' (' . JText::sprintf('COM_JKIT_TRANSLATION_TITLE', $this->language) . ')');

        // display the view template
        parent::display($tpl);
    }

}

<?php
/**
 * @package     JKit
 * @author   	Xavier Pallicer
 * @copyright	Copyright (C) 2013 CloudHotelier. All rights reserved.
 * @license	GNU GPLv3 <http://www.gnu.org/licenses/gpl.intro>
 * @link	http://www.cloudhotelier.com
 */
defined('_JEXEC') or die;

JHtml::_('behavior.keepalive');

$api_key = $this->params->get('api_key', '');
$editor = JFactory::getEditor();
?>

<?php if ($api_key): ?>
    <input type="hidden" id="translate_key" value="<?php echo $api_key; ?>" />
    <input type="hidden" id="translate_id" value="" />
    <input type="hidden" id="translate_source" value="<?php echo substr($this->item->language, 0, 2); ?>" />
    <input type="hidden" id="translate_target" value="<?php echo substr(JRequest::getCmd('lang'), 0, 2); ?>" />
<?php endif; ?>

<form action="<?php echo JRoute::_('index.php?option=com_jkit'); ?>" method="post" name="adminForm" id="adminForm" class="form-validate">

    <input type="hidden" name="lang" value="<?php echo JRequest::getCmd('lang'); ?>" />
    <input type="hidden" name="id" value="<?php echo JRequest::getInt('id'); ?>" />
    <input type="hidden" name="tid" value="<?php echo @$this->translation->id; ?>" />
    <input type="hidden" name="task" value="" />
    <?php echo JHtml::_('form.token'); ?>

    <!-- main -->
    <div class="row-fluid" id="jkit-view-translation">

        <div class="span12">

            <div class="form-horizontal">

                <?php
                // title & alias
                foreach (array('title', 'alias') as $field) {
                    $label = JText::_('COM_JKIT_ITEM_' . strtoupper($field));
                    JKitHelper::renderTranslateText($field, $this->item->$field, @$this->translation->$field, $label, $api_key);
                }

                // info
                $field = 'info';
                $label = JText::_('COM_JKIT_ITEM_' . strtoupper($field));
                if ($this->item->$field) {
                    JKitHelper::renderTranslateTextArea($field, $this->item->$field, @$this->translation->data->$field, $label, $api_key);
                }

                // editors
                foreach (array('intro', 'body') as $field) {
                    $label = JText::_('COM_JKIT_ITEM_' . strtoupper($field));
                    if ($this->item->$field) {
                        JKitHelper::renderTranslateEditor($field, $this->item->$field, @$this->translation->data->$field, $label, $api_key);
                    }
                }

                // other translatable fields
                $fields = array(
                    'link' => 'ANY_LINK',
                    'params_link_text' => 'ANY_LINK_TEXT',
                    'video' => 'ITEM_VIDEO',
                    'params_location' => 'ITEM_LOCATION',
                    'params_author' => 'ITEM_AUTHOR',
                    'params_source' => 'ITEM_SOURCE'
                );
                foreach ($fields as $field => $label) {
                    if ($this->item->$field) {
                        JKitHelper::renderTranslateText($field, $this->item->$field, @$this->translation->$field, JText::_('COM_JKIT_' . strtoupper($label)), $api_key);
                    }
                }
                ?>

            </div>

            <?php if ($this->item->images): ?>

                <div class="form-horizontal">
                    <fieldset>
                        <legend><?php echo JText::_('COM_JKIT_ITEMS_IMAGES') ?></legend>
                        <?php
                        foreach ($this->item->images as $i => $image) {
                            $field = "image_" . $image->id;
                            $label = JText::_('COM_JKIT_ANY_IMAGE') . ' #' . ($i + 1);
                            JKitHelper::renderTranslateText($field, $image->title, @$this->translation->data->$field, $label, $api_key);
                            if ($image->info) {
                                echo '<div class="jkit-translate-image-info">';
                                $field = "image_info_" . $image->id;
                                $label = ' ';
                                JKitHelper::renderTranslateTextArea($field, $image->info, @$this->translation->data->$field, $label, $api_key);
                                echo '</div>';
                            }
                            if ($image->params->link) {
                                echo '<div class="jkit-translate-image-info">';
                                $field = "image_params_link_" . $image->id;
                                $label = ' ';
                                JKitHelper::renderTranslateText($field, $image->params->link, @$this->translation->data->$field, $label, $api_key);
                                echo '</div>';
                            }                           
                            if ($image->params->link_text) {
                                echo '<div class="jkit-translate-image-info">';
                                $field = "image_params_link_text_" . $image->id;
                                $label = ' ';
                                JKitHelper::renderTranslateText($field, $image->params->link_text, @$this->translation->data->$field, $label, $api_key);
                                echo '</div>';
                            }
                        }
                        ?>
                    </fieldset>
                </div>

            <?php endif; ?>

            <?php if ($this->item->tags): ?>

                <div class="form-horizontal">
                    <fieldset>
                        <legend><?php echo JText::_('COM_JKIT_ITEM_TAGS') ?></legend>
                        <?php
                        foreach ($this->item->tags as $i => $tag) {
                            $field = "tag_" . $tag->id;
                            $label = JText::_('COM_JKIT_ITEM_TAG') . ' #' . ($i + 1);
                            JKitHelper::renderTranslateText($field, $tag->title, @$this->translation->data->$field, $label, $api_key);
                        }
                        ?>
                    </fieldset>
                </div>

            <?php endif; ?>
        </div>

    </div>
    <!--/main -->

</form>
<?php

/**
 * @package     JKit
 * @subpackage  com_jkit
 * @copyright   Copyright (C) 2013 - 2014 CloudHotelier. All rights reserved.
 * @license     GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 * @link        http://www.cloudhotelier.com
 * @author      Xavier Pallicer <xpallicer@gmail.com>
 */
// no direct access
defined('_JEXEC') or die;

/**
 * Panel View
 */
class JKitViewPanel extends JViewLegacy {

    /**
     * Display the view
     */
    public function display($tpl = null) {

        // get data
        $this->categories = $this->get('Categories');
        $this->langs = $this->get('Langs');
        $this->edited = $this->get('Edited');
        $this->added = $this->get('Added');
        $this->images = $this->get('Images');
        $this->translations = $this->get('Translations');

        // create the toolbar
        JKitHelper::getToolbar('panel');

        // sidebar
        $this->sidebar = JHtmlSidebar::render();

        // display the view layout
        parent::display($tpl);
    }

}

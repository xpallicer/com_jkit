<?php
/**
 * @package     JKit
 * @subpackage  com_jkit
 * @copyright   Copyright (C) 2013 - 2014 CloudHotelier. All rights reserved.
 * @license     GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 * @link        http://www.cloudhotelier.com
 * @author      Xavier Pallicer <xpallicer@gmail.com>
 */
// no direct access
defined('_JEXEC') or die;

$langs = JKitHelperLangs::getLangs();
$imageHelper = new JKitHelperImage(JComponentHelper::getParams('com_jkit'));
$path_items = JURI::root() . "/images/jkit/items/";
$path_images = JURI::root() . "/images/jkit/images/";
$types = JKitHelper::defaultTypes();
?>

<!-- adminForm -->
<form action="<?php echo JRoute::_('index.php?option=com_jkit&view=items'); ?>" method="post" name="adminForm" id="adminForm">

    <!-- sidebar -->
    <div id="j-sidebar-container" class="span2">

        <?php echo $this->sidebar; ?>

        <!-- categories -->
        <?php if (count($this->categories)) : ?>
            <?php
            $language = $this->categories[0]->language;
            $lang_name = $this->langs[$language];
            ?>
            <br>
            <div class="sidebar-nav">
                <ul class="nav nav-list">
                    <li class="nav-header"><?php echo $lang_name; ?></li>
                    <?php
                    foreach ($this->categories as $i => $cat) {
                        if ($language != $cat->language) {
                            $language = $cat->language;
                            $lang_name = $this->langs[$language];
                            echo '<li class="nav-header">' . $lang_name . '</li>';
                        }
                        $link = JRoute::_('index.php?option=com_jkit&view=items&filter[catid]=' . $cat->id . '&filter[language]=');
                        echo '<li><a href="' . $link . '">' . str_repeat('&nbsp;&nbsp;&nbsp;', $cat->level - 1) . $cat->title . '</a></li>';
                    }
                    ?>
                </ul>
            </div>
        <?php endif; ?>

        <!-- langs -->
        <?php if (count($langs)): ?>
            <br>
            <div class="sidebar-nav">
                <ul class="nav nav-list">
                    <li class="nav-header"><?php echo JText::_('COM_JKIT_PANEL_LANGS'); ?></li>
                    <?php
                    $link = JRoute::_('index.php?option=com_jkit&view=items&filter[catid]=&filter[language]=*');
                    echo '<li><a href="' . $link . '">' . JText::_('JALL') . '</a></li>';
                    foreach ($langs as $lang) {
                        $link = JRoute::_('index.php?option=com_jkit&view=items&filter[catid]=&filter[language]=' . $lang->lang_code);
                        echo '<li><a href="' . $link . '">' . $lang->title . '</a></li>';
                    }
                    ?>
                </ul>
            </div>
        <?php endif; ?>

    </div>
    <!-- /sidebar -->

    <!-- main -->
    <div id="j-main-container" class="span10">

        <!-- latest edited & recently added items -->
        <div class="row-fluid">

            <?php foreach (array('edited', 'added') as $panel): ?>

                <div class="span6">

                    <div class="well well-small jkit-panel">

                        <h2 class="module-title nav-header"><?php echo JText::_('COM_JKIT_PANEL_' . strtoupper($panel)); ?></h2>

                        <?php if ($this->$panel): ?>

                            <?php foreach ($this->$panel as $item): ?>
                                <div class="row-striped">
                                    <div class="row-fluid">
                                        <div class="span9">
                                            <a href="<?php echo JRoute::_('index.php?option=com_jkit&task=item.edit&id=' . $item->id); ?>">
                                                <?php if ($imageHelper->getImage($item->id, 'items')) : ?>
                                                    <?php echo '<img class="thumbnail pull-left jkit-images-thumb" src="' . $path_items . $item->id . '-tiny.jpg?' . rand(1, 999) . '" />'; ?>
                                                <?php endif; ?>
                                                <?php echo $this->escape($item->title); ?>
                                            </a>
                                            <div class="small">
                                                <?php echo JText::_('JCATEGORY') . ": " . $this->escape($item->category); ?>
                                            </div>
                                        </div>
                                        <div class="span3">
                                            <span class="small"><i class="icon-calendar"></i> <?php echo JHtml::_('date', $item->modified, JText::_('DATE_FORMAT_LC4') . ' H:m'); ?></span>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; ?>

                            <div class="clearfix">
                                <?php
                                $order = $panel == 'edited' ? 'a.modified%20DESC' : 'a.id%20DESC';
                                $link = JRoute::_('index.php?option=com_jkit&view=items&filter[catid]=&filter[language]=&list[fullordering]=' . $order);
                                ?>
                                <a class="btn btn-small pull-right" href="<?php echo $link; ?>"><?php echo Jtext::_('COM_JKIT_PANEL_VIEW_MORE'); ?></a>
                            </div>

                        <?php endif; ?>

                    </div>

                </div> 

            <?php endforeach; ?>

        </div>          

        <div class="row-fluid">

            <!-- latest edited images -->
            <div class="span6">

                <div class="well well-small jkit-panel">

                    <h2 class="module-title nav-header"><?php echo JText::_('COM_JKIT_PANEL_IMAGES'); ?></h2>

                    <?php if ($this->images): ?>

                        <?php foreach ($this->images as $image): ?>

                            <div class="row-striped">
                                <div class="row-fluid">
                                    <div class="span9">
                                        <?php
                                        $link = JRoute::_('index.php?option=com_jkit&task=image.edit&id=' . $image->id);
                                        $gallery_link = JRoute::_("index.php?option=com_jkit&view=images&filter[item]=$image->item_id&filter[catid]=$image->item_catid&filter[user]=&filter[state]=");
                                        ?>
                                        <a href="<?php echo $link; ?>">
                                            <?php if ($imageHelper->getImage($image->id, 'images')) : ?>
                                                <?php echo '<img class="thumbnail pull-left jkit-images-thumb" src="' . $path_images . $image->id . '-tiny.jpg?' . rand(1, 999) . '" />'; ?>
                                            <?php endif; ?>
                                            <?php echo $this->escape($image->title); ?>
                                        </a>
                                        <div class="small">
                                            <?php echo JText::_('COM_JKIT_IMAGE_ITEM') . ': <a href="' . $gallery_link . '">' . $this->escape($image->item) . '</a>'; ?>
                                        </div>
                                    </div>
                                    <div class="span3">
                                        <span class="small"><i class="icon-calendar"></i> <?php echo JHtml::_('date', $image->modified, JText::_('DATE_FORMAT_LC4') . ' H:m'); ?></span>
                                    </div>
                                </div>
                            </div>

                        <?php endforeach; ?>

                    <?php endif; ?>

                </div>

            </div> 

            <!-- latest edited translations -->
            <div class="span6">

                <div class="well well-small jkit-panel">

                    <h2 class="module-title nav-header"><?php echo JText::_('COM_JKIT_PANEL_TRANSLATIONS'); ?></h2>

                    <?php if ($this->translations): ?>

                        <?php foreach ($this->translations as $translation): ?>

                            <div class="row-striped">
                                <div class="row-fluid">
                                    <div class="span9">
                                        <?php $link = JRoute::_('index.php?option=com_jkit&task=translation.edit&id=' . $translation->ref_id . '&lang=' . $translation->lang); ?>
                                        <a href="<?php echo $link; ?>">
                                            <?php echo $this->escape($translation->title); ?>
                                        </a>
                                        <div class="small">
                                            <?php echo JText::_('COM_JKIT_IMAGE_ITEM') . ': ' . $this->escape($translation->item) . ' (' . $translation->lang . ')'; ?>
                                        </div>
                                    </div>
                                    <div class="span3">
                                        <span class="small"><i class="icon-calendar"></i> <?php echo JHtml::_('date', $translation->created, JText::_('DATE_FORMAT_LC4') . ' H:m'); ?></span>
                                    </div>
                                </div>
                            </div>

                        <?php endforeach; ?>

                    <?php endif; ?>

                </div>

            </div> 

        </div>  

    </div>
    <!-- /main -->

    <!-- form fields -->
    <input type="hidden" name="task" value="" />
    <input type="hidden" name="boxchecked" value="0" />
    <?php echo JHtml::_('form.token'); ?>
    <!-- /form fields -->

</form>
<!-- /adminForm -->

<div id="jkit-tips"> </div>

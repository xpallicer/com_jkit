<?php

/**
 * @package     JKit
 * @subpackage  com_jkit
 * @copyright   Copyright (C) 2013 - 2014 CloudHotelier. All rights reserved.
 * @license     GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 * @link        http://www.cloudhotelier.com
 * @author      Xavier Pallicer <xpallicer@gmail.com>
 */
// no direct access
defined('_JEXEC') or die;

/**
 * JKit Main Controller
 */
class JKitController extends JControllerLegacy {

    /**
     * Display the view
     */
    public function display($cachable = false, $urlparams = false) {
        
        // set the default view
        JRequest::setVar('view', JRequest::getCmd('view', 'panel'));
        
        // display the view
        parent::display($cachable, $urlparams);
    }

}

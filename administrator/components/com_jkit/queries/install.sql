
CREATE TABLE IF NOT EXISTS `#__jkit_images` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `item_id` bigint(20) unsigned NOT NULL,
  `title` varchar(255) NOT NULL,
  `info` text NOT NULL,
  `params` text NOT NULL,
  `ordering` int(11) NOT NULL DEFAULT '0',
  `state` tinyint(1) NOT NULL DEFAULT '1',
  `created` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `modified` datetime NOT NULL,
  `modified_by` int(11) NOT NULL,
  `checked_out_time` datetime NOT NULL,
  `checked_out` int(11) NOT NULL,
  UNIQUE KEY `idx_id` (`id`),
  KEY `idx_state` (`state`),
  KEY `idx_checkout` (`checked_out`),
  KEY `idx_createdby` (`created_by`),
  KEY `idx_itemid` (`item_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `#__jkit_items` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `catid` bigint(20) unsigned NOT NULL,
  `language` char(7) NOT NULL,
  `user_id` bigint(20) unsigned NOT NULL,
  `type_id` int(11) NOT NULL DEFAULT '1',
  `title` varchar(255) NOT NULL,
  `alias` varchar(255) NOT NULL,
  `info` text NOT NULL,
  `intro` text NOT NULL,
  `body` text NOT NULL,
  `link` varchar(1024) NOT NULL,
  `video` varchar(255) NOT NULL,
  `attachment` varchar(1024) NOT NULL,
  `units` int(11) NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `datetime` datetime NOT NULL,
  `address` varchar(1024) NOT NULL,
  `zip` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `region` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `params` text NOT NULL,
  `ordering` int(11) NOT NULL DEFAULT '0',
  `featured` int(11) NOT NULL,
  `state` tinyint(1) NOT NULL DEFAULT '1',
  `created` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `modified` datetime NOT NULL,
  `modified_by` int(11) NOT NULL,
  `checked_out_time` datetime NOT NULL,
  `checked_out` int(11) NOT NULL,
  UNIQUE KEY `idx_id` (`id`),
  KEY `idx_state` (`state`),
  KEY `idx_checkout` (`checked_out`),
  KEY `idx_createdby` (`created_by`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `#__jkit_tags` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `language` char(7) NOT NULL,
  `title` varchar(255) NOT NULL,
  `alias` varchar(255) NOT NULL,
  `info` text NOT NULL,
  `params` text NOT NULL,
  `state` tinyint(1) NOT NULL DEFAULT '1',
  `created` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  UNIQUE KEY `idx_id` (`id`),
  KEY `idx_state` (`state`),
  KEY `idx_createdby` (`created_by`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `#__jkit_tags_item` (
  `tag_id` bigint(20) NOT NULL,
  `item_id` bigint(20) NOT NULL,
  PRIMARY KEY (`tag_id`,`item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `#__jkit_translations` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ref_table` varchar(255) NOT NULL,
  `ref_id` bigint(20) unsigned NOT NULL,
  `lang` char(7) NOT NULL,
  `alias` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `translation` text NOT NULL,
  `created` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  UNIQUE KEY `idx_id` (`id`),
  KEY `idx_refid` (`ref_id`),
  KEY `idx_reftable` (`ref_table`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `#__jkit_types` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `params` text NOT NULL,
  `state` tinyint(1) NOT NULL DEFAULT '1',
  `checked_out_time` datetime NOT NULL,
  `checked_out` int(11) NOT NULL,
  UNIQUE KEY `idx_id` (`id`),
  KEY `idx_state` (`state`),
  KEY `idx_checkout` (`checked_out`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=101;

CREATE TABLE IF NOT EXISTS `#__jkit_users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `jid` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `alias` varchar(255) NOT NULL,
  `first_name` varchar(1024) NOT NULL,
  `last_name` varchar(1024) NOT NULL,
  `phone` varchar(1024) NOT NULL,
  `info` text NOT NULL,
  `params` text NOT NULL,
  `state` tinyint(1) NOT NULL DEFAULT '1',
  `created` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `checked_out_time` datetime NOT NULL,
  `checked_out` int(11) NOT NULL,
  UNIQUE KEY `idx_id` (`id`),
  KEY `idx_state` (`state`),
  KEY `idx_checkout` (`checked_out`),
  KEY `idx_createdby` (`created_by`),
  KEY `idx_jid` (`jid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;



INSERT IGNORE INTO `#__jkit_types` (`id`, `title`, `params`, `state`, `checked_out_time`, `checked_out`) VALUES
(1, 'Standard', '{"intro":"1","body":"1","link":"1","video":"1","attachment":"1","event":"1","pricing":"1","quote":"1","contact":"1","image":"1","tags":"1"}', 1, '0000-00-00 00:00:00', 0),
(2, 'Page', '{"intro":"0","body":"1","link":"0","video":"0","attachment":"0","event":"0","pricing":"0","quote":"0","contact":"0","image":"1","tags":"0"}', 1, '0000-00-00 00:00:00', 0),
(3, 'Post', '{"intro":"1","body":"1","link":"0","video":"0","attachment":"0","event":"0","pricing":"0","quote":"0","contact":"0","image":"1","tags":"1"}', 1, '0000-00-00 00:00:00', 0),
(4, 'Image', '{"intro":"0","body":"0","link":"0","video":"0","attachment":"0","event":"0","pricing":"0","quote":"0","contact":"0","image":"1","tags":"1"}', 1, '0000-00-00 00:00:00', 0),
(5, 'Video', '{"intro":"0","body":"0","link":"0","video":"1","attachment":"0","event":"0","pricing":"0","quote":"0","contact":"0","image":"1","tags":"1"}', 1, '0000-00-00 00:00:00', 0),
(6, 'Link', '{"intro":"0","body":"0","link":"1","video":"0","attachment":"0","event":"0","pricing":"0","quote":"0","contact":"0","image":"0","tags":"1"}', 1, '0000-00-00 00:00:00', 0),
(7, 'Attachment', '{"intro":"0","body":"0","link":"0","video":"0","attachment":"1","event":"0","pricing":"0","quote":"0","contact":"0","image":"1","tags":"1"}', 1, '0000-00-00 00:00:00', 0),
(8, 'Quote', '{"intro":"0","body":"0","link":"0","video":"0","attachment":"0","event":"0","pricing":"0","quote":"1","contact":"0","image":"1","tags":"1"}', 1, '0000-00-00 00:00:00', 0),
(9, 'Gallery', '{"intro":"0","body":"0","link":"0","video":"0","attachment":"0","event":"0","pricing":"0","quote":"0","contact":"0","image":"1","tags":"1"}', 1, '0000-00-00 00:00:00', 0),
(10, 'Event', '{"intro":"0","body":"0","link":"0","video":"0","attachment":"0","event":"1","pricing":"0","quote":"0","contact":"0","image":"1","tags":"1"}', 1, '0000-00-00 00:00:00', 0),
(11, 'Contact', '{"intro":"0","body":"1","link":"0","video":"0","attachment":"0","event":"0","pricing":"0","quote":"0","contact":"1","image":"1","tags":"1"}', 1, '0000-00-00 00:00:00', 0),
(12, 'Product', '{"intro":"0","body":"1","link":"0","video":"1","attachment":"0","event":"0","pricing":"1","quote":"0","contact":"0","image":"1","tags":"1"}', 1, '0000-00-00 00:00:00', 0);
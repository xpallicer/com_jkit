DROP TABLE IF EXISTS `#__jkit_images`;
DROP TABLE IF EXISTS `#__jkit_items`;
DROP TABLE IF EXISTS `#__jkit_tags`;
DROP TABLE IF EXISTS `#__jkit_tags_item`;
DROP TABLE IF EXISTS `#__jkit_translations`;
DROP TABLE IF EXISTS `#__jkit_users`;
DROP TABLE IF EXISTS `#__jkit_types`;
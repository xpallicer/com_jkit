# UPGRADE TO JOOMLA! 3.x series

# alias
ALTER TABLE  `#__jkit_items` CHANGE  `slug` `alias` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;
ALTER TABLE  `#__jkit_tags` CHANGE  `slug` `alias` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;
ALTER TABLE  `#__jkit_translations` CHANGE `slug` `alias` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL
ALTER TABLE  `#__jkit_users` CHANGE  `slug`  `alias` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL

# state
ALTER TABLE  `#__jkit_items` CHANGE  `published` `state` TINYINT( 1 ) NOT NULL DEFAULT  '1';
ALTER TABLE  `#__jkit_images` CHANGE  `published` `state` TINYINT( 1 ) NOT NULL DEFAULT  '1';
ALTER TABLE  `#__jkit_tags` CHANGE  `published` `state` TINYINT( 1 ) NOT NULL DEFAULT  '1';
ALTER TABLE  `#__jkit_users` CHANGE  `published` `state` TINYINT( 1 ) NOT NULL DEFAULT  '1';

# contact
ALTER TABLE  `#__jkit_items` ADD  `address` VARCHAR( 1024 ) NOT NULL AFTER  `datetime` ,
ADD  `zip` VARCHAR( 255 ) NOT NULL AFTER  `address` ,
ADD  `city` VARCHAR( 255 ) NOT NULL AFTER  `zip` ,
ADD  `region` VARCHAR( 255 ) NOT NULL AFTER  `city` ,
ADD  `country` VARCHAR( 255 ) NOT NULL AFTER  `region` ,
ADD  `phone` VARCHAR( 255 ) NOT NULL AFTER  `country`,
ADD  `email` VARCHAR( 255 ) NOT NULL AFTER  `phone`;

# catid
ALTER TABLE  `#__jkit_items` CHANGE  `category_id`  `catid` BIGINT( 20 ) UNSIGNED NOT NULL;

# new types table
SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

CREATE TABLE `#__jkit_types` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `params` text NOT NULL,
  `state` tinyint(1) NOT NULL DEFAULT '1',
  `checked_out_time` datetime NOT NULL,
  `checked_out` int(11) NOT NULL,
  UNIQUE KEY `idx_id` (`id`),
  KEY `idx_state` (`state`),
  KEY `idx_checkout` (`checked_out`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=101 ;

INSERT INTO `#__jkit_types` (`id`, `title`, `params`, `state`, `checked_out_time`, `checked_out`) VALUES
(1, 'Standard', '{"intro":"1","body":"1","link":"1","video":"1","attachment":"1","event":"1","pricing":"1","quote":"1","contact":"1","image":"1","tags":"1"}', 1, '0000-00-00 00:00:00', 0),
(2, 'Page', '{"intro":"0","body":"1","link":"0","video":"0","attachment":"0","event":"0","pricing":"0","quote":"0","contact":"0","image":"1","tags":"0"}', 1, '0000-00-00 00:00:00', 0),
(3, 'Post', '{"intro":"1","body":"1","link":"0","video":"0","attachment":"0","event":"0","pricing":"0","quote":"0","contact":"0","image":"1","tags":"1"}', 1, '0000-00-00 00:00:00', 0),
(4, 'Image', '{"intro":"0","body":"0","link":"0","video":"0","attachment":"0","event":"0","pricing":"0","quote":"0","contact":"0","image":"1","tags":"1"}', 1, '0000-00-00 00:00:00', 0),
(5, 'Video', '{"intro":"0","body":"0","link":"0","video":"1","attachment":"0","event":"0","pricing":"0","quote":"0","contact":"0","image":"1","tags":"1"}', 1, '0000-00-00 00:00:00', 0),
(6, 'Link', '{"intro":"0","body":"0","link":"1","video":"0","attachment":"0","event":"0","pricing":"0","quote":"0","contact":"0","image":"0","tags":"1"}', 1, '0000-00-00 00:00:00', 0),
(7, 'Attachment', '{"intro":"0","body":"0","link":"0","video":"0","attachment":"1","event":"0","pricing":"0","quote":"0","contact":"0","image":"1","tags":"1"}', 1, '0000-00-00 00:00:00', 0),
(8, 'Quote', '{"intro":"0","body":"0","link":"0","video":"0","attachment":"0","event":"0","pricing":"0","quote":"1","contact":"0","image":"1","tags":"1"}', 1, '0000-00-00 00:00:00', 0),
(9, 'Gallery', '{"intro":"0","body":"0","link":"0","video":"0","attachment":"0","event":"0","pricing":"0","quote":"0","contact":"0","image":"1","tags":"1"}', 1, '0000-00-00 00:00:00', 0),
(10, 'Event', '{"intro":"0","body":"0","link":"0","video":"0","attachment":"0","event":"1","pricing":"0","quote":"0","contact":"0","image":"1","tags":"1"}', 1, '0000-00-00 00:00:00', 0),
(11, 'Contact', '{"intro":"0","body":"0","link":"0","video":"0","attachment":"0","event":"0","pricing":"0","quote":"0","contact":"1","image":"1","tags":"1"}', 1, '0000-00-00 00:00:00', 0),
(12, 'Product', '{"intro":"0","body":"1","link":"0","video":"1","attachment":"0","event":"0","pricing":"1","quote":"0","contact":"0","image":"1","tags":"1"}', 1, '0000-00-00 00:00:00', 0);


# update types
ALTER TABLE  `#__jkit_items` CHANGE  `type`  `type_id` INT( 11 ) NOT NULL DEFAULT  '1';
UPDATE  `#__jkit_items` SET  `type_id` =  '1' WHERE  `id` > 0;

# users table
ALTER TABLE `#__jkit_users`
  DROP `url`,
  DROP `twitter`,
  DROP `facebook`,
  DROP `linkedin`,
  DROP `google`;
ALTER TABLE  `#__jkit_users` ADD  `params` TEXT NOT NULL AFTER  `info`;


# rename INDEXES

ALTER TABLE  `#__jkit_items` DROP INDEX  `id` , ADD UNIQUE  `idx_id` (  `id` );
ALTER TABLE  `#__jkit_items` DROP INDEX  `published` , ADD INDEX  `idx_state` (  `state` );
ALTER TABLE  `#__jkit_items` DROP INDEX  `checked_out` , ADD INDEX  `idx_checkout` (  `checked_out` );
ALTER TABLE  `#__jkit_items` DROP INDEX  `created_by` , ADD INDEX  `idx_createdby` (  `created_by` );

ALTER TABLE  `xcghj_jkit_images` DROP INDEX  `id` , ADD UNIQUE  `idx_id` (  `id` );
ALTER TABLE  `xcghj_jkit_images` DROP INDEX  `published` , ADD INDEX  `idx_state` (  `state` );
ALTER TABLE  `xcghj_jkit_images` DROP INDEX  `checked_out` , ADD INDEX  `idx_checkout` (  `checked_out` );
ALTER TABLE  `xcghj_jkit_images` DROP INDEX  `created_by` , ADD INDEX  `idx_createdby` (  `created_by` );
ALTER TABLE  `xcghj_jkit_images` DROP INDEX  `item_id` , ADD INDEX  `idx_itemid` (  `item_id` );

ALTER TABLE  `#__jkit_tags` DROP INDEX  `id` , ADD UNIQUE  `idx_id` (  `id` );
ALTER TABLE  `#__jkit_tags` DROP INDEX  `published` , ADD INDEX  `idx_state` (  `state` );
ALTER TABLE  `#__jkit_tags`  DROP INDEX  `created_by` ,ADD INDEX  `idx_createdby` (  `created_by` );

ALTER TABLE  `#__jkit_translations` DROP INDEX  `id` , ADD UNIQUE  `idx_id` (  `id` );
ALTER TABLE  `#__jkit_translations` DROP INDEX  `ref_id` , ADD INDEX  `idx_refid` (  `ref_id` );
ALTER TABLE  `#__jkit_translations`  DROP INDEX  `ref_table` , ADD INDEX  `idx_reftable` (  `ref_table` );

ALTER TABLE  `#__jkit_types` DROP INDEX  `id` , ADD UNIQUE  `idx_id` (  `id` );
ALTER TABLE  `#__jkit_types` DROP INDEX  `state` , ADD INDEX  `idx_state` (  `state` );
ALTER TABLE  `#__jkit_types` DROP INDEX  `checked_out` , ADD INDEX  `idx_checkout` (  `checked_out` );

ALTER TABLE  `#__jkit_users` DROP INDEX  `id` , ADD UNIQUE  `idx_id` (  `id` );
ALTER TABLE  `#__jkit_users` DROP INDEX  `published` , ADD INDEX  `idx_state` (  `state` );
ALTER TABLE  `#__jkit_users` DROP INDEX  `checked_out` , ADD INDEX  `idx_checkout` (  `checked_out` );
ALTER TABLE  `#__jkit_users` DROP INDEX  `created_by` , ADD INDEX  `idx_createdby` (  `created_by` );
ALTER TABLE  `#__jkit_users` DROP INDEX  `jid` , ADD INDEX  `idx_jid` (  `jid` );
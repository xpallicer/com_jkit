<?php

/**
 * @package     JKit
 * @subpackage  com_jkit
 * @copyright   Copyright (C) 2013 - 2014 CloudHotelier. All rights reserved.
 * @license     GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 * @link        http://www.cloudhotelier.com
 * @author      Xavier Pallicer <xpallicer@gmail.com>
 */
// no direct access
defined('_JEXEC') or die;

/**
 * Tags Controller
 */
class JKitControllerTags extends JControllerAdmin {

    /**
     * Prefix
     */
    protected $text_prefix = 'COM_JKIT_TAGS';

    /**
     * Get the model
     */
    public function getModel($name = 'Tag', $prefix = 'JKitModel', $config = array('ignore_request' => true)) {
        return parent::getModel($name, $prefix, $config);
    }

}

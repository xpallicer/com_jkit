<?php

/**
 * @package     JKit
 * @subpackage  com_jkit
 * @copyright   Copyright (C) 2013 - 2014 CloudHotelier. All rights reserved.
 * @license     GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 * @link        http://www.cloudhotelier.com
 * @author      Xavier Pallicer <xpallicer@gmail.com>
 */
// no direct access
defined('_JEXEC') or die;

/**
 * Image controller
 */
class JKitControllerImage extends JControllerForm {

    /**
     * Prefix
     */
    protected $text_prefix = 'COM_JKIT_IMAGE';

    /**
     * Extend parent edit to check if the new image has a item defined
     */
    public function edit($key = null, $urlVar = null) {

        // check item 
        $id = JRequest::getInt('id', 0);
        $filters = JFactory::getApplication()->getUserState('com_jkit.images.filter');
        $item = $filters['item'];
        if (!$id && !$item) {
            $this->setRedirect(JRoute::_('index.php?option=com_jkit&view=images', false), JText::_('COM_JKIT_IMAGE_SELECT_ITEM'), 'notice');
            return;
        }

        parent::edit($key = null, $urlVar = null);
    }

}

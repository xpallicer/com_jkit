<?php

/**
 * @package     JKit
 * @subpackage  com_jkit
 * @copyright   Copyright (C) 2013 - 2014 CloudHotelier. All rights reserved.
 * @license     GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 * @link        http://www.cloudhotelier.com
 * @author      Xavier Pallicer <xpallicer@gmail.com>
 */
// no direct access
defined('_JEXEC') or die;

/**
 * Item Controller
 */
class JKitControllerItem extends JControllerForm {

    /**
     * Prefix
     */
    protected $text_prefix = 'COM_JKIT_ITEM';

    /**
     * batch
     */
    public function batch($model = null) {

        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // Set the model
        $model = $this->getModel('Item', '', array());

        // Preset the redirect
        $this->setRedirect(JRoute::_('index.php?option=com_jkit&view=items' . $this->getRedirectToListAppend(), false));

        return parent::batch($model);
    }

}

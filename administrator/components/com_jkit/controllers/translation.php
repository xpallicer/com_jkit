<?php

/**
 * @package     JKit
 * @subpackage  com_jkit
 * @copyright   Copyright (C) 2013 - 2014 CloudHotelier. All rights reserved.
 * @license     GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 * @link        http://www.cloudhotelier.com
 * @author      Xavier Pallicer <xpallicer@gmail.com>
 */
// no direct access
defined('_JEXEC') or die;

/**
 * Translation Controller
 */
class JKitControllerTranslation extends JControllerForm {

    /**
     * Prefix
     */
    protected $text_prefix = 'COM_JKIT_ITEM';

    /**
     * Edit task
     */
    function edit($key = null, $urlVar = null) {
        $edit = 'index.php?option=com_jkit&view=translation&id=' . JRequest::getInt('id') . '&lang=' . JRequest::getCmd('lang');
        $this->setRedirect($edit);
    }

    /**
     * Save task
     */
    function save($key = null, $urlVar = null) {

        $app = JFactory::getApplication();
        $model = $this->getModel();
        $apply = 'index.php?option=com_jkit&view=translation&id=' . JRequest::getInt('id') . '&lang=' . JRequest::getCmd('lang');

        $save = $model->save();
        if (!$save) {
            $errors = $model->getErrors();
            foreach ($errors as $error) {
                $app->enqueueMessage($error, 'warning');
            }
            $this->setRedirect($apply);
            return false;
        }

        JFactory::getApplication()->setUserState('com_jkit.translation.data', null);
        if (JRequest::getWord('task') == 'save') {
            $this->setRedirect('index.php?option=com_jkit&view=items', JText::_('JLIB_APPLICATION_SAVE_SUCCESS'));
        } else {
            $this->setRedirect($apply, JText::_('JLIB_APPLICATION_SAVE_SUCCESS'));
        }
    }

    /**
     * Cancel task
     */
    function cancel($key = null) {
        JFactory::getApplication()->setUserState('com_jkit.translation.data', null);
        $this->setRedirect('index.php?option=com_jkit&view=items');
    }

    /**
     * Delete task
     */
    function delete() {
        $model = $this->getModel();
        $model->delete();
        JFactory::getApplication()->setUserState('com_jkit.translation.data', null);
        $this->setRedirect('index.php?option=com_jkit&view=items', JText::_('COM_JKIT_TRANSLATION_DELETED'));
    }

}

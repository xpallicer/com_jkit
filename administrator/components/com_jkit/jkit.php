<?php

/**
 * @package     JKit
 * @subpackage  com_jkit
 * @copyright   Copyright (C) 2013 - 2014 CloudHotelier. All rights reserved.
 * @license     GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 * @link        http://www.cloudhotelier.com
 * @author      Xavier Pallicer <xpallicer@gmail.com>
 */
// no direct access
defined('_JEXEC') or die;

// ACL check
if (!JFactory::getUser()->authorise('core.manage', 'com_jkit')) {
    return JError::raiseWarning(404, JText::_('JERROR_ALERTNOAUTHOR'));
}

// load language files
JFactory::getLanguage()->load('com_jkit', null, 'en-GB', true);
JFactory::getLanguage()->load('com_jkit');

// load helpers
require_once JPATH_COMPONENT . '/helpers/jkit.php';
require_once JPATH_COMPONENT . '/helpers/image.php';
require_once JPATH_COMPONENT . '/helpers/langs.php';

// load and execute the controller
$controller = JControllerLegacy::getInstance('JKit');
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();

<?php

/**
 * @package     JKit
 * @author   	Xavier Pallicer
 * @copyright	Copyright (C) 2013 CloudHotelier. All rights reserved.
 * @license	GNU GPLv3 <http://www.gnu.org/licenses/gpl.html>
 * @link	http://www.cloudhotelier.com
 */
defined('_JEXEC') or die;

JLoader::register('JTableUser', JPATH_LIBRARIES . '/joomla/database/table/user.php');

/**
 * User Table
 */
class JKitTableUser extends JTable {

    function __construct(&$db) {
        parent::__construct('#__jkit_users', 'id', $db);
    }

    /**
     * Store data
     */
    public function store($updateNulls = false) {

        $date = JFactory::getDate();
        $user = JFactory::getUser();

        if (!$this->id) {
            $this->created = $date->toSql();
            $this->created_by = $user->get('id');
        }

        return parent::store($updateNulls);
    }

    /**
     * Check data
     */
    public function check() {

        // check title 
        if (trim($this->title) == '') {
            $this->setError(JText::_('COM_JKIT_ANY_ERROR_NOTITLE'));
            return false;
        }

        // check alias
        $this->alias = JApplication::stringURLSafe($this->alias);
        if (trim(str_replace('-', '', $this->alias)) == '') {
            $this->alias = JApplication::stringURLSafe($this->title);
            if (trim(str_replace('-', '', $this->alias)) == '') {
                $this->setError(JText::_('COM_JKIT_ANY_ERROR_NOALIAS'));
                return false;
            }
        }

        // verify unique alias
        $table = JTable::getInstance('User', 'JKitTable', array('dbo' => $this->getDbo()));
        if ($table->load(array('alias' => $this->alias)) && ($table->id != $this->id || $this->id == 0)) {
            $this->setError(JText::_('COM_JKIT_TAG_ALIAS_ERROR'));
            return false;
        }

        return true;
    }

    /**
     * Publish method (yeah this is sad but we are on J!3)
     */
    public function publish($pks = null, $state = 1, $userId = 0) {

        $k = $this->_tbl_key;

        // sanitize input
        JArrayHelper::toInteger($pks);
        $userId = (int) $userId;
        $state = (int) $state;

        // if there are no primary keys set check if the instance key is set already
        if (empty($pks)) {
            if ($this->$k) {
                $pks = array($this->$k);
            } else {
                $this->setError(JText::_('JLIB_DATABASE_ERROR_NO_ROWS_SELECTED'));
                return false;
            }
        }

        // get table instance
        $table = JTable::getInstance('User', 'JKitTable');

        foreach ($pks as $pk) {

            // Load the item
            if (!$table->load($pk)) {
                $this->setError($table->getError());
            }

            // verify checkout
            if ($table->checked_out == 0 || $table->checked_out == $userId) {

                // change the state
                $table->state = $state;
                $table->checked_out = 0;
                $table->checked_out_time = $this->_db->getNullDate();

                // check the row
                $table->check();

                // store the row
                if (!$table->store()) {
                    $this->setError($table->getError());
                }
            }
        }

        return count($this->getErrors()) == 0;
    }

}

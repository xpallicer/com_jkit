<?php

/**
 * @package     JKit
 * @author   	Xavier Pallicer
 * @copyright	Copyright (C) 2013 CloudHotelier. All rights reserved.
 * @license	GNU GPLv3 <http://www.gnu.org/licenses/gpl.html>
 * @link	http://www.cloudhotelier.com
 */
defined('_JEXEC') or die;

/**
 * Tag Table
 */
class JKitTableTag extends JTable {

    /**
     * Constructor
     */
    function __construct(&$db) {
        parent::__construct('#__jkit_tags', 'id', $db);
    }

    /**
     * Store
     */
    public function store($updateNulls = false) {

        $date = JFactory::getDate();
        $user = JFactory::getUser();

        // created & modified
        if (!$this->id) {
            $this->created = $date->toSql();
            $this->created_by = $user->get('id');
        }

        return parent::store($updateNulls);
    }

    /**
     * Check
     */
    public function check() {

        // check title 
        if (trim($this->title) == '') {
            $this->setError(JText::_('COM_JKIT_TAG_ERROR_TITLE'));
            return false;
        }

        // / create alias from title, and make safe
        $this->alias = JApplication::stringURLSafe($this->title);
        if (trim(str_replace('-', '', $this->alias)) == '') {
            $this->setError(JText::_('COM_JKIT_TAG_ERROR_ALIAS'));
            return false;
        }

        // verify unique alias
        $table = JTable::getInstance('Tag', 'JKitTable', array('dbo' => $this->getDbo()));
        if ($table->load(array('alias' => $this->alias)) && ($table->id != $this->id || $this->id == 0)) {
            $this->setError(JText::_('COM_JKIT_TAG_ERROR_ALIAS'));
            return false;
        }



        // check translations titles and alias
        if ($this->language != '*') {
            $db = JFactory::getDbo();
            $langs = JKitHelperLangs::getLangs();
            foreach ($langs as $lang) {
                if ($this->language != $lang->lang_code) {

                    // check translation title
                    $t_title = trim(JRequest::getString('translation_title_' . $lang->lang_code));
                    if ($t_title == '') {
                        $this->setError(JText::sprintf('COM_JKIT_TAG_ERROR_TRANSLATION_TITLE', $lang->title));
                        return false;
                    }

                    // check translation alias
                    $t_alias = JApplication::stringURLSafe($t_title);
                    if (trim(str_replace('-', '', $t_alias)) == '') {
                        $this->setError(JText::sprintf('COM_JKIT_TAG_ERROR_TRANSLATION_ALIAS', $lang->title));
                        return false;
                    }

                    // check translation repeated alias
                    $q_l = $db->quote($lang->lang_code);
                    $q_a = $db->quote($t_alias);
                    $ref_id = $this->id ? " AND `ref_id` != $this->id" : '';
                    $q1 = "SELECT `ref_id` FROM `#__jkit_translations` WHERE `ref_table` = 'tags' AND `alias` = $q_a AND `lang` = $q_l $ref_id";
                    $t_ids1 = $db->setQuery($q1)->loadColumn();
                    if ($t_ids1) {
                        $this->setError(JText::sprintf('COM_JKIT_TAG_ERROR_TRANSLATION_ALIAS', $lang->title));
                        return false;
                    }

                    // check duplicated tags in different languages
                    $tid = $this->id ? " AND id != $this->id" : '';
                    $q2 = "SELECT `id` FROM `#__jkit_tags` WHERE `alias` = $q_a AND `language` = $q_l $tid";
                    $t_ids2 = $db->setQuery($q2)->loadColumn();
                    if ($t_ids2) {
                        $this->setError(JText::sprintf('COM_JKIT_TAG_ERROR_TRANSLATION_ALIAS', $lang->title));
                        return false;
                    }
                }
            }
        }

        return true;
    }

    /**
     * Publish method (yeah this is sad but we are on J!3)
     */
    public function publish($pks = null, $state = 1, $userId = 0) {

        $k = $this->_tbl_key;

        // sanitize input
        JArrayHelper::toInteger($pks);
        $userId = (int) $userId;
        $state = (int) $state;

        // if there are no primary keys set check if the instance key is set already
        if (empty($pks)) {
            if ($this->$k) {
                $pks = array($this->$k);
            } else {
                $this->setError(JText::_('JLIB_DATABASE_ERROR_NO_ROWS_SELECTED'));
                return false;
            }
        }

        // get table instance
        $table = JTable::getInstance('Tag', 'JKitTable');

        foreach ($pks as $pk) {

            // Load the item
            if (!$table->load($pk)) {
                $this->setError($table->getError());
            }

            $table->state = $state;

            // store the row
            if (!$table->store()) {
                $this->setError($table->getError());
            }
        }

        return count($this->getErrors()) == 0;
    }

}

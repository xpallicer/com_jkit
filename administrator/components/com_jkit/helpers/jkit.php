<?php
/**
 * @package     JKit
 * @subpackage  com_jkit
 * @copyright   Copyright (C) 2013 - 2014 CloudHotelier. All rights reserved.
 * @license     GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 * @link        http://www.cloudhotelier.com
 * @author      Xavier Pallicer <xpallicer@gmail.com>
 */
// no direct access
defined('_JEXEC') or die;

/**
 * JKit Helper
 */
class JKitHelper {

    /**
     * Return component views
     */
    public static function getViews() {
        return array('items', 'images', 'users', 'categories', 'tags', 'types');
    }

    /**
     * Create Submenu
     */
    public static function addSubmenu($vName) {

        JHtmlSidebar::addEntry(JText::_('COM_JKIT_VIEW_PANEL'), 'index.php?option=com_jkit&view=panel', $vName == 'panel');

        foreach (self::getViews() as $view) {
            if ($view != 'categories') {
                JHtmlSidebar::addEntry(JText::_('COM_JKIT_VIEW_' . strtoupper($view)), 'index.php?option=com_jkit&view=' . $view, $vName == $view);
            } else {
                JHtmlSidebar::addEntry(JText::_('COM_JKIT_VIEW_CATEGORIES'), 'index.php?option=com_categories&extension=com_jkit', $vName == 'categories');
            }
        }
    }

    /**
     * Toolbar Helper
     */
    public static function getToolbar($view = '', $show = false, $title = false) {

        // set view
        $view = $view ? $view : JRequest::getWord('view', 'panel');

        // view type
        if (substr($view, -1) == 's' || $view == 'panel') {
            $controller = substr($view, 0, -1);
            $controller_list = $view;
            $list = true;
            self::addSubmenu($view);
        } else {
            $controller = $view;
            $controller_list = $view . 's';
            $list = false;
        }

        // load assets
        JHtml::_('behavior.framework');
        JHtml::_('behavior.formvalidation');
        JHtml::_('bootstrap.framework');
        JHtml::script(JUri::base() . 'components/com_jkit/assets/jkit.js');
        JHtml::stylesheet(JUri::base() . 'components/com_jkit/assets/jkit.css');

        // page title
        $title = $title ? $title : $title = JText::_('COM_JKIT_VIEW_' . strtoupper($view));
        JToolBarHelper::title('JKit: ' . $title, $view);
        JFactory::getDocument()->setTitle('JKit: ' . $title);

        // buttons
        if ($list && $view != 'panel') {

            // new item
            JToolBarHelper::custom($controller . '.edit', 'new.png', 'new_f2.png', 'JTOOLBAR_NEW', false);

            // publish state
            if ($view != 'types') {
                JToolBarHelper::custom($controller_list . '.publish', 'publish.png', 'publish_f2.png', 'JTOOLBAR_PUBLISH', true);
                JToolBarHelper::custom($controller_list . '.unpublish', 'unpublish.png', 'unpublish_f2.png', 'JTOOLBAR_UNPUBLISH', true);
                JToolBarHelper::archiveList($controller_list . '.archive', 'JTOOLBAR_ARCHIVE');
            }

            // delete / trash
            if ($show || $view == 'types') {
                JToolBarHelper::custom($controller_list . '.delete', 'delete.png', 'delete_f2.png', 'JTOOLBAR_DELETE', true);
            } else {
                JToolBarHelper::trash($controller_list . '.trash', 'JTOOLBAR_TRASH');
            }
        }

        if ($view == 'panel') {
            JToolBarHelper::custom('item.edit', 'new.png', 'new_f2.png', 'COM_JKIT_PANEL_NEWITEM', false);
            JToolBarHelper::preferences('com_jkit');
        }

        if (!$list) {
            JToolBarHelper::apply($controller . '.apply', 'JTOOLBAR_APPLY');
            JToolBarHelper::save($controller . '.save');
            if ($view != 'translation') {
                JToolBarHelper::custom($controller . '.save2new', 'save-new.png', 'save-new_f2.png', 'JTOOLBAR_SAVE_AND_NEW', false);
                if ($show) {
                    JToolBarHelper::custom($controller . '.save2copy', 'save-copy.png', 'save-copy_f2.png', 'JTOOLBAR_SAVE_AS_COPY', false);
                }
            } else {
                JToolBarHelper::custom($controller . '.delete', 'delete.png', 'delete_f2.png', 'JTOOLBAR_DELETE', false);
            }
            JToolBarHelper::cancel($controller . '.cancel');
            JRequest::setVar('hidemainmenu', true);
        }


        if ($view == 'items') {

            // Instantiate a new JLayoutFile instance and render the batch button
            $layout = new JLayoutFile('joomla.toolbar.batch');
            $dhtml = $layout->render(array('title' => JText::_('JTOOLBAR_BATCH')));

            // add the button
            JToolBar::getInstance('toolbar')->appendButton('Custom', $dhtml, 'batch');
        }
    }

    public static function defaultTypes() {
        $types = array(
            1 => 'STANDARD',
            2 => 'PAGE',
            3 => 'POST',
            4 => 'IMAGE',
            5 => 'VIDEO',
            6 => 'LINK',
            7 => 'ATTACHMENT',
            8 => 'QUOTE',
            9 => 'GALLERY',
            10 => 'EVENT',
            11 => 'CONTACT',
            12 => 'PRODUCT'
        );
        return $types;
    }

    /**
     * ControlGroup for param
     * @param type $params
     * @param type $field
     * @return type
     */
    public static function getControlGroup($params, $field) {
        return '<div class="control-group"><div class="control-label">' . $params['jform_params_' . $field]->label . '</div><div class="controls">' . $params['jform_params_' . $field]->input . '</div></div>';
    }

    /**
     * Render item featured switcher
     * @param type $featured
     * @param type $i
     * @return type
     */
    public static function renderFeatured($featured, $i) {
        $class = $featured ? 'featured' : 'unfeatured';
        $action = $featured ? 'unfeatured' : 'featured';
        $click = 'onclick="return listItemTask(\'cb' . $i . '\',\'items.' . $action . '\')"';
        return '<a href="#" ' . $click . ' class="btn btn-micro hasTooltip" title="" data-original-title="' . JText::_('COM_JKIT_ITEM_FEATURED_TOGGLE') . '"><i class="icon-' . $class . '"></i></a>';
    }

    /**
     * Render a translation text input control-group 
     * @param $field
     * @param $original
     * @param $translation
     * @param $label
     * @param $api_key
     */
    public static function renderTranslateText($field, $original, $translation, $label, $api_key) {
        ?>
        <div class="control-group">
            <label class="control-label"><?php echo $label; ?></label>
            <div class="controls">
                <input type="text" name="" id="<?php echo $field; ?>_original" value="<?php echo htmlspecialchars($original); ?>" disabled class="input-xlarge original" >
                <input type="text" name="<?php echo $field; ?>" id="<?php echo $field; ?>" value="<?php echo htmlspecialchars($translation); ?>" class="input-xlarge" >
                <a href="javascript:;" class="btn btn-mini copy" rel="<?php echo $field; ?>"><?php echo JText::_('COM_JKIT_TRANSLATION_COPY'); ?></a>
                <?php if ($api_key) : ?><a href="javascript:;" class="btn btn-mini translate" rel="<?php echo $field; ?>"><?php echo JText::_('COM_JKIT_TRANSLATION_TRANSLATE'); ?></a><?php endif; ?>
            </div>
        </div>
        <?php
    }

    /**
     * Render a translation textarea control-group 
     * @param $field
     * @param $original
     * @param $translation
     * @param $label
     * @param $api_key
     */
    public static function renderTranslateTextArea($field, $original, $translation, $label, $api_key) {
        ?>
        <div class="control-group">
            <label class="control-label"><?php echo $label; ?></label>
            <div class="controls">
                <textarea name="" id="<?php echo $field; ?>_original" disabled class="input-xlarge original" ><?php echo htmlspecialchars($original); ?></textarea>
                <textarea name="<?php echo $field; ?>" id="<?php echo $field; ?>" class="input-xlarge" ><?php echo htmlspecialchars($translation); ?></textarea>
                <a href="javascript:;" class="btn btn-mini copy" rel="<?php echo $field; ?>"><?php echo JText::_('COM_JKIT_TRANSLATION_COPY'); ?></a>
                <?php if ($api_key) : ?><a href="javascript:;" class="btn btn-mini translate" rel="<?php echo $field; ?>"><?php echo JText::_('COM_JKIT_TRANSLATION_TRANSLATE'); ?></a><?php endif; ?>
            </div>
        </div>
        <?php
    }

    /**
     * Render a translation editor control-group
     * @param $field
     * @param $original
     * @param $translation
     * @param $label
     * @param $api_key
     */
    public static function renderTranslateEditor($field, $original, $translation, $label, $api_key) {
        $editor = JFactory::getEditor();
        ?>
        <div class="control-group jkit-translation-editor">
            <label class="control-label"><?php echo $label; ?></label>
            <div class="controls">
                <textarea name="" id="<?php echo $field; ?>_original" disabled class="input-xxlarge original" ><?php echo htmlspecialchars($original); ?></textarea>
                <a href="javascript:;" class="btn btn-mini copy" rel="<?php echo $field; ?>"><?php echo JText::_('COM_JKIT_TRANSLATION_COPY'); ?></a>
                <?php if ($api_key) : ?><a href="javascript:;" class="btn btn-mini translate" rel="<?php echo $field; ?>"><?php echo JText::_('COM_JKIT_TRANSLATION_TRANSLATE'); ?></a><?php endif; ?>
                <div class="jkit-editor">
                    <?php echo $editor->display($field, @htmlspecialchars($translation), '700px', '300px', '20', '20', false, null, null, null, array('html_height' => 300)); ?>
                </div>
            </div>
        </div>
        <?php
    }

}

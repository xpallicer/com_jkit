<?php

/**
 * @package     JKit
 * @subpackage  com_jkit
 * @copyright   Copyright (C) 2013 - 2014 CloudHotelier. All rights reserved.
 * @license     GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 * @link        http://www.cloudhotelier.com
 * @author      Xavier Pallicer <xpallicer@gmail.com>
 */
// no direct access
defined('_JEXEC') or die;

/**
 * Language Helper
 */
class JKitHelperLangs {

    /**
     * Get current website's content languages
     */
    static function getLangs() {
        $db = JFactory::getDbo();
        $query = 'SELECT * FROM `#__languages` WHERE `published` = 1 ORDER BY `ordering`';
        return $db->setQuery($query)->loadObjectList();
    }

    /**
     * Get current website's content languages
     */
    static function getLangTitle($code) {
        $db = JFactory::getDbo();
        $query = 'SELECT `title` FROM `#__languages` WHERE `lang_code` = ' . $db->quote($code);
        return $db->setQuery($query)->loadResult();
    }

    /**
     * Build the list header with the languages to translate
     */
    static function listHeader($langs, $view = 'items') {
        $html = '';
        $path = JURI::root() . 'media/mod_languages/images/';
        $width = $view == 'items' ? 'width: 10%; ' : 'width: 20%; ';
        foreach ($langs as $lang) {
            $html .= '<th class="nowrap center" style="' . $width . '">' . "\n";
            $html .= '  <img alt="' . $lang->title_native . '" src="' . $path . $lang->image . '.gif' . '" title="' . $lang->title_native . '" />' . "\n";
            $html .= '</th>' . "\n";
        }
        return $html;
    }

    /**
     * Build the list link to languages translations
     */
    static function listItem($langs, $translations, $item) {
        $html = '';
        foreach ($langs as $lang) {

            if ($item->language == '*') {

                $html .='<td class="small center">-</td>';
            } else {

                $disabled = '';
                $state = 'missing';
                $link = JRoute::_('index.php?option=com_jkit&task=translation.edit&id=' . $item->id . '&lang=' . $lang->lang_code);
                if ($item->language == $lang->lang_code) {
                    $state = 'original';
                    $link = 'javascript:;';
                    $disabled = 'disabled';
                } else {
                    if (!empty($translations)) {
                        foreach ($translations as $translation) {
                            if ($translation->ref_id == $item->id && $translation->lang == $lang->lang_code) {
                                $state = 'ok';
                                if ($item->modified > $translation->created) {
                                    $state = 'check';
                                }
                            }
                        }
                    }
                }
                $html .='<td class="small center"><a class="btn btn-mini ' . $disabled . ' translation-' . $state . '" href="' . $link . '">' . JText::_('COM_JKIT_ANY_TRANSLATION_' . strtoupper($state)) . '</a></td>';
            }
        }
        return $html;
    }

    /**
     * Build the list language status for tag
     */
    static function listTag($langs, $translations, $item) {

        $html = '';

        foreach ($langs as $lang) {

            $state_text = false;
            $state = 'missing';
            $link = JRoute::_('index.php?option=com_jkit&task=tag.edit&id=' . $item->id);

            if ($item->language == '*') {
                $html .= '<td class="center nowrap">-</td>';
            } else {
                if ($item->language == $lang->lang_code) {
                    $state = 'original';
                } else {
                    if (!empty($translations)) {
                        foreach ($translations as $translation) {
                            if ($translation->ref_id == $item->id && $translation->lang == $lang->lang_code) {
                                $item->translation = $translation->title;
                                $state = 'ok';
                                $state_text = $translation->title;
                            }
                        }
                    }
                }
                $text = $state_text ? $state_text : JText::_('COM_JKIT_ANY_TRANSLATION_' . strtoupper($state));
                $html .= '<td class="center nowrap"><a class="translation-' . $state . '" href="' . $link . '">' . $text . '</a></td>';
            }
        }
        return $html;
    }

    /**
     * Get the translations for a items and a table
     */
    static function getTranslations($items, $table = 'items') {

        if (!$items) {
            return array();
        }

        $ids = array();
        foreach ($items as $item) {
            $ids[] = $item->id;
        }

        $db = JFactory::getDbo();
        $table = $db->quote($table);
        $query = "SELECT * FROM `#__jkit_translations` WHERE `ref_table` = $table AND `ref_id` IN(" . implode(',', $ids) . ")";
        $translations = $db->setQuery($query)->loadObjectList();

        return $translations;
    }

}

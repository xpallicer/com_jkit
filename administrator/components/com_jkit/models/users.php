<?php

/**
 * @package     JKit
 * @subpackage  com_jkit
 * @copyright   Copyright (C) 2013 - 2014 CloudHotelier. All rights reserved.
 * @license     GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 * @link        http://www.cloudhotelier.com
 * @author      Xavier Pallicer <xpallicer@gmail.com>
 */
// no direct access
defined('_JEXEC') or die;

/**
 * Users Model
 */
class JKitModelUsers extends JModelList {

    /**
     * Text prefix
     */
    protected $text_prefix = 'COM_JKIT_USERS';

    /**
     * Constructor
     */
    public function __construct($config = array()) {
        if (empty($config['filter_fields'])) {
            $config['filter_fields'] = array(
                'a.id',
                'a.created',
                'a.state',
                'b.registerDate'
            );
        }

        parent::__construct($config);
    }

    /**
     * State
     */
    protected function populateState($ordering = null, $direction = null) {

        $filters = array('search', 'state');

        foreach ($filters as $filter) {
            $var = $this->getUserStateFromRequest($this->context . '.filter.' . $filter, 'filter_' . $filter);
            $this->setState('filter.' . $filter, $var);
        }

        parent::populateState('a.created', 'DESC');
    }

    /**
     * Filters
     */
    protected function getStoreId($id = '') {
        $id .= ':' . $this->getState('filter.search');
        $id .= ':' . $this->getState('filter.state');
        return parent::getStoreId($id);
    }

    /**
     * Query
     */
    protected function getListQuery() {

        // main query
        $query = $this->_db->getQuery(true);
        $query->select('a.*');
        $query->from('#__jkit_users AS a');

        // join over the users
        $query->select('b.name, b.email, b.registerDate, b.lastvisitDate');
        $query->join('LEFT', '#__users AS b ON b.id = a.jid');

        // join over the users for the checked out user
        $query->select('uc.name AS editor');
        $query->join('LEFT', '#__users AS uc ON uc.id=a.checked_out_time');

        // filter by state
        $state = $this->getState('filter.state');
        if (is_numeric($state)) {
            $query->where('a.state = ' . (int) $state);
        } else if ($state != '*') {
            $query->where('a.state IN (0, 1)');
        }

        // filter by search in name
        $search = $this->getState('filter.search');
        if (!empty($search)) {
            if (stripos($search, 'id:') === 0) {
                $query->where('a.id = ' . (int) substr($search, 3));
            } else {
                $search = $this->_db->Quote('%' . $this->_db->escape($search, true) . '%');
                $query->where('(a.title LIKE ' . $search . ' OR a.first_name LIKE ' . $search . ' OR a.last_name LIKE ' . $search . ' OR b.email LIKE ' . $search . ')');
            }
        }

        // add the list ordering clause
        $orderCol = $this->state->get('list.ordering');
        $orderDirn = $this->state->get('list.direction');
        $query->order($this->_db->escape($orderCol . ' ' . $orderDirn));

        return $query;
    }

}

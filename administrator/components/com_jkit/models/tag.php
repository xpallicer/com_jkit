<?php

/**
 * @package     JKit
 * @subpackage  com_jkit
 * @copyright   Copyright (C) 2013 - 2014 CloudHotelier. All rights reserved.
 * @license     GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 * @link        http://www.cloudhotelier.com
 * @author      Xavier Pallicer <xpallicer@gmail.com>
 */
// no direct access
defined('_JEXEC') or die;

/**
 * Tag Model
 */
class JKitModelTag extends JModelAdmin {

    /**
     * text prefix
     */
    protected $text_prefix = 'COM_JKIT_TAG';

    /**
     * Get the table
     */
    public function getTable($type = 'Tag', $prefix = 'JKitTable', $config = array()) {
        return JTable::getInstance($type, $prefix, $config);
    }

    /**
     * Get the form
     */
    public function getForm($data = array(), $loadData = true) {
        $form = $this->loadForm('com_jkit.tag', 'tag', array('control' => 'jform', 'load_data' => $loadData));
        if (empty($form)) {
            return false;
        }
        return $form;
    }

    /**
     * Get the data that should be injected in the form.
     */
    protected function loadFormData() {

        $data = JFactory::getApplication()->getUserState('com_jkit.edit.tag.data', array());

        if (!$data) {

            $data = $this->getItem();

            // default values
            if (!$data->id) {
                $filters = JFactory::getApplication()->getUserState('com_jkit.tags.filter');
                $default = $filters['language'] ? $filters['language'] : '*';
                $data->set('language', $default);
            }
        }

        return $data;
    }

    /**
     * Override save method to add tag translations
     */
    public function save($data) {

        // perform default save
        $save = parent::save($data);
        if (!$save) {
            return false;
        }

        // delete current translations
        $tag_id = $this->getState('tag.id');
        $this->_db->setQuery("DELETE FROM #__jkit_translations WHERE `ref_table` = 'tags' AND `ref_id` = $tag_id")->query();

        // get the language
        $table = $this->getTable();
        $table->load(($tag_id));

        // store translations
        if ($table->language != '*') {

            // prepare data
            $langs = JKitHelperLangs::getLangs();
            $values = array();
            foreach ($langs as $lang) {

                // get translations preventing tag own language transaltion
                if (JRequest::getString('translation_title_' . $lang->lang_code) && $lang->lang_code != $table->language) {
                    $title = JRequest::getString('translation_title_' . $lang->lang_code);
                    $alias = JRequest::getString('translation_alias_' . $lang->lang_code);
                    if (!$alias) {
                        $alias = $title;
                    }
                    $safe_alias = JApplication::stringURLSafe($alias);
                    $values[] = "('tags', $tag_id, " . $this->_db->quote($lang->lang_code) . ", " . $this->_db->quote($title) . ", " . $this->_db->quote($safe_alias) . ")";
                }
            }

            // insert translations
            if (count($values)) {
                $this->_db->setQuery('INSERT INTO #__jkit_translations (ref_table, ref_id, lang, title, alias) VALUES ' . implode(',', $values))->query();
            }
        }

        return true;
    }

    /**
     * Override delete method to delete item tags and translations also
     */
    public function delete(&$pks) {

        // standard joomla delete
        $delete = parent::delete($pks);
        if (!$delete) {
            return false;
        }

        // delete tag translations
        $this->_db->setQuery("DELETE FROM `#__jkit_translations` WHERE `ref_table` = 'tags' AND `ref_id` IN(" . implode(',', $pks) . ")")->query();

        // delete tags from item
        $this->_db->setQuery('DELETE FROM `#__jkit_tags_item` WHERE `tag_id` IN(' . implode(',', $pks) . ')')->query();

        return true;
    }

}

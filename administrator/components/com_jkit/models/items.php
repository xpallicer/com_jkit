<?php

/**
 * @package     JKit
 * @subpackage  com_jkit
 * @copyright   Copyright (C) 2013 - 2014 CloudHotelier. All rights reserved.
 * @license     GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 * @link        http://www.cloudhotelier.com
 * @author      Xavier Pallicer <xpallicer@gmail.com>
 */
// no direct access
defined('_JEXEC') or die;

/**
 * Items Model
 */
class JKitModelItems extends JModelList {

    /**
     * Text prefix
     */
    protected $text_prefix = 'COM_JKIT_ITEMS';

    /**
     * Constructor
     */
    public function __construct($config = array()) {
        if (empty($config['filter_fields'])) {
            $config['filter_fields'] = array(
                'a.id',
                'a.created',
                'a.modified',
                'a.title',
                'a.alias',
                'a.state',
                'a.ordering',
                'a.featured',
                'c.title',
                'l.title',
                't.title',
                'u.title'
            );
        }
        parent::__construct($config);
    }

    /**
     * State
     */
    protected function populateState($ordering = null, $direction = null) {

        $filters = array('search', 'state', 'catid', 'type', 'user', 'language');

        foreach ($filters as $filter) {
            $var = $this->getUserStateFromRequest($this->context . '.filter.' . $filter, 'filter_' . $filter);
            $this->setState('filter.' . $filter, $var);
        }
        parent::populateState('a.created', 'DESC');
    }

    /**
     * Filters
     */
    protected function getStoreId($id = '') {
        $id .= ':' . $this->getState('filter.search');
        $id .= ':' . $this->getState('filter.state');
        $id .= ':' . $this->getState('filter.catid');
        $id .= ':' . $this->getState('filter.type');
        $id .= ':' . $this->getState('filter.user');
        $id .= ':' . $this->getState('filter.language');
        return parent::getStoreId($id);
    }

    /**
     * The List Query
     */
    protected function getListQuery() {

        // main query
        $query = $this->_db->getQuery(true);
        $query->select('a.*');
        $query->from('#__jkit_items AS a');

        // joins
        $query->select('c.title AS category')->join('LEFT', '#__categories AS c ON c.id = a.catid');
        $query->select('t.title AS type')->join('LEFT', '#__jkit_types AS t ON t.id = a.type_id');
        $query->select('u.title AS user')->join('LEFT', '#__jkit_users AS u ON u.id = a.user_id');
        $query->select('l.title AS language_title')->join('LEFT', '#__languages AS l ON l.lang_code = a.language');
        $query->select('COUNT(i.id) AS images')->join('LEFT', '#__jkit_images AS i ON i.item_id = a.id')->group('a.id');

        // checked out
        $query->select('uc.name AS editor');
        $query->join('LEFT', '#__users AS uc ON uc.id=a.checked_out_time');

        // state filter
        $state = $this->getState('filter.state');
        if (is_numeric($state)) {
            $query->where('a.state = ' . (int) $state);
        } else if ($state != '*') {
            $query->where('a.state IN (0,1)');
        }

        // search filter
        $search = $this->getState('filter.search');
        if (!empty($search)) {
            if (stripos($search, 'id:') === 0) {
                $query->where('a.id = ' . (int) substr($search, 3));
            } else {
                $search = $this->_db->Quote('%' . $this->_db->escape($search, true) . '%');
                $query->where('(a.title LIKE ' . $search . ')');
            }
        }

        // category filter
        $filter_catid = $this->getState('filter.catid');
        if (is_numeric($filter_catid)) {
            // get category
            $cat_tbl = JTable::getInstance('Category', 'JTable');
            $cat_tbl->load($filter_catid);
            $rgt = $cat_tbl->rgt;
            $lft = $cat_tbl->lft;
            $query->where('c.lft >= ' . (int) $lft);
            $query->where('c.rgt <= ' . (int) $rgt);
        }

        // type filter
        $filter_type = (int) $this->getState('filter.type');
        if ($filter_type) {
            $query->where("(a.type_id = $filter_type)");
        }

        // language filter
        $filter_language = $this->getState('filter.language');
        if ($filter_language) {
            $language = $this->_db->quote($filter_language);
            $query->where("(a.language = $language)");
        }

        // other standard filters
        foreach (array('user') as $filter_name) {
            $filter_value = $this->getState('filter.' . $filter_name);
            if (is_numeric($filter_value)) {
                $query->where('a.' . $filter_name . '_id = ' . $filter_value);
            }
        }

        // ordering clause
        $orderCol = $this->state->get('list.ordering');
        $orderDirn = $this->state->get('list.direction');
        if ($orderCol == 'a.ordering') {
            $query->order($this->_db->escape("c.title $orderDirn, a.ordering $orderDirn"));
        } else {
            $query->order($this->_db->escape("$orderCol $orderDirn"));
        }

        return $query;
    }

}

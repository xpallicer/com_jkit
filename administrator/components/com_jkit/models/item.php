<?php

/**
 * @package     JKit
 * @subpackage  com_jkit
 * @copyright   Copyright (C) 2013 - 2014 CloudHotelier. All rights reserved.
 * @license     GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 * @link        http://www.cloudhotelier.com
 * @author      Xavier Pallicer <xpallicer@gmail.com>
 */
// no direct access
defined('_JEXEC') or die;

/**
 * Item Model
 */
class JKitModelItem extends JModelAdmin {

    /**
     * Text prefix
     */
    protected $text_prefix = 'COM_JKIT_ITEM';

    /**
     * Get the table
     */
    public function getTable($type = 'Item', $prefix = 'JKitTable', $config = array()) {
        return JTable::getInstance($type, $prefix, $config);
    }

    /**
     * get the form
     */
    public function getForm($data = array(), $loadData = true) {
        $form = $this->loadForm('com_jkit.item', 'item', array('control' => 'jform', 'load_data' => $loadData));
        if (empty($form)) {
            return false;
        }
        return $form;
    }

    /**
     * Get the data that should be injected in the form.
     */
    protected function loadFormData() {

        $data = JFactory::getApplication()->getUserState('com_jkit.edit.item.data', array());

        if (!$data) {

            $data = $this->getItem();

            // default values
            if ($this->getState('item.id') == 0) {

                // default catid and language
                $filters = JFactory::getApplication()->getUserState('com_jkit.items.filter');
                $data->set('catid', $filters['catid']);
                $data->set('language', $filters['language']);

                // default current user if defined
                $userid = $this->_db->setQuery('SELECT `id` FROM `#__jkit_users` WHERE `jid` = ' . JFactory::getUser()->get('id'))->loadResult();
                $data->set('user_id', $userid);
            } else {
                // set tags
                $tags = $this->getItemTagList();
                $data->tags = implode(',', $tags);
            }
        }

        return $data;
    }

    /**
     * Get Tags
     */
    public function getTags($key = 'id') {

        if (!isset($this->tags)) {

            $db = $this->getDbo();
            $item = $this->getItem();

            // get all the tags (for typeahead list)
            $query_tags = $db->getQuery(true)->select('a.id, a.title, a.alias')->from('#__jkit_tags AS a')->order('a.title');
            $this->tags = $db->setQuery($query_tags)->loadObjectList($key);

            // check for tags translations
            if (count($this->tags) && $item->language != '*') {

                // get translations
                $lang = $db->quote($item->language);
                $query_translations = $db->getQuery(true)->select('*')->from('#__jkit_translations')->where("ref_table = 'tags'")->where("lang = $lang");
                $translations = $db->setQuery($query_translations)->loadObjectList();

                // apply translations
                if (count($translations)) {
                    foreach ($this->tags as $k => $tag) {
                        foreach ($translations as $translation) {
                            if ($translation->ref_id == $tag->id) {
                                $tag->title = $translation->title;
                                $tag->alias = $translation->alias;
                                // update tag key
                                if ($key == 'alias') {
                                    unset($this->tags[$k]);
                                    $this->tags[$translation->alias] = $tag;
                                }
                            }
                        }
                    }
                }
            }
        }

        return $this->tags;
    }

    /**
     * Get Types
     */
    public function getTypes() {

        if (!isset($this->types)) {

            $this->types = array();

            $item = $this->getItem();
            $types = $this->_getList('SELECT `id`, `title`, `params` FROM `#__jkit_types` ORDER BY `id`');
            $this->item_type = $types[0];
            foreach ($types as $type) {
                $type->params = json_decode($type->params);
                if ($type->id == $item->type_id) {
                    $this->item_type = $type;
                }
            }
            $this->types = $types;
        }

        return $this->types;
    }

    /**
     * Get Item Type
     */
    public function getItemType() {

        if (!isset($this->item_type)) {
            $this->getTypes();
        }

        return $this->item_type;
    }

    /**
     * Get item Tags
     */
    public function getItemTags($key = 'id') {

        if (!isset($this->item_tags)) {

            $this->item_tags = array();

            if (!$this->getState('item.id')) {
                return $this->item_tags;
            }

            // get item tags ids
            $query = $this->_db->getQuery(true)->select('a.tag_id')->from('#__jkit_tags_item AS a')->where('a.item_id = ' . $this->getState('item.id'));
            $tags_ids = $this->_db->setQuery($query)->loadColumn();

            // load tags
            if (count($tags_ids)) {
                $tags = $this->getTags();
                if (count($tags)) {
                    foreach ($tags_ids as $id) {
                        if (isset($tags[$id])) {
                            $this->item_tags[$tags[$id]->$key] = $tags[$id];
                        }
                    }
                }
            }
        }
        return $this->item_tags;
    }

    /**
     * getTagList
     */
    public function getTagList() {

        if (!isset($this->tag_list)) {

            $this->tag_list = array();
            $tags = $this->getTags();
            if (count($tags)) {
                foreach ($tags as $tag) {
                    $this->tag_list[] = $tag->title;
                }
            }
        }

        return $this->tag_list;
    }

    /**
     * getItemTagsList
     */
    public function getItemTagList() {

        if (!isset($this->item_tag_list)) {

            $this->item_tag_list = array();
            $this->item_tag_ids = array();
            $tags = $this->getItemTags();
            if (count($tags)) {
                foreach ($tags as $tag) {
                    $this->item_tag_list[] = $tag->title;
                    $this->item_tag_ids[] = $tag->id;
                }
            }
        }

        return $this->item_tag_list;
    }

    /**
     * getItemTagsList
     */
    public function getItemTagIds() {

        if (!isset($this->item_tag_ids)) {
            $this->getItemTagList();
        }

        return $this->item_tag_ids;
    }

    /**
     * Extend save method to add the image and tag processing
     */
    public function save($data) {

        // get tags data
        $tags = array_map('trim', explode(",", $data['tags']));

        // save item (preventing joomla default operation with tags)
        $data['tags'] = null;
        $save = parent::save($data);
        if (!$save) {
            return false;
        }

        // get the item id
        $item_id = $this->getState('item.id');
        if (!$item_id) {
            return false;
        }

        // save tags (only when saving in the same language or a new item)
        if (!JRequest::getString('original_language') || JRequest::getString('original_language') == $data['language']) {
            if (count($tags) > 0) {
                $this->saveTags($tags);
            }
        }

        // save or delete image
        $imageHelper = new JKitHelperImage(JComponentHelper::getParams('com_jkit'));
        $file = $_FILES['image'];
        if ($file['size']) {
            $imageHelper->uploadImage($file, $item_id, 'items');
        } else {
            if (JRequest::getInt('image_delete')) {
                $imageHelper->deleteImage($item_id, 'items');
            }
        }

        return true;
    }

    /**
     * Save Tags
     */
    private function saveTags($tags) {

        // get the item
        $item = $this->getItem();

        // prepare tags
        $clean_tags = array();
        foreach ($tags as $tag) {

            // clean tag
            $title = trim(preg_replace('/\s+/', ' ', $tag));

            // prepare tag
            if ($title) {
                $alias = JApplication::stringURLSafe($title);
                if (trim(str_replace('-', '', $alias)) != '' && !isset($clean_tags[$alias])) {
                    // clean tags array
                    $clean_tag = new stdClass();
                    $clean_tag->alias = $alias;
                    $clean_tag->title = $title;
                    $clean_tags[$alias] = $clean_tag;
                }
            }
        }

        if ($clean_tags) {

            // get existing tags
            $prev_tags = $this->getTags('alias');

            // get existing tags ids, while adding new tags if necessary
            $tags_ids = array();
            $language = $this->_db->quote($item->language);
            foreach ($clean_tags as $tag) {

                // existing tag
                if (isset($prev_tags[$tag->alias])) {
                    $tags_ids[] = $prev_tags[$tag->alias]->id;
                }
                // add a new tag
                else {
                    $title = $this->_db->quote($tag->title);
                    $talias = $this->_db->quote($tag->alias);
                    $this->_db->setQuery("INSERT INTO `#__jkit_tags` (`language`, `title`, `alias`, `state`) VALUES ($language, $title, $talias, 1)")->query();
                    $tags_ids[] = $this->_db->insertid();
                }
            }
        }

        // delete all prev item tags
        $this->_db->setQuery('DELETE FROM `#__jkit_tags_item` WHERE `item_id` = ' . $item->id)->query();

        // add new tags
        if (isset($tags_ids)) {
            if (count($tags_ids)) {
                $values = array();
                foreach ($tags_ids as $tag_id) {
                    $values[] = "($item->id, $tag_id)";
                }
                $this->_db->setQuery('INSERT INTO `#__jkit_tags_item` (`item_id`, `tag_id`) VALUES ' . implode(',', $values))->query();
            }
        }
    }

    /**
     * Override delete method to add the image processing
     */
    public function delete(&$pks) {

        // standard joomla delete
        $delete = parent::delete($pks);
        if (!$delete) {
            return false;
        }

        // delete images
        $imageHelper = new JKitHelperImage(JComponentHelper::getParams('com_jkit'));
        foreach ($pks as $pk) {

            // delete item image file
            $imageHelper->deleteImage($pk, 'items');

            // delete item gallery images
            $query_images = $this->_db->getQuery(true)->select('id')->from('#__jkit_images')->where("`item_id` = " . (int) $pk);
            $images_ids = $this->_db->setQuery($query_images)->loadColumn();
            if (count($images_ids)) {
                // delete images files
                foreach ($images_ids as $image_id) {
                    $imageHelper->deleteImage($image_id, 'images');
                }
                // db delete images
                $query_delete_images = $this->_db->getQuery(true)->delete('#__jkit_images')->where("`item_id` = " . (int) $pk);
                $this->_db->setQuery($query_delete_images)->query();
            }
        }

        // delete item translations
        $this->_db->setQuery("DELETE FROM `#__jkit_translations` WHERE `ref_table` = 'items' AND `ref_id` IN(" . implode(',', $pks) . ")")->query();

        // delete item tags
        $this->_db->setQuery('DELETE FROM `#__jkit_tags_item` WHERE `item_id` IN(' . implode(',', $pks) . ')')->query();

        return true;
    }

    /**
     * Set reordering conditions
     */
    protected function getReorderConditions($table) {
        $condition = array();
        $condition[] = 'catid = ' . (int) $table->catid;
        return $condition;
    }

    /**
     * Featured toggler
     */
    public function featured($pks, $value = 0) {

        // sanitize pks
        $pks = (array) $pks;
        JArrayHelper::toInteger($pks);

        if (empty($pks)) {
            $this->setError(JText::_('COM_JKIT_ANY_ERROR_NOITEMS'));
            return false;
        }

        try {
            $db = $this->getDbo();
            $query = $db->getQuery(true)
                    ->update($db->quoteName('#__jkit_items'))
                    ->set('`featured` = ' . (int) $value)
                    ->where('`id` IN (' . implode(',', $pks) . ')');
            $db->setQuery($query);
            $db->execute();
        } catch (Exception $e) {
            $this->setError($e->getMessage());
            return false;
        }

        $this->cleanCache();

        return true;
    }

}

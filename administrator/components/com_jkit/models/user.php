<?php

/**
 * @package     JKit
 * @subpackage  com_jkit
 * @copyright   Copyright (C) 2013 - 2014 CloudHotelier. All rights reserved.
 * @license     GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 * @link        http://www.cloudhotelier.com
 * @author      Xavier Pallicer <xpallicer@gmail.com>
 */
// no direct access
defined('_JEXEC') or die;

/**
 * User Model
 */
class JKitModelUser extends JModelAdmin {

    /**
     * Prefix
     */
    protected $text_prefix = 'COM_JKIT_USER';

    /**
     * Get the table
     */
    public function getTable($type = 'User', $prefix = 'JKitTable', $config = array()) {
        return JTable::getInstance($type, $prefix, $config);
    }

    /**
     * get the form
     */
    public function getForm($data = array(), $loadData = true) {
        $form = $this->loadForm('com_jkit.user', 'user', array('control' => 'jform', 'load_data' => $loadData));
        if (empty($form)) {
            return false;
        }
        return $form;
    }

    /**
     * Get the data that should be injected to the form
     */
    protected function loadFormData() {
        $data = JFactory::getApplication()->getUserState('com_jkit.edit.user.data', array());
        if (empty($data)) {
            $data = $this->getItem();
        }

        return $data;
    }

    /**
     * Override get Item Method to Join J! User
     */
    public function getItem($pk = null) {

        $pk = (!empty($pk)) ? $pk : (int) $this->getState($this->getName() . '.id');

        if ($pk > 0) {
            // main query
            $query = $this->_db->getQuery(true);
            $query->select('a.*')->from('#__jkit_users AS a');
            $query->select('b.name, b.username, b.email, b.registerDate, b.lastvisitDate');
            $query->leftJoin('#__users AS b ON b.id = a.jid');
            $query->where("a.id = $pk");
            $this->_db->setQuery($query);
            $result = $this->_db->loadAssoc();
            return JArrayHelper::toObject($result, 'JObject');
        } else {
            return parent::getItem();
        }
    }

    /**
     * Extend save method to add J! User actions
     */
    public function save($data) {

        // get the table
        $table = $this->getTable();

        // check isNew
        $key = $table->getKeyName();
        $pk = (!empty($data[$key])) ? $data[$key] : (int) $this->getState($this->getName() . '.id');
        $isNew = true;
        if ($pk > 0) {
            $table->load($pk);
            $isNew = false;
        }

        // bind
        if (!$table->bind($data)) {
            $this->setError($table->getError());
            return false;
        }

        // check
        if (!$table->check()) {
            $this->setError($table->getError());
            return false;
        }

        // check if the user exists
        $jid = $table->jid ? $table->jid : 0;

        // check, save or update joomla user before store
        $juser = $this->saveJoomlaUser($jid, $data);
        if (!$juser) {
            return false;
        }

        // save jkit user
        $table->jid = $juser->id;
        if (!$table->store()) {
            $this->setError($table->getError());
            // TODO delete prev created joomla user
            return false;
        }

        // set state
        $pkName = $table->getKeyName();
        if (isset($table->$pkName)) {
            $this->setState($this->getName() . '.id', $table->$pkName);
        }
        $this->setState($this->getName() . '.new', $isNew);

        // save or delete image
        $imageHelper = new JKitHelperImage(JComponentHelper::getParams('com_jkit'));
        $file = $_FILES['image'];
        if ($file['size']) {
            $imageHelper->uploadImage($file, $this->getState('user.id'), 'users');
        } else {
            if (JRequest::getInt('image_delete')) {
                $imageHelper->deleteImage($this->getState('user.id'), 'users');
            }
        }

        return true;
    }

    /**
     * Create or update a joomla user
     */
    private function saveJoomlaUser($jid, $data) {

        // prepare to save    
        $juser_data = array();

        // get the user
        $juser = JUser::getInstance($jid);
        if ($jid) {
            $juser_data['groups'] = false; // do not update groups
        } else {
            $params = JComponentHelper::getParams('com_users');
            $juser_data['groups'][1] = $params->get('new_usertype', 2); // set default user group
        }

        // save email, username & name
        $juser_data['email'] = $data['email'];
        if ($data['password']) {
            $juser_data['password'] = $data['password'];
            $juser_data['password1'] = $data['password'];
            $juser_data['password2'] = $data['password'];
        }
        $juser_data['username'] = $data['username'];
        $juser_data['name'] = $data['first_name'] . ' ' . $data['last_name'];

        // bind joomla user the data
        if (!$juser->bind($juser_data)) {
            $this->setError($juser->getError());
            return false;
        }

        // save joomla user data
        if (!$juser->save()) {
            $this->setError($juser->getError());
            return false;
        }

        return $juser;
    }

}

<?php

/**
 * @package     JKit
 * @subpackage  com_jkit
 * @copyright   Copyright (C) 2013 - 2014 CloudHotelier. All rights reserved.
 * @license     GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 * @link        http://www.cloudhotelier.com
 * @author      Xavier Pallicer <xpallicer@gmail.com>
 */
// no direct access
defined('_JEXEC') or die;

/**
 * Images Model
 */
class JKitModelImages extends JModelList {

    /**
     * Text prefix
     */
    protected $text_prefix = 'COM_JKIT_IMAGES';

    /**
     * Constructor
     */
    public function __construct($config = array()) {
        if (empty($config['filter_fields'])) {
            $config['filter_fields'] = array(
                'a.id',
                'a.created',
                'a.title',
                'a.state',
                'a.ordering',
                'i.title',
                'c.title',
                'l.title',
                'u.title'
            );
        }
        parent::__construct($config);
    }

    /**
     * State
     */
    protected function populateState($ordering = null, $direction = null) {

        $filters = array('search', 'state', 'item', 'catid', 'user', 'language');

        foreach ($filters as $filter) {
            $var = $this->getUserStateFromRequest($this->context . '.filter.' . $filter, 'filter_' . $filter);
            $this->setState('filter.' . $filter, $var);
        }

        // reset item filter on filter category change
        if (JRequest::getInt('filter_catid')) {
            $app = JFactory::getApplication();
            if (JRequest::getInt('filter_catid') != $app->getUserState($this->context . '.filter.catid')) {
                $this->setState('filter.item', '');
                $app->setUserState($this->context . '.filter.item', '');
            }
        }

        parent::populateState('a.ordering', 'asc');
    }

    /**
     * Filters
     */
    protected function getStoreId($id = '') {
        $id .= ':' . $this->getState('filter.search');
        $id .= ':' . $this->getState('filter.state');
        $id .= ':' . $this->getState('filter.item');
        $id .= ':' . $this->getState('filter.catid');
        $id .= ':' . $this->getState('filter.user');
        $id .= ':' . $this->getState('filter.language');
        return parent::getStoreId($id);
    }

    /**
     * Query
     */
    protected function getListQuery() {

        // main query
        $query = $this->_db->getQuery(true);
        $query->select('a.*');
        $query->from('#__jkit_images AS a');

        // joins
        $query->select('i.title AS item')->join('LEFT', '#__jkit_items AS i ON i.id = a.item_id');
        $query->select('c.title AS category')->join('LEFT', '#__categories AS c ON c.id = i.catid');
        $query->select('u.title AS user')->join('LEFT', '#__jkit_users AS u ON u.id = i.user_id');
        $query->select('l.title AS language_title')->join('LEFT', '#__languages AS l ON l.lang_code = i.language');

        // checked out
        $query->select('uc.name AS editor');
        $query->join('LEFT', '#__users AS uc ON uc.id=a.checked_out_time');

        // state filter
        $state = $this->getState('filter.state');
        if (is_numeric($state)) {
            $query->where('a.state = ' . (int) $state);
        } else if ($state != '*') {
            $query->where('a.state IN (0,1)');
        }

        // search filter
        $search = $this->getState('filter.search');
        if (!empty($search)) {
            if (stripos($search, 'id:') === 0) {
                $query->where('a.id = ' . (int) substr($search, 3));
            } else {
                $search = $this->_db->Quote('%' . $this->_db->escape($search, true) . '%');
                $query->where('(a.title LIKE ' . $search . ')');
            }
        }

        // category filter
        $filter_catid = $this->getState('filter.catid');
        if (is_numeric($filter_catid)) {
            // get category
            $cat_tbl = JTable::getInstance('Category', 'JTable');
            $cat_tbl->load($filter_catid);
            $rgt = $cat_tbl->rgt;
            $lft = $cat_tbl->lft;
            $query->where('c.lft >= ' . (int) $lft);
            $query->where('c.rgt <= ' . (int) $rgt);
        }

        // other standard filters
        foreach (array('item') as $filter_name) {
            $filter_value = $this->getState('filter.' . $filter_name);
            if (is_numeric($filter_value)) {
                $query->where('a.' . $filter_name . '_id = ' . $filter_value);
            }
        }

        // ordering clause
        $orderCol = $this->state->get('list.ordering');
        $orderDirn = $this->state->get('list.direction');
        if ($orderCol == 'a.ordering') {
            $query->order($this->_db->escape("i.title $orderDirn, a.ordering $orderDirn"));
        } else {
            $query->order($this->_db->escape("$orderCol $orderDirn"));
        }

        return $query;
    }

}

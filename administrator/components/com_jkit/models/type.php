<?php

/**
 * @package     JKit
 * @subpackage  com_jkit
 * @copyright   Copyright (C) 2013 - 2014 CloudHotelier. All rights reserved.
 * @license     GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 * @link        http://www.cloudhotelier.com
 * @author      Xavier Pallicer <xpallicer@gmail.com>
 */
// no direct access
defined('_JEXEC') or die;

/**
 * Type Model
 */
class JKitModelType extends JModelAdmin {

    /**
     * Text prefix
     */
    protected $text_prefix = 'COM_JKIT_TYPE';

    /**
     * Get the table
     */
    public function getTable($type = 'Type', $prefix = 'JKitTable', $config = array()) {
        return JTable::getInstance($type, $prefix, $config);
    }

    /**
     * get the form
     */
    public function getForm($data = array(), $loadData = true) {
        $form = $this->loadForm('com_jkit.item', 'type', array('control' => 'jform', 'load_data' => $loadData));
        if (empty($form)) {
            return false;
        }
        return $form;
    }

    /**
     * Get the data that should be injected to the form
     */
    protected function loadFormData() {

        $data = JFactory::getApplication()->getUserState('com_jkit.edit.item.data', array());

        if (!$data) {
            $data = $this->getItem();
        }

        return $data;
    }

    /**
     * Override delete method to avoid deletion of default types and types that have items
     */
    public function delete(&$pks) {

        // check ids
        foreach ($pks as $i => $pk) {

            // default types
            if ($pk < 100) {
                $this->setError(JText::_('COM_JKIT_TYPE_ERROR_DELETE_DEFAULT'));
                return false;
            }

            // check items
            $items = $this->_getList('SELECT `id`, `title` FROM `#__jkit_items` WHERE `type_id` IN(' . implode(',', $pks) . ')');
            if (count($items)) {
                $this->setError(JText::_('COM_JKIT_TYPE_ERROR_DELETE_CURRENT'));
                return false;
            }
        }

        // standard joomla delete
        $delete = parent::delete($pks);
        if (!$delete) {
            return false;
        }

        return true;
    }

}

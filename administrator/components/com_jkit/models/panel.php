<?php

/**
 * @package     JKit
 * @subpackage  com_jkit
 * @copyright   Copyright (C) 2013 - 2014 CloudHotelier. All rights reserved.
 * @license     GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 * @link        http://www.cloudhotelier.com
 * @author      Xavier Pallicer <xpallicer@gmail.com>
 */
// no direct access
defined('_JEXEC') or die;

/**
 * Panel Model
 */
class JKitModelPanel extends JModelLegacy {

    /**
     * Get JKit categories
     */
    public function getCategories() {

        if (!isset($this->categories)) {

            // query base
            $query = $this->_db->getQuery(true);
            $query->select('a.*')->from('#__categories AS a');
            $query->where("a.extension = " . $this->_db->quote('com_jkit'));
            $query->where('a.published = 1');

            // category language
            $query->leftJoin('#__languages AS l ON l.lang_code = a.language');

            // order by language, category order
            $query->order('l.ordering, a.lft');

            $this->categories = $this->_getList($query);
        }

        return $this->categories;
    }

    /**
     * Get site languages
     */
    public function getLangs() {

        if (!isset($this->langs)) {

            $langs = JKitHelperLangs::getLangs();

            $this->langs['*'] = JText::_('COM_JKIT_PANEL_CATEGORIES');
            foreach ($langs as $lang) {
                $this->langs[$lang->lang_code] = $lang->title;
            }
        }

        return $this->langs;
    }

    /**
     * Get latest edited items
     */
    public function getEdited() {

        if (!isset($this->edited)) {
            $query = $this->getQueryItems();
            $query->order('a.modified DESC');
            $this->edited = $this->_getList($query, 0, 5);
        }

        return $this->edited;
    }

    /**
     * Get recently added items
     */
    public function getAdded() {

        if (!isset($this->added)) {
            $query = $this->getQueryItems();
            $query->order('a.id DESC');
            $this->added = $this->_getList($query, 0, 5);
        }

        return $this->added;
    }

    /**
     * Items Query
     */
    private function getQueryItems() {
        $query = $this->_db->getQuery(true);
        $query->select('a.*')->from('#__jkit_items AS a');
        $query->select('c.title AS category')->join('LEFT', '#__categories AS c ON c.id = a.catid');
        $query->where('a.state IN (0,1)');
        return $query;
    }

    /**
     * Get recently added items
     */
    public function getImages() {

        if (!isset($this->images)) {
            $query = $this->_db->getQuery(true);
            $query->select('a.*')->from('#__jkit_images AS a');
            $query->select('i.title AS item, i.catid AS item_catid')->join('LEFT', '#__jkit_items AS i ON i.id = a.item_id');
            $query->where('a.state IN (0,1)')->where('i.state IN (0,1)');
            $query->order('a.modified DESC');
            $this->images = $this->_getList($query, 0, 5);
        }

        return $this->images;
    }

    /**
     * Get recently added items
     */
    public function getTranslations() {

        if (!isset($this->translations)) {
            $query = $this->_db->getQuery(true);
            $query->select('a.*')->from('#__jkit_translations AS a')->where("a.ref_table = 'items'");
            $query->select('i.title AS item')->join('LEFT', '#__jkit_items AS i ON i.id = a.ref_id');
            $query->where('i.state IN (0,1)');
            $query->order('a.created DESC');
            $this->translations = $this->_getList($query, 0, 5);
        }

        return $this->translations;
    }

}

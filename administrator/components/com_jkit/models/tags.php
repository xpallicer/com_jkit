<?php

/**
 * @package     JKit
 * @subpackage  com_jkit
 * @copyright   Copyright (C) 2013 - 2014 CloudHotelier. All rights reserved.
 * @license     GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 * @link        http://www.cloudhotelier.com
 * @author      Xavier Pallicer <xpallicer@gmail.com>
 */
// no direct access
defined('_JEXEC') or die;

/**
 * Tags Model
 */
class JKitModelTags extends JModelList {

    /**
     * Text prefix
     */
    protected $text_prefix = 'COM_JKIT_TAGS';

    /**
     * Constructor
     */
    public function __construct($config = array()) {
        if (empty($config['filter_fields'])) {
            $config['filter_fields'] = array(
                'a.id',
                'a.created',
                'a.title',
                'a.alias',
                'a.state'
            );
        }
        parent::__construct($config);
    }

    /**
     * State
     */
    protected function populateState($ordering = null, $direction = null) {

        $filters = array('search', 'state', 'language');

        foreach ($filters as $filter) {
            $var = $this->getUserStateFromRequest($this->context . '.filter.' . $filter, 'filter_' . $filter);
            $this->setState('filter.' . $filter, $var);
        }

        parent::populateState('a.title', 'asc');
    }

    /**
     * Filters
     */
    protected function getStoreId($id = '') {
        $id .= ':' . $this->getState('filter.search');
        $id .= ':' . $this->getState('filter.state');
        $id .= ':' . $this->getState('filter.language');
        return parent::getStoreId($id);
    }

    /**
     * Query
     */
    protected function getListQuery() {

        // main query
        $query = $this->_db->getQuery(true);
        $query->select('a.*');
        $query->from('#__jkit_tags AS a');

        // state filter
        $state = $this->getState('filter.state');
        if (is_numeric($state)) {
            $query->where('a.state = ' . (int) $state);
        } else if ($state != '*') {
            $query->where('a.state IN (0,1)');
        }

        // search filter
        $search = $this->getState('filter.search');
        if (!empty($search)) {
            if (stripos($search, 'id:') === 0) {
                $query->where('a.id = ' . (int) substr($search, 3));
            } else {
                $search = $this->_db->Quote('%' . $this->_db->escape($search, true) . '%');
                $query->where('(a.title LIKE ' . $search . ')');
            }
        }

        // language filter
        $filter_language = $this->getState('filter.language');
        if ($filter_language) {
            $language = $this->_db->quote($filter_language);
            $query->where("(a.language = $language)");
        }

        // ordering clause
        $orderCol = $this->state->get('list.ordering');
        $orderDirn = $this->state->get('list.direction');
        $query->order($this->_db->escape("$orderCol $orderDirn"));

        return $query;
    }

}

<?php

/**
 * @package     JKit
 * @subpackage  com_jkit
 * @copyright   Copyright (C) 2013 - 2014 CloudHotelier. All rights reserved.
 * @license     GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 * @link        http://www.cloudhotelier.com
 * @author      Xavier Pallicer <xpallicer@gmail.com>
 */
// no direct access
defined('_JEXEC') or die;

// load helper
JFormHelper::loadFieldClass('list');

/**
 * JKitType
 */
class JFormFieldJKitType extends JFormFieldList {

    /**
     * Filter name
     */
    protected $type = 'JKitType';

    /**
     * Get the options
     */
    public function getOptions() {

        // query
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('a.id AS value, a.title AS text');
        $query->from('#__jkit_types AS a');
        $query->where('a.id > 100');
        $query->order('a.title');

        $options = $db->setQuery($query)->loadObjectList();

        return array_merge(parent::getOptions(), $options);
    }

}

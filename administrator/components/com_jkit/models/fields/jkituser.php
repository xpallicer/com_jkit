<?php

/**
 * @package     JKit
 * @subpackage  com_jkit
 * @copyright   Copyright (C) 2013 - 2014 CloudHotelier. All rights reserved.
 * @license     GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 * @link        http://www.cloudhotelier.com
 * @author      Xavier Pallicer <xpallicer@gmail.com>
 */
// no direct access
defined('_JEXEC') or die;

jimport('joomla.form.formfield');

/**
 * JKitUser
 */
class JFormFieldJKitUser extends JFormField {

    protected $type = 'JKitUser';

    public function getInput() {

        // load language files
        $language = JFactory::getLanguage();
        $base_dir = JPATH_BASE . '/components/com_jkit';
        $language->load('com_jkit', JPATH_SITE, 'en-GB', true);
        $language->load('com_jkit', $base_dir, 'en-GB', true);
        $language->load('com_jkit', JPATH_SITE);
        $language->load('com_jkit', $base_dir);

        // get options
        $db = JFactory::getDbo();
        $query = 'SELECT `id`, `title` FROM `#__jkit_users` ORDER BY `title`';
        $items = $db->setQuery($query)->loadObjectList();

        // generate select
        $default = new stdClass();
        $default->id = '';
        $default->title = JText::_('COM_JKIT_ANY_SELECT_USER');
        return JHtml::_('select.genericlist', array_merge(array($default), $items), $this->name, 'class="inputbox"', 'id', 'title', $this->value, $this->id);
    }

}
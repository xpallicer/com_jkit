<?php

/**
 * @package     JKit
 * @subpackage  com_jkit
 * @copyright   Copyright (C) 2013 - 2014 CloudHotelier. All rights reserved.
 * @license     GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 * @link        http://www.cloudhotelier.com
 * @author      Xavier Pallicer <xpallicer@gmail.com>
 */
// no direct access
defined('_JEXEC') or die;

/**
 * JKit Item Modal Picker
 */
class JFormFieldModal_Item extends JFormField {

    /** @var string Form field type */
    protected $type = 'Modal_Item';

    /**
     * Method to get the field input markup
     */
    public function getInput() {

        // load language files
        $base_dir = JPATH_ADMINISTRATOR . '/components/com_jkit';
        JFactory::getLanguage()->load('com_jkit', $base_dir, 'en-GB', true);
        JFactory::getLanguage()->load('com_jkit', $base_dir);

        // load the javascript
        JHtml::_('behavior.framework');
        JHtml::_('behavior.modal', 'a.modal');
        JHtml::_('bootstrap.tooltip');

        // select button script
        $script = array();
        $script[] = '	function jkitSelectItem(id, name, object) {';
        $script[] = '		document.id("' . $this->id . '_id").value = id;';
        $script[] = '		document.id("' . $this->id . '_name").value = name;';
        $script[] = '		SqueezeBox.close();';
        $script[] = '	}';
        JFactory::getDocument()->addScriptDeclaration(implode("\n", $script));

        // Setup variables for display
        $html = array();
        $link = 'index.php?option=com_jkit&amp;view=items&amp;layout=modal&amp;tmpl=component';

        // get options
        $db = JFactory::getDbo();
        $query = $db->getQuery(true)->select('title')->from('#__jkit_items')->where('id=' . (int) $this->value);
        $title = $db->setQuery($query)->loadResult();
        if (empty($title)) {
            $title = JText::_('COM_JKIT_ANY_SELECT_ITEM');
        }
        $title = htmlspecialchars($title, ENT_QUOTES, 'UTF-8');

        // Current item input field
        $html[] = '<span class="input-append">';
        $html[] = '<input type="text" class="input-medium" id="' . $this->id . '_name" value="' . $title . '" disabled="disabled" />';
        $html[] = '<a class="modal btn btn-primary hasTooltip" title="' . JText::_('COM_JKIT_ANY_SELECT_ITEM') . '" href="' . $link . '" rel="{handler: \'iframe\', size: {x: 800, y: 450}}"><i class="icon-file"></i> ' . JText::_('COM_JKIT_ANY_SELECT_ITEM_BUTTON') . '</a>';
        $html[] = '</span>';

        // active item id field
        if (0 == (int) $this->value) {
            $value = '';
        } else {
            $value = (int) $this->value;
        }

        // class='required' for client side validation
        $class = '';
        if ($this->required) {
            $class = ' class="required modal-value"';
        }

        $html[] = '<input type="hidden" id="' . $this->id . '_id"' . $class . ' name="' . $this->name . '" value="' . $value . '" />';

        return implode("\n", $html);
    }

}

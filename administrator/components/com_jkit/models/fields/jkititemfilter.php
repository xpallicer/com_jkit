<?php

/**
 * @package     JKit
 * @subpackage  com_jkit
 * @copyright   Copyright (C) 2013 - 2014 CloudHotelier. All rights reserved.
 * @license     GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 * @link        http://www.cloudhotelier.com
 * @author      Xavier Pallicer <xpallicer@gmail.com>
 */
// no direct access
defined('_JEXEC') or die;

// load helper
JFormHelper::loadFieldClass('list');

/**
 * KitItemFilter
 */
class JFormFieldJKitItemFilter extends JFormFieldList {

    /**
     * Filter name
     */
    protected $type = 'JKitItemFilter';

    /**
     * Get the options
     */
    public function getOptions() {

        // query
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('a.id AS value, a.title AS text');
        $query->from('#__jkit_items AS a');
        $query->where('a.state IN (0,1)');
        $query->order('a.title');

        // joins
        $query->select('c.title AS category')->join('LEFT', '#__categories AS c ON c.id = a.catid');

        // category filter
        $filters = JFactory::getApplication()->getUserState('com_jkit.images.filter');
        $filter_catid = $filters['catid'];
        if (is_numeric($filter_catid)) {
            // get category
            $cat_tbl = JTable::getInstance('Category', 'JTable');
            $cat_tbl->load($filter_catid);
            $rgt = $cat_tbl->rgt;
            $lft = $cat_tbl->lft;
            $query->where('c.lft >= ' . (int) $lft);
            $query->where('c.rgt <= ' . (int) $rgt);
        }

        $options = $db->setQuery($query)->loadObjectList();

        return array_merge(parent::getOptions(), $options);
    }

}

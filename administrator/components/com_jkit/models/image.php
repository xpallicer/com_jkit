<?php

/**
 * @package     JKit
 * @subpackage  com_jkit
 * @copyright   Copyright (C) 2013 - 2014 CloudHotelier. All rights reserved.
 * @license     GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 * @link        http://www.cloudhotelier.com
 * @author      Xavier Pallicer <xpallicer@gmail.com>
 */
// no direct access
defined('_JEXEC') or die;

/**
 * Image Model
 */
class JKitModelImage extends JModelAdmin {

    /**
     * Text prefix
     */
    protected $text_prefix = 'COM_JKIT_IMAGE';

    /**
     * Get the table
     */
    public function getTable($type = 'Image', $prefix = 'JKitTable', $config = array()) {
        return JTable::getInstance($type, $prefix, $config);
    }

    /**
     * get the form
     */
    public function getForm($data = array(), $loadData = true) {
        $form = $this->loadForm('com_jkit.image', 'image', array('control' => 'jform', 'load_data' => $loadData));
        if (empty($form)) {
            return false;
        }
        return $form;
    }

    /**
     * Get the data that should be injected to the form
     */
    protected function loadFormData() {

        $data = JFactory::getApplication()->getUserState('com_jkit.edit.image.data', array());

        if (empty($data)) {

            $data = $this->getItem();

            // default values
            if ($this->getState('image.id') == 0) {
                $filters = JFactory::getApplication()->getUserState('com_jkit.images.filter');
                $data->set('item_id', $filters['item']);
                $data->set('created', '-');
            }

            // item
            $item_table = JTable::getInstance('Item', 'JKitTable');
            $item_table->load($data->item_id);
            $data->set('item', $item_table->title);
        }

        return $data;
    }

    /**
     * Override save method to add the image processing
     */
    public function save($data) {

        // perform default save
        $save = parent::save($data);
        if (!$save) {
            return false;
        }

        // save or delete image
        $imageHelper = new JKitHelperImage(JComponentHelper::getParams('com_jkit'));
        $file = $_FILES['image'];
        if ($file['size']) {
            $imageHelper->uploadImage($file, $this->getState('image.id'), 'images');
        } else {
            if (JRequest::getInt('image_delete')) {
                $imageHelper->deleteImage($this->getState('image.id'), 'images');
            }
        }

        return true;
    }

    /**
     * Override delete method to add the image processing
     */
    public function delete(&$pks) {

        // standard joomla delete
        $delete = parent::delete($pks);
        if (!$delete) {
            return false;
        }

        // delete images
        $imageHelper = new JKitHelperImage(JComponentHelper::getParams('com_jkit'));
        foreach ($pks as $pk) {
            $imageHelper->deleteImage($pk, 'images');
        }

        return true;
    }

    /**
     * Set reordering conditions
     */
    protected function getReorderConditions($table) {
        $condition = array();
        $condition[] = 'item_id = ' . (int) $table->item_id;
        return $condition;
    }

    /**
     * get the item tags so it can be assigned to images
     */
    public function getTags() {

        // check item 
        $id = JRequest::getInt('id', 0);
        $filters = JFactory::getApplication()->getUserState('com_jkit.images.filter');
        $item_id = $filters['item'];
        if (!$id && !$item_id) {
            return array();
        } elseif ($id) {
            $item = $this->getItem();
            $item_id = $item->item_id;
        }

        $query = $this->_db->getQuery(true)->select('a.id, a.title')->from('#__jkit_tags AS a')->order('a.title ASC');
        $query->leftJoin('#__jkit_tags_item AS b ON a.id = b.tag_id')->where('b.item_id = ' . $item_id);
        return $this->_getList($query);
    }

}

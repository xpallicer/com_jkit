<?php

/**
 * @package     JKit
 * @subpackage  com_jkit
 * @copyright   Copyright (C) 2013 - 2014 CloudHotelier. All rights reserved.
 * @license     GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 * @link        http://www.cloudhotelier.com
 * @author      Xavier Pallicer <xpallicer@gmail.com>
 */
// no direct access
defined('_JEXEC') or die;

/**
 * Translation Model
 */
class JKitModelTranslation extends JModelLegacy {

    /**
     * Text prefix
     */
    protected $text_prefix = 'COM_JKIT_ITEM';

    /**
     * Get the item
     */
    public function getItem() {

        if (!isset($this->item)) {

            // prepare query
            $db = $this->_db;
            $id = JRequest::getInt('id', 0);

            // get the item
            $query = $db->getQuery(true)->select('a.*')->from('#__jkit_items AS a')->where("a.id = $id");
            $this->item = $db->setQuery($query)->loadObject();

            // params
            $this->item->params = $this->item ? json_decode($this->item->params) : array();
            if (count($this->item->params)) {
                foreach ($this->item->params as $paramt => $paramv) {
                    $paramn = 'params_' . $paramt;
                    $this->item->$paramn = $paramv;
                }
            }

            // get the item images
            $query_images = $db->getQuery(true)->select('*')->from('#__jkit_images')->where("item_id = $id");
            $this->item->images = $db->setQuery($query_images)->loadObjectList();
            foreach($this->item->images as $image){
                $image->params = json_decode($image->params);
            }

            // get the item tags (only those tags with the same original language as the item -that is- the tags that shoudl be translated)
            $query_tags = $db->getQuery(true)->select('a.*')->from('#__jkit_tags AS a')->where('a.language = ' . $db->quote($this->item->language))->where("b.item_id = $id");
            $query_tags->leftJoin('#__jkit_tags_item AS b ON b.tag_id = a.id');
            $this->item->tags = $db->setQuery($query_tags)->loadObjectList();
        }

        return $this->item;
    }

    /**
     * Get the translation
     */
    public function getTranslation() {

        if (!isset($this->translation)) {

            // check state (error saving)
            $data = JFactory::getApplication()->getUserState('com_jkit.translation.data', null);
            if ($data) {
                $data->data = json_decode($data->translation);
                $this->translation = $data;
                return $this->translation;
            }

            // prepare query
            $db = $this->_db;
            $id = JRequest::getInt('id', 0);

            // get translation
            $query = $db->getQuery(true)->select('a.*')->from('#__jkit_translations AS a');
            $query->where("a.ref_table = 'items'")->where('a.lang = ' . $db->quote(JRequest::getCmd('lang')));
            $query->where("a.ref_id = $id");
            $this->translation = $db->setQuery($query)->loadObject();

            if ($this->translation) {
                $strings = json_decode($this->translation->translation);
                $this->translation->data = new stdClass();
                foreach ($strings as $field => $value) {
                    $this->translation->data->$field = $value;
                }
            }

            // get tags translations
            $tags = $this->getItem()->tags;

            if (count($tags)) {

                // get tags ids
                $ids = array();
                foreach ($tags as $tag) {
                    $ids[] = $tag->id;
                }

                // query for translations
                $query_tags = $db->getQuery(true)->select('a.*')->from('#__jkit_translations AS a');
                $query_tags->where("a.ref_table = 'tags'")->where('a.lang = ' . $db->quote(JRequest::getCmd('lang')));
                $query_tags->where('a.ref_id IN(' . implode(',', $ids) . ')');
                $tags_translations = $db->setQuery($query_tags)->loadObjectList();

                // assign tags translations
                if (count($tags_translations)) {
                    if (!$this->translation) {
                        $this->translation = new stdClass();
                        $this->translation->data = new stdClass();
                    }
                    foreach ($tags as $tag) {
                        $field = 'tag_' . $tag->id;
                        $value = '';
                        foreach ($tags_translations as $tarnslation) {
                            if ($tarnslation->ref_id == $tag->id) {
                                $value = $tarnslation->title;
                            }
                        }
                        $this->translation->data->$field = $value;
                    }
                }
            }
        }

        return $this->translation;
    }

    /**
     * getTranslationId
     */
    private function getTranslationId() {
        $translation = $this->getTranslation();
        return $translation->id;
    }

    /**
     * Save
     */
    public function save() {

        // preapre 
        $db = $this->_db;
        $item = $this->getItem();
        $user = JFactory::getUser();

        // get main values
        $data = array();
        $data['ref_table'] = 'items';
        $data['ref_id'] = JRequest::getInt('id');
        $data['lang'] = JRequest::getCmd('lang');
        $data['title'] = trim(JRequest::getString('title'));
        $data['alias'] = JApplication::stringURLSafe(JRequest::getString('alias'));
        if (trim(str_replace('-', '', $data['alias'])) == '') {
            $data['alias'] = JApplication::stringURLSafe($data['title']);
        }

        // prepare an array with the rest of the translation strings
        $strings = array(
            'info',
            'intro',
            'body',
            'link',
            'params_link_text',
            'video',
            'params_location',
            'params_author',
            'params_source'
        );
        foreach ($item->images as $image) {
            $strings[] = 'image_' . $image->id;
            $strings[] = 'image_info_' . $image->id;
            $strings[] = 'image_params_link_' . $image->id;
            $strings[] = 'image_params_link_text_' . $image->id;
        }

        // get the translations for each string
        $translation = array();
        foreach ($strings as $string) {
            if (in_array($string, array('intro', 'body')) || substr($string, 0, 10) == 'image_info') {
                $translation[$string] = JRequest::getString($string, '', 'post', JREQUEST_ALLOWRAW);
            } else {
                $translation[$string] = JRequest::getString($string);
            }
        }

        // get tags translations
        if ($item->tags) {
            foreach ($item->tags as $tag) {
                $translation['tag_' . $tag->id] = JRequest::getString('tag_' . $tag->id);
            }
        }

        // encode translation
        $data['translation'] = json_encode($translation);

        // check the data
        $check = $this->check($data);
        if (!$check) {
            // store the data to model state (so it can be recovered if save fails)
            JFactory::getApplication()->setUserState('com_jkit.translation.data', json_decode(json_encode($data)));
            return false;
        }

        // delete current translation if exists
        $this->delete();

        // add the new translation
        $query = $db->getQuery(true)->insert('#__jkit_translations');
        $query->set('created = ' . $db->Quote(JFactory::getDate()->toSql()));
        $query->set('created_by = ' . $user->id);
        foreach ($data as $field => $value) {
            $query->set($db->quoteName($field) . ' = ' . $db->quote((string) $value));
        }
        $db->setQuery($query)->query();

        // tags translation
        if (count($item->tags)) {
            $this->saveTags();
        }

        return true;
    }

    /**
     * Update tags translations
     */
    public function saveTags() {

        // get the item tags 
        $tags = $this->getItem()->tags;

        // prepare insert
        $db = $this->_db;
        $values = array();
        $delete_ids = array();
        foreach ($tags as $tag) {
            $title = JRequest::getString('tag_' . $tag->id);
            $alias = JApplication::stringURLSafe($title);
            if ($alias) {
                $row = array();
                $row[] = $db->quote('tags');
                $row[] = $db->quote($tag->id);
                $row[] = $db->quote(JRequest::getCmd('lang'));
                $row[] = $db->quote($title);
                $row[] = $db->quote($alias);
                $values[] = '(' . implode(',', $row) . ')';
                $delete_ids[] = $tag->id;
            }
        }

        // replace tags translation 
        if (count($values)) {
            $query_delete = $db->getQuery(true)->delete('#__jkit_translations');
            $query_delete->where("ref_table = 'tags'")->where('ref_id IN(' . implode(',', $delete_ids) . ')')->where("lang = " . $db->quote(JRequest::getCmd('lang')));
            $db->setQuery($query_delete)->query();

            $query = 'INSERT INTO #__jkit_translations (ref_table, ref_id, lang, title, alias) VALUES ' . implode(',', $values);
            $db->setQuery($query)->query();
        }
    }

    /**
     * Delete a translation
     */
    public function delete() {

        // delete current translation if exists
        $db = $this->_db;
        $query_delete = $db->getQuery(true)->delete('#__jkit_translations')->where('id = ' . JRequest::getInt('tid', 0));
        $db->setQuery($query_delete)->query();

        return true;
    }

    /**
     * Check before save translation
     */
    private function check($data) {

        $db = JFactory::getDbo();
        $q_l = $db->quote($data['lang']);
        $q_a = $db->quote($data['alias']);

        // check item title
        if ($data['title'] == '') {
            $this->setError(JText::_('COM_JKIT_ANY_ERROR_NOTITLE'));
        }

        // check item alias
        else {

            $alias_ok = true;
            $data['alias'] = JApplication::stringURLSafe($data['alias']);
            if (trim(str_replace('-', '', $data['alias'])) == '') {
                $data['alias'] = JApplication::stringURLSafe($data['title']);
                if (trim(str_replace('-', '', $data['alias'])) == '') {
                    $alias_ok = false;
                    $this->setError(JText::_('COM_JKIT_ANY_ERROR_NOALIAS'));
                }
            }

            // check duplicated alias
            if ($alias_ok) {

                // check item unique alias (in translations)
                $q = "SELECT `ref_id` FROM `#__jkit_translations` WHERE `ref_table` = 'items' AND `alias` = $q_a AND `lang` = $q_l AND ref_id != " . (int) $data['ref_id'];
                $r = $db->setQuery($q)->loadColumn();
                if ($r) {
                    $this->setError(JText::_('COM_JKIT_ITEM_ALIAS_ERROR'));
                }

                // check duplicated alias in different languages
                else {

                    $q = "SELECT `id` FROM `#__jkit_items` WHERE `alias` = $q_a AND `language` = $q_l AND id != " . (int) $data['ref_id'];
                    $r = $db->setQuery($q)->loadColumn();
                    if ($r) {
                        $this->setError(JText::_('COM_JKIT_ITEM_ALIAS_ERROR'));
                    }
                }
            }
        }

        // check tags translations
        $tags = $this->getItem()->tags;
        foreach ($tags as $tag) {

            // check tag title
            $t_title = trim(JRequest::getString('tag_' . $tag->id));
            if ($t_title == '') {
                $this->setError(JText::sprintf('COM_JKIT_TAG_ERROR_TRANSLATION_TITLE', $tag->title));
            }

            // check tag alias
            else {
                $t_alias = JApplication::stringURLSafe($t_title);
                if (trim(str_replace('-', '', $t_alias)) == '') {
                    $this->setError(JText::sprintf('COM_JKIT_TAG_ERROR_TRANSLATION_ALIAS', $tag->title));
                }

                // check duplicated tags
                else {

                    // check duplicated tags (in translations)
                    $q_t_a = $db->quote($t_alias);
                    $q = "SELECT `ref_id` FROM `#__jkit_translations` WHERE `ref_table` = 'tags' AND `alias` = $q_t_a AND `lang` = $q_l AND `ref_id` != $tag->id";
                    $r = $db->setQuery($q)->loadColumn();
                    if ($r) {
                        $this->setError(JText::sprintf('COM_JKIT_TAG_ERROR_TRANSLATION_ALIAS', $tag->title));
                    }

                    // check duplicated tags in different languages
                    else {
                        $q = "SELECT `id` FROM `#__jkit_tags` WHERE `alias` = $q_t_a AND `language` = $q_l AND id != $tag->id";
                        $r = $db->setQuery($q)->loadColumn();
                        if ($r) {
                            $this->setError(JText::sprintf('COM_JKIT_TAG_ERROR_TRANSLATION_ALIAS', $tag->title));
                        }
                    }
                }
            }
        }

        if (!$this->getErrors()) {
            return true;
        } else {
            return false;
        }
    }

}

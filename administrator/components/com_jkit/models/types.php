<?php

/**
 * @package     JKit
 * @subpackage  com_jkit
 * @copyright   Copyright (C) 2013 - 2014 CloudHotelier. All rights reserved.
 * @license     GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 * @link        http://www.cloudhotelier.com
 * @author      Xavier Pallicer <xpallicer@gmail.com>
 */
// no direct access
defined('_JEXEC') or die;

/**
 * Types Model
 */
class JKitModelTypes extends JModelList {

    /**
     * Text prefix
     */
    protected $text_prefix = 'COM_JKIT_TYPES';

    /**
     * Constructor
     */
    public function __construct($config = array()) {
        if (empty($config['filter_fields'])) {
            $config['filter_fields'] = array(
                'a.id',
                'a.title'
            );
        }
        parent::__construct($config);
    }

    /**
     * State
     */
    protected function populateState($ordering = null, $direction = null) {

        $filters = array('search', 'custom');

        foreach ($filters as $filter) {
            $var = $this->getUserStateFromRequest($this->context . '.filter.' . $filter, 'filter_' . $filter);
            $this->setState('filter.' . $filter, $var);
        }
        parent::populateState('a.id', 'ASC');
    }

    /**
     * Filters
     */
    protected function getStoreId($id = '') {
        $id .= ':' . $this->getState('filter.search');
        $id .= ':' . $this->getState('filter.custom');
        return parent::getStoreId($id);
    }

    /**
     * The List Query
     */
    protected function getListQuery() {

        // main query
        $query = $this->_db->getQuery(true);
        $query->select('a.*');
        $query->from('#__jkit_types AS a');

        // checked out
        $query->select('uc.name AS editor');
        $query->join('LEFT', '#__users AS uc ON uc.id=a.checked_out_time');

        // state custom
        $custom = $this->getState('filter.custom');
        if (is_numeric($custom)) {
            $query->where($custom ? 'a.id > 100' : 'a.id < 100');
        }

        // search filter
        $search = $this->getState('filter.search');
        if (!empty($search)) {
            if (stripos($search, 'id:') === 0) {
                $query->where('a.id = ' . (int) substr($search, 3));
            } else {
                $search = $this->_db->Quote('%' . $this->_db->escape($search, true) . '%');
                $query->where('(a.title LIKE ' . $search . ')');
            }
        }

        // ordering clause
        $orderCol = $this->state->get('list.ordering');
        $orderDirn = $this->state->get('list.direction');
        $query->order($this->_db->escape("$orderCol $orderDirn"));

        return $query;
    }

}

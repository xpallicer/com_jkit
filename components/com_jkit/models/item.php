<?php

/**
 * @package     JKit
 * @subpackage  com_jkit
 * @copyright   Copyright (C) 2013 - 2014 CloudHotelier. All rights reserved.
 * @license     GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 * @link        http://www.cloudhotelier.com
 * @author      Xavier Pallicer <xpallicer@gmail.com>
 */
// No direct access
defined('_JEXEC') or die;

/**
 * Item Model
 */
class JKitModelItem extends JModelLegacy {

    /**
     * Get the item
     * Note: params used when loaded from modules
     */
    public function getItem($item_id = null, $prepare_content = true) {

        if (isset($this->item)) {
            return $this->item;
        }

        // helpers
        $db = $this->_db;
        $imageHelper = new JKitHelperImage(JComponentHelper::getParams('com_jkit'));

        // determine item id
        $id = $item_id ? $item_id : JRequest::getInt('id');

        // get the item
        $query_item = $db->getQuery(true)->select('a.*')->from('#__jkit_items AS a')
                ->where("a.id = $id")
                ->where('a.state = 1');
        $query_item->select('c.title AS category')->leftJoin('#__categories AS c ON c.id = a.catid');
        $query_item->select('u.title AS user, u.alias AS user_alias')->leftJoin('#__jkit_users AS u ON u.id = a.user_id');
        $this->item = $db->setQuery($query_item)->loadObject();

        // set item params and image
        $this->item->params = $this->item ? json_decode($this->item->params) : array();
        $this->item->image = $imageHelper->getImage($id, 'items');

        // get the item images
        $query_images = $db->getQuery(true)->select('*')->from('#__jkit_images')
                ->where("item_id = $id")
                ->where('state = 1')
                ->order('ordering');
        $images = $db->setQuery($query_images)->loadObjectList();
        foreach ($images as $image) {
            $image->params = json_decode($image->params);
            if (isset($image->params->tags)) {
                $image->params->tags = array_keys(get_object_vars($image->params->tags));
            }
        }
        $this->item->images = $images;

        // get the item tags
        $query_tags = $db->getQuery(true)->select('a.id, a.title, a.alias')
                ->from('#__jkit_tags AS a')
                ->innerJoin('#__jkit_tags_item AS t ON t.tag_id = a.id')
                ->where("a.state = 1")
                ->where("t.item_id = $id")
                ->order('a.id');
        $this->item->tags = $db->setQuery($query_tags)->loadObjectList();

        // get item translation
        $lang = $db->quote(JFactory::getLanguage()->getTag());
        $query_translation = $db->getQuery(true)->select('*')->from('#__jkit_translations')
                ->where("ref_table = 'items'")
                ->where("lang = $lang")
                ->where("ref_id = $id");
        $translation = $db->setQuery($query_translation)->loadObject();
        if ($translation) {
            $this->item = JKitHelper::applyTranslation($this->item, $translation);
        }

        // get tags translations
        $tags_ids = array();
        $tags_translations = array();
        foreach ($this->item->tags as $tag) {
            $tags_ids[] = $tag->id;
        }
        if ($tags_ids) {
            $query_tags_translations = $db->getQuery(true)->select('*')->from('#__jkit_translations')
                    ->where("ref_table = 'tags'")
                    ->where("lang = $lang")
                    ->where("ref_id IN (" . implode(',', $tags_ids) . ")");
            $tags_translations = $db->setQuery($query_tags_translations)->loadObjectList();
        }
        if ($tags_translations) {
            foreach ($this->item->tags as $i => $tag) {
                foreach ($tags_translations as $tag_translation) {
                    if ($tag->id == $tag_translation->ref_id) {
                        $this->item->tags[$i]->title = $tag_translation->title;
                        $this->item->tags[$i]->alias = $tag_translation->alias;
                    }
                }
            }
        }

        // prepare content
        if ($prepare_content) {
            $this->item->intro = JHtml::_('content.prepare', $this->item->intro);
            $this->item->body = JHtml::_('content.prepare', $this->item->body);
        }

        return $this->item;
    }

}

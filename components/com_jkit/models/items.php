<?php

/**
 * @package     JKit
 * @subpackage  com_jkit
 * @copyright   Copyright (C) 2013 - 2014 CloudHotelier. All rights reserved.
 * @license     GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 * @link        http://www.cloudhotelier.com
 * @author      Xavier Pallicer <xpallicer@gmail.com>
 */
// No direct access
defined('_JEXEC') or die;

/**
 * Items Model
 */
class JKitModelItems extends JModelList {

    /**
     * State
     */
    protected function populateState($ordering = null, $direction = null) {

        parent::populateState($ordering, $direction);

        // start and limit
        $start = JRequest::getUInt('start', 0);
        $this->setState('list.start', $start);
    }

    /**
     * The List Query
     * Note: params used when loaded from modules
     */
    function getListQuery($catids = null, $order = null, $limit = null) {

        // db helper
        $db = $this->_db;

        // params
        $app = JFactory::getApplication();
        $menu = $app->getMenu();
        $active = ($menu->getActive()) ? $menu->getActive() : $menu->getDefault();
        $params = $menu->getParams($active->id);

        // query
        $query = parent::getListQuery();
        $query->select('a.*')->from('#__jkit_items AS a')->where('a.state = 1');
        $query->select('c.title AS category')->leftJoin('#__categories AS c ON c.id = a.catid');
        $query->select('u.title AS user, u.alias AS user_alias')->leftJoin('#__jkit_users AS u ON u.id = a.user_id');

        // category
        $catids = $catids ? $catids : $params->get('categories');
        $categories = implode(',', (array) $catids);
        $query->where("a.catid IN ($categories)");

        // tag view?
        $tag = $this->getTag();
        if ($tag) {
            $query->leftJoin('#__jkit_tags_item AS t ON t.item_id = a.id');
            $query->where("t.tag_id = $tag->id");
        }

        // archive view?
        $archive = $this->getArchive();
        if ($archive) {
            $start = $db->quote($archive->start);
            $end = $db->quote($archive->end);
            $query->where("a.created >= $start");
            $query->where("a.created <= $end");
        }

        // author view?
        $author = $this->getAuthor();
        if ($author) {
            $query->where("a.user_id = $author->id");
        }

        // ordering
        $order = $order ? $order : $params->get('order', 'new');
        if ($order == 'new') {
            $ordering = 'a.created DESC';
        } elseif ($order == 'old') {
            $ordering = 'a.created ASC';
        } elseif ($order == 'asc') {
            $ordering = 'a.ordering ASC';
        } elseif ($order == 'desc') {
            $ordering = 'a.ordering DESC';
        }
        $this->setState('list.ordering', $ordering);
        $this->setState('list.direction', '');
        $query->order($ordering);

        // limit
        $limit = $limit ? $limit : $params->get('leading') + $params->get('primary') + $params->get('secondary');
        $this->setState('list.limit', $limit);

        return $query;
    }

    /**
     * Override getItems method to add images
     * @return boolean
     */
    public function getItems($catids = null, $order = null, $limit = null) {

        // Get a storage key
        $store = $this->getStoreId();

        // Try to load the data from internal storage.
        if (isset($this->cache[$store])) {
            return $this->cache[$store];
        }

        if ($catids) {
            // module query
            $query = $this->getListQuery($catids, $order, $limit);
        } else {
            // component list view query
            $query = $this->_getListQuery();
        }

        // state limit problems from module
        $limit = $limit ? $limit : $this->getState('list.limit');

        // get the list
        $items = $this->_getList($query, $this->getStart(), $limit);

        // load params, images and transaltions
        $this->tags = array();
        $this->tags_ids = array();
        if (count($items)) {
            $this->loadProperties($items);
            $this->loadImages($items);
            $this->loadTags($items);
            $this->loadTranslations($items);
        }

        // Check for a database error.
        if ($this->_db->getErrorNum()) {
            $this->setError($this->_db->getErrorMsg());
            return false;
        }

        // Add the items to the internal cache.
        $this->cache[$store] = $items;

        // Add the tags to the internal cache.
        $this->cache[$store . 'tags'] = $this->tags;

        return $this->cache[$store];
    }

    /**
     * Prepare items: decode params and preapre to assign tags and images
     * @param type $items
     * @return type
     */
    private function loadProperties($items) {

        // load image helper
        $imageHelper = new JKitHelperImage(JComponentHelper::getParams('com_jkit'));

        // get ids and arrange params
        $ids = array();
        foreach ($items as $item) {
            $ids[] = $item->id;
            $item->intro = JHtml::_('content.prepare', $item->intro);
            $item->body = JHtml::_('content.prepare', $item->body);
            $item->params = json_decode($item->params);
            $item->image = $imageHelper->getImage($item->id, 'items');
            $item->images = array();
            $item->tags = array();
        }

        // global items_ids
        $this->items_ids = $ids;

        return $items;
    }

    /**
     * Add gallery to items
     * @param type $items
     * @return type
     */
    private function loadImages($items) {

        // query for images
        $qids = implode(',', $this->items_ids);
        $query = $this->_db->getQuery(true)->select('a.*')->from('#__jkit_images AS a');
        $query->where("a.item_id IN ($qids)")->where('a.state = 1')->order('a.ordering');
        $images = $this->_getList($query);

        // assign item images
        if (count($images)) {
            foreach ($items as $item) {
                foreach ($images as $image) {
                    if ($image->item_id == $item->id) {
                        $item->images[] = $image;
                    }
                }
            }
        }

        return $items;
    }

    /**
     * Add tags to items
     * @param type $items
     * @return type
     */
    private function loadTags($items) {

        // get tags per item
        $qids = implode(',', $this->items_ids);
        $query_tags_items = $this->_db->getQuery(true)->select('a.*')->from('#__jkit_tags_item AS a')->where("a.item_id IN($qids)");
        $tags_items = $this->_db->setQuery($query_tags_items)->loadObjectList();

        // get tags ids
        if (count($tags_items)) {
            $t_ids = array();
            foreach ($tags_items as $row) {
                $t_ids[] = $row->tag_id;
            }
            $tags_ids = array_unique($t_ids);
        } else {
            return $items;
        }

        // set tags_ids
        $this->tags_ids = $tags_ids;

        // get tags info
        $tids = implode(',', $tags_ids);
        $query_tags = $this->_db->getQuery(true)->select('a.id, a.title, a.alias')->from('#__jkit_tags AS a')->where("a.id IN($tids)")->order('a.title');
        $tags = $this->_db->setQuery($query_tags)->loadObjectList('id');

        if (count($tags)) {
            foreach ($items as $item) {
                foreach ($tags_items as $tag_item) {
                    if ($item->id == $tag_item->item_id) {
                        if (isset($tags[$tag_item->tag_id])) {
                            $item->tags[] = $tags[$tag_item->tag_id]; // assign tag to item
                            if (!isset($this->tags[$tag_item->tag_id])) {
                                $this->tags[$tag_item->tag_id] = $tags[$tag_item->tag_id]; // add tag to list tags
                            }
                        }
                    }
                }
            }
        }

        return $items;
    }

    /**
     * Search for transaltions and override items strings
     * @param type $items
     * @return type
     */
    private function loadTranslations($items) {

        // get translations
        $lang = $this->_db->quote(JFactory::getLanguage()->getTag());

        // get items transaltions
        $qids = implode(',', $this->items_ids);
        $query = $this->_db->getQuery(true)->select('*')->from('#__jkit_translations')->where("ref_table = 'items'")->where("lang = $lang");
        $query->where("ref_id IN ($qids)");
        $translations = $this->_db->setQuery($query)->loadObjectList();

        // load tags transaltions
        $translations_tags = array();
        if (count($this->tags_ids)) {
            $tids = implode(',', $this->tags_ids);
            if ($tids) {
                $query_tags = $this->_db->getQuery(true)->select('*')->from('#__jkit_translations')->where("ref_table = 'tags'")->where("lang = $lang");
                $query_tags->where("ref_id IN ($tids)");
                $translations_tags = $this->_db->setQuery($query_tags)->loadObjectList();
            }
        }

        // assign items transaltions
        foreach ($items as $item) {
            if (count($translations)) {
                foreach ($translations as $translation) {
                    if ($item->id == $translation->ref_id) {
                        $item = JKitHelper::applyTranslation($item, $translation);
                    }
                }
            }
            if (count($translations_tags) && count($item->tags)) {
                foreach ($translations_tags as $translation) {
                    foreach ($item->tags as $tag) {
                        if ($tag->id == $translation->ref_id) {
                            $tag->title = $translation->title;
                            $tag->alias = $translation->alias;
                        }
                    }
                }
            }
            // reorder item tags
            usort($item->tags, array('JKitModelItems', 'reorderTranslatedTags'));
        }

        // assign tags translation
        // TODO: merge in the first loop, no need for 2 loops
        if (count($this->tags) && count($translations_tags)) {
            foreach ($this->tags as $tag) {
                foreach ($translations_tags as $translation) {
                    if ($tag->id == $translation->ref_id) {
                        $tag->title = $translation->title;
                        $tag->alias = $translation->alias;
                    }
                }
            }
            // reorder translation tags
            usort($this->tags, array('JKitModelItems', 'reorderTranslatedTags'));
        }

        return $items;
    }

    /**
     * Reorder transaltion tags
     */
    private function reorderTranslatedTags($a, $b) {
        return strcmp($a->title, $b->title);
    }

    /**
     * Get a list of all tags of the current items
     * @return type
     */
    public function getTags() {

        $store = $this->getStoreId();
        if (isset($this->cache[$store . 'tags'])) {
            return $this->cache[$store . 'tags'];
        }

        return $this->tags;
    }

    /**
     * Get tag data
     * TODO rewrite queries to sql object
     * @return type
     */
    public function getTag() {

        if (isset($this->tag)) {
            return $this->tag;
        }

        if (!JRequest::getCmd('tag')) {
            $this->tag = false;
            return false;
        }

        // get the tag id
        $tag = $this->_db->quote(JRequest::getCmd('tag'));
        $lang = $this->_db->quote(JFactory::getLanguage()->getTag());

        // check translations table first
        $tag_id_query_translation = "SELECT `ref_id` FROM `#__jkit_translations` WHERE `ref_table` = 'tags' AND `lang` = $lang AND `alias` = $tag";
        $tag_id = $this->_db->setQuery($tag_id_query_translation)->loadResult();
        if (!$tag_id) {
            $tag_id_query_original = "SELECT `id` FROM `#__jkit_tags` WHERE `alias` = $tag";
            $tag_id = $this->_db->setQuery($tag_id_query_original)->loadResult();
        }

        // tag found
        if (!$tag_id) {
            $this->tag = false;
            return false;
        }

        // get the tag info
        $tag_query = "SELECT `id`, `title`, `alias` FROM `#__jkit_tags` WHERE `id` = $tag_id";
        $this->tag = $this->_db->setQuery($tag_query)->loadObject();

        // get the tag translation
        $tag_transaltion_query = "SELECT *  FROM `#__jkit_translations` WHERE `ref_table` = 'tags' AND `lang` = $lang AND `ref_id` = $tag_id";
        $translation = $this->_db->setQuery($tag_transaltion_query)->loadObject();
        if ($translation) {
            $this->tag->title = $translation->title;
            $this->tag->alias = $translation->alias;
        }

        return $this->tag;
    }

    /**
     * Get the archive month info
     * @return type
     */
    public function getArchive() {

        if (isset($this->archive)) {
            return $this->archive;
        }

        if (!JRequest::getCmd('archive')) {
            $this->archive = false;
            return false;
        }

        $this->archive = new stdClass();
        $this->archive->start = JRequest::getCmd('archive') . '-01';
        $this->archive->end = JRequest::getCmd('archive') . '-31';
        $this->archive->title = JFactory::getDate($this->archive->start)->format('F Y');

        return $this->archive;
    }

    /**
     * Get author data
     * TODO rewrite queries to sql object
     * @return type
     */
    public function getAuthor() {

        if (isset($this->author)) {
            return $this->author;
        }

        if (!JRequest::getCmd('author')) {
            $this->author = false;
            return false;
        }

        // get the user info
        $author_alias = $this->_db->quote(JRequest::getCmd('author'));
        $author_query = "SELECT * FROM `#__jkit_users` WHERE `alias` = $author_alias";
        $this->author = $this->_db->setQuery($author_query)->loadObject();

        return $this->author;
    }

}

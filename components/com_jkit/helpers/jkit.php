<?php

/**
 * @package     JKit
 * @subpackage  com_jkit
 * @copyright   Copyright (C) 2013 - 2014 CloudHotelier. All rights reserved.
 * @license     GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 * @link        http://www.cloudhotelier.com
 * @author      Xavier Pallicer <xpallicer@gmail.com>
 */
// No direct access
defined('_JEXEC') or die;

/**
 * JKit main helper
 */
class JKitHelper {

    /**
     * Override item values with translation strings
     * @param type $item
     * @param type $translation
     */
    public static function applyTranslation($item, $translation) {

        $item->title = $translation->title;
        $item->alias = $translation->alias;
        $strings = (array) json_decode($translation->translation);

        foreach ($strings as $string => $value) {

            if ($value != '') {

                // params
                if (substr($string, 0, 7) == 'params_') {
                    $param = substr($string, 7);
                    $item->params->$param = $value;
                    continue;
                }

                // image info
                if (substr($string, 0, 11) == 'image_info_') {
                    $image_id = (int) substr($string, 11);
                    foreach ($item->images as $image) {
                        if ($image->id == $image_id) {
                            $image->info = $value;
                        }
                    }
                    continue;
                }

                // image params link
                if (substr($string, 0, 18) == 'image_params_link_') {
                    $image_id = (int) substr($string, 18);
                    foreach ($item->images as $image) {
                        if ($image->id == $image_id) {
                            $image->params->link = $value;
                        }
                    }
                    continue;
                }

                // image params link text
                if (substr($string, 0, 23) == 'image_params_link_text_') {
                    $image_id = (int) substr($string, 23);
                    foreach ($item->images as $image) {
                        if ($image->id == $image_id) {
                            $image->params->link_text = $value;
                        }
                    }
                    continue;
                }

                // image
                if (substr($string, 0, 6) == 'image_') {
                    $image_id = (int) substr($string, 6);
                    foreach ($item->images as $image) {
                        if ($image->id == $image_id) {
                            $image->title = $value;
                        }
                    }
                    continue;
                }

                // regular item field
                $item->$string = $value;
            }
        }

        return $item;
    }

    /**
     * Load JKit assets to the document
     */
    public static function loadAssets($params = null) {

        // load component params
        if (!$params) {
            $params = JComponentHelper::getParams('com_jkit');
        }

        // require bootstrap framework
        JHtml::_('bootstrap.framework');

        // force reload bootstrap
        if ($params->get('assets_bootstrap', 0)) {
            JHtml::script('media/com_jkit/assets/bootstrap/js/bootstrap.js');
            JHtml::stylesheet('media/com_jkit/assets/bootstrap/css/bootstrap.css');
            JHtml::stylesheet('media/com_jkit/assets/bootstrap/css/bootstrap-responsive.css');
        }

        // load jkit media assets
        JHtml::script('com_jkit/jkit.js', false, true, false);
        JHtml::stylesheet('com_jkit/jkit.css', false, true, false);

        // font awesome
        if ($params->get('assets_fawesome', 0)) {
            JHtml::stylesheet('components/com_jkit/assets/font-awesome/css/font-awesome.min.css');
        }

        // load cycle2
        if ($params->get('assets_cycle2', 1)) {
            JHtml::script('media/com_jkit/assets/cycle2/jquery.cycle2.js');
            JHtml::script('media/com_jkit/assets/cycle2/jquery.cycle2.center.js');
            JHtml::script('media/com_jkit/assets/cycle2/jquery.cycle2.swipe.js');
        }

        // load fancybox
        if ($params->get('assets_fancybox', 1)) {
            JHtml::script('media/com_jkit/assets/fancybox/jquery.fancybox.js');
            JHtml::script('media/com_jkit/assets/fancybox/helpers/jquery.fancybox-media.js');
            JHtml::stylesheet('media/com_jkit/assets/fancybox/jquery.fancybox.css');
        }

        // load imageLightbox
        if ($params->get('assets_imagelightbox', 1)) {
            JHtml::script('media/com_jkit/assets/imagelightbox/imagelightbox.js');
            JHtml::stylesheet('media/com_jkit/assets/imagelightbox/imagelightbox.css');
        }

        // load isotope
        if ($params->get('assets_isotope', 1)) {
            JHtml::script('media/com_jkit/assets/isotope/jquery.isotope.js');
            JHtml::stylesheet('media/com_jkit/assets/isotope/isotope.css');
        }

        // load twitter widget
        if ($params->get('assets_twitter', 0)) {
            JFactory::getDocument()->addScript('//platform.twitter.com/widgets.js');
        }

        // load plusone widget
        if ($params->get('assets_plusone', 0)) {
            JFactory::getDocument()->addScript('//apis.google.com/js/plusone.js');
        }
    }

}

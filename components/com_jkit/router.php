<?php

/**
 * @package     JKit
 * @subpackage  com_jkit
 * @copyright   Copyright (C) 2013 - 2014 CloudHotelier. All rights reserved.
 * @license     GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 * @link        http://www.cloudhotelier.com
 * @author      Xavier Pallicer <xpallicer@gmail.com>
 */
// No direct access
defined('_JEXEC') or die;

/**
 * Route Builder
 */
function JKitBuildRoute(&$query) {

    $segments = array();

    // remove the view form the url
    // Jkit views are always based on the menu item
    if (isset($query['view'])) {
        $view = $query['view'];
        unset($query['view']);
    }

    // remove the id form the url
    // items view does not need it
    // item view will take the id from the alias
    if (isset($query['id'])) {
        if ($view == 'item') {
            if (is_numeric($query['id'])) {
                $segments[] = $query['id'];
            } else {
                $url = explode(':', $query['id']);
                $segments[] = $url[1];
            }
        } else {
            $segments[] = $query['id'];
        }
        unset($query['id']);
    }

    // tag
    if (isset($query['tag'])) {
        $segments[] = 'tag';
        $segments[] = $query['tag'];
        unset($query['tag']);
    }

    // archive
    if (isset($query['archive'])) {
        $segments[] = 'archive';
        $segments[] = $query['archive'];
        unset($query['archive']);
    }

    // author
    if (isset($query['author'])) {
        $segments[] = 'author';
        $segments[] = $query['author'];
        unset($query['author']);
    }

    return $segments;
}

/**
 * Route Parser
 */
function JKitParseRoute($segments) {

    $vars = array();
    
    // get the menu item params
    $menu = JFactory::getApplication()->getMenu()->getActive();
    $view = $menu->query['view'];

    // tag
    if ($view == 'items' && $segments[0] == 'tag') {
        $vars['view'] = 'items';
        $vars['tag'] = str_replace(':', '-', $segments[1]);
        return $vars;
    }

    // archive
    if ($view == 'items' && $segments[0] == 'archive') {
        $vars['view'] = 'items';
        $vars['archive'] = str_replace(':', '-', $segments[1]);
        return $vars;
    }

    // author
    if ($view == 'items' && $segments[0] == 'author') {
        $vars['view'] = 'items';
        $vars['author'] = str_replace(':', '-', $segments[1]);
        return $vars;
    }

    // if we have segments in the url we assume we are on a item view
    if ($view == 'items' && count($segments) == 1) {

        $vars['view'] = 'item';

        // find the id that corresponds to the alias
        $db = JFactory::getDbo();
        $lang = $db->quote(JFactory::getLanguage()->getTag());
        $alias = $db->quote(str_replace(':', '-', $segments[0]));
        $query_translation = $db->getQuery(true)->select('ref_id')->from('#__jkit_translations')->where("ref_table = 'items'")->where("lang = $lang")->where("alias = $alias");
        $id = $db->setQuery($query_translation)->loadResult();
        if (!$id) {
            $query_default = $db->getQuery(true)->select('id')->from('#__jkit_items')->where("alias = $alias");
            $id = $db->setQuery($query_default)->loadResult();
        }

        $vars['id'] = $id;

        return $vars;
    } else {

        $vars['view'] = $segments[0];
    }

    if (!in_array($vars['view'], array('items'))) {
        if (isset($segments[1])) {
            $vars['id'] = $segments[1];
        } else {
            $vars['id'] = $menu->query['id'];
        }
    }

    return $vars;
}

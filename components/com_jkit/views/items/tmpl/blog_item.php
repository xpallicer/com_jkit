<?php
/**
 * @package     JKit
 * @author   	Xavier Pallicer
 * @copyright	Copyright (C) 2013 CloudHotelier. All rights reserved.
 * @license	GNU GPLv3 <http://www.gnu.org/licenses/gpl.html>
 * @link	http://www.clouditemier.com
 */
// No direct access
defined('_JEXEC') or die;

$path_items = JURI::root() . 'images/jkit/items/';
$img_size = '-screen';

$link = JRoute::_('index.php?option=com_jkit&view=item&id=' . $this->item->id . ':' . $this->item->alias);
$header = $this->item->display == 'secondary' ? 'h3' : 'h1';
$lead = $this->item->display == 'secondary' ? '' : ' class="lead"';
?>

<article class="jkit-item">

    <header>

        <<?php echo $header; ?>>
        <a href="<?php echo $link; ?>" title="<?php echo $this->escape($this->item->title); ?>">
            <?php echo $this->escape($this->item->title); ?>
        </a>
        </<?php echo $header; ?>>

        <p class="meta muted">
            <?php if ($this->item->user) echo JText::_('COM_JKIT_ANY_WRITTEN') . ' <a href="' . JRoute::_('index.php?option=com_jkit&view=items&author=' . $this->item->user_alias) . '">' . $this->item->user . '</a>'; ?>
            <?php echo JHtml::date($this->item->created); ?>
        </p>

        <?php if ($this->item->image): ?>
            <a class="thumbnail" href="<?php echo $link; ?>" title="<?php echo $this->escape($this->item->title); ?>">
                <img src="<?php echo $path_items . $this->item->id . $img_size; ?>.jpg" alt="<?php echo $this->item->title; ?>" />
            </a>
        <?php endif; ?>

    </header>

    <p<?php echo $lead; ?>><?php echo $this->item->info; ?></p>

    <?php echo $this->item->intro; ?>


    <a class="btn btn-link" href="<?php echo $link; ?>" title="<?php echo $this->escape($this->item->title); ?>">
        <?php echo JText::_('COM_JKIT_ANY_READMORE'); ?>
    </a>


</article>
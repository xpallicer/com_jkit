<?php
/**
 * @package     JKit
 * @author   	Xavier Pallicer
 * @copyright	Copyright (C) 2013 CloudHotelier. All rights reserved.
 * @license	GNU GPLv3 <http://www.gnu.org/licenses/gpl.html>
 * @link	http://www.clouditemier.com
 */
// No direct access
defined('_JEXEC') or die;

// load assets
JKitHelper::loadAssets($this->params);

// layout
$next = 0;
$leading = (int) $this->params->get('leading');
$primary = ((int) $this->params->get('primary')) + $leading;
$primary_cols = (int) $this->params->get('primary_cols');
$secondary = ((int) $this->params->get('secondary')) + $primary;
$secondary_cols = (int) $this->params->get('secondary_cols');

// subheading
$subheading = ($this->params->get('subheading')) ? ' <small>' . $this->escape($this->params->get('subheading')) . '</small>' : '';
?>

<?php if ($this->params->get('title', 1)): ?>
    <div class="page-header">
        <h1><?php echo $this->lists->title . $subheading; ?></h1>
    </div>
<?php endif; ?>

<div class="jkit-items">

    <?php if ($leading): ?>
        <div class="row-fluid">
            <?php
            for ($i = 0; $i <= $leading; $i++) {
                $this->item = $this->items[$i];
                $this->item->display = 'leading';
                echo '<div class="span12">' . $this->loadTemplate('item') . '<hr></div>';
                $next = $i + 1;
                if (($next < $leading) && isset($this->items[$next])) {
                    echo '</div><div class="row-fluid">';
                } else {
                    break;
                }
            }
            ?>
        </div>
    <?php endif; ?>


    <?php if ($primary && isset($this->items[$next])): ?>
        <div class="row-fluid">
            <?php
            $columns = $primary_cols;
            $span_width = intval(12 / $columns);
            $col_item = 0;
            for ($i = $leading; $i < $primary; $i++) {
                $col_item++;
                $this->item = $this->items[$i];
                $this->item->display = 'primary';
                echo '<div class="span' . $span_width . '">' . $this->loadTemplate('item') . '<hr></div>';
                $next = $i + 1;
                if (isset($this->items[$next]) && $col_item == $columns) {
                    echo '</div><div class="row-fluid">';
                    $col_item = 0;
                }
                if (!isset($this->items[$next])) {
                    break;
                }
            }
            ?>
        </div>
    <?php endif; ?>


    <?php if ($secondary && isset($this->items[$next])): ?>
        <div class="row-fluid">
            <?php
            $columns = $secondary_cols;
            $span_width = intval(12 / $columns);
            $col_item = 0;
            for ($i = $primary; $i < $secondary; $i++) {
                $col_item++;
                $this->item = $this->items[$i];
                $this->item->display = 'secondary';
                echo '<div class="span' . $span_width . '">' . $this->loadTemplate('item') . '<hr></div>';
                $next = $i + 1;
                if (isset($this->items[$next]) && $col_item == $columns) {
                    echo '</div><div class="row-fluid">';
                    $col_item = 0;
                }
                if (!isset($this->items[$next])) {
                    break;
                }
            }
            ?>
        </div>
    <?php endif; ?>

</div>

<?php if ($this->pagination->get('pages.total') > 1) : ?>
    <div class="jkit-pagination pagination pagination-centered">
        <ul><?php echo $this->pagination->getPagesLinks(); ?></ul>
    </div>
    <?php

 endif;

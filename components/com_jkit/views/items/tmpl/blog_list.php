<?php
/**
 * @package     JKit
 * @author   	Xavier Pallicer
 * @copyright	Copyright (C) 2013 CloudHotelier. All rights reserved.
 * @license	GNU GPLv3 <http://www.gnu.org/licenses/gpl.html>
 * @link	http://www.clouditemier.com
 */
// No direct access
defined('_JEXEC') or die;
?>

<?php if ($this->params->get('title', 1)): ?>
    <div class="page-header">
        <h1><?php echo $this->lists->title; ?></h1>
    </div>
<?php endif; ?>

<div class="jkit-tag-items">

    <div class="row-fluid">
        <?php
        $columns = 2;
        $span_width = intval(12 / $columns);
        $col_item = 0;
        foreach ($this->items as $i => $item) {
            $col_item++;
            $this->item = $this->items[$i];
            $this->item->display = 'secondary';
            echo '<div class="span' . $span_width . '">' . $this->loadTemplate('item') . '<hr></div>';
            $next = $i + 1;
            if (isset($this->items[$next]) && $col_item == $columns) {
                echo '</div><div class="row-fluid">';
                $col_item = 0;
            }
            if (!isset($this->items[$next])) {
                break;
            }
        }
        ?>
    </div>
</div>

<?php if ($this->pagination->get('pages.total') > 1) : ?>
    <div class="jkit-pagination pagination pagination-centered">
        <ul><?php echo $this->pagination->getPagesLinks(); ?></ul>
    </div>
<?php endif; ?>
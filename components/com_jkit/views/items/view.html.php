<?php

/**
 * @package     JKit
 * @author   	Xavier Pallicer
 * @copyright	Copyright (C) 2013 CloudHotelier. All rights reserved.
 * @license	GNU GPLv3 <http://www.gnu.org/licenses/gpl.html>
 * @link	http://www.clouditemier.com
 */
// No direct access
defined('_JEXEC') or die;

/**
 * Items View
 */
class JKitViewItems extends JViewLegacy {

    /**
     * Display the view
     */
    function display($tpl = null) {

        // load app
        $app = JFactory::getApplication();

        // get the items
        $this->items = $this->get('Items');
        $this->tags = $this->get('Tags');
        $this->pagination = $this->get('Pagination');

        // params
        $this->params = JComponentHelper::getParams('com_jkit');
        $app_params = $app->getParams();
        $this->params->merge($app_params);

        // lists
        $this->lists = new stdClass();

        // layout
        $layout = $this->params->get('jkitlayout');
        if ($layout) {
            $this->setLayout($layout);
        }

        // list views
        $this->tag = $this->get('Tag');
        $this->archive = $this->get('Archive');
        $this->author = $this->get('Author');

        // prepare document
        $this->prepareDocument();

        // jkit views
        if ($this->tag || $this->archive || $this->author || JRequest::getUInt('start', 0)) {
            $title = '';
            if ($this->tag) {
                $title = JText::sprintf('COM_JKIT_ITEMS_TITLE_TAG', $this->tag->title);
            } elseif ($this->archive) {
                $title = JText::sprintf('COM_JKIT_ITEMS_TITLE_ARCHIVE', $this->archive->title);
            } elseif ($this->author) {
                $title = JText::sprintf('COM_JKIT_ITEMS_TITLE_AUTHOR', $this->author->title);
            }
            $tpl = 'list';
            $this->document->setTitle($this->lists->title . ' | ' . $title);
            $this->lists->title .= '<br>' . $title;
        }

        // display the view
        parent::display($tpl);
    }

    protected function prepareDocument() {

        // load app and menu
        $app = JFactory::getApplication();
        $menu = $app->getMenu()->getActive();

        // default title
        $title = $this->params->get('page_title', $menu->title);

        // set lists title
        $this->lists->title = $title;

        // prepare document title
        if ($app->getCfg('sitename_pagetitles', 0) == 1) {
            $title = JText::sprintf('JPAGETITLE', $app->getCfg('sitename'), $title);
        } elseif ($app->getCfg('sitename_pagetitles', 0) == 2) {
            $title = JText::sprintf('JPAGETITLE', $title, $app->getCfg('sitename'));
        }

        // set document title
        $this->document->setTitle($title);

        // metas
        if ($this->params->get('menu-meta_description')) {
            $this->document->setDescription($this->params->get('menu-meta_description'));
        }
        if ($this->params->get('menu-meta_keywords')) {
            $this->document->setMetadata('keywords', $this->params->get('menu-meta_keywords'));
        }

        // pageclass_sfx 
        $this->pageclass_sfx = htmlspecialchars($this->params->get('pageclass_sfx'));

        // pathway
        if ($this->tag) {
            $app->getPathway()->addItem(JText::sprintf('COM_JKIT_ANY_TAG', $this->tag->title));
        }
    }

}

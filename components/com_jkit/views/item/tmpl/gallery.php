<?php
/**
 * @package     JKit
 * @subpackage  com_jkit
 * @copyright   Copyright (C) 2013 - 2014 CloudHotelier. All rights reserved.
 * @license     GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 * @link        http://www.cloudhotelier.com
 * @author      Xavier Pallicer <xpallicer@gmail.com>
 */
// No direct access
defined('_JEXEC') or die;

// load assets
JKitHelper::loadAssets($this->params);

// images
$path_images = JURI::root() . 'images/jkit/images/';

// subheading
$subheading = ($this->params->get('subheading')) ? ' <small>' . $this->escape($this->params->get('subheading')) . '</small>' : '';
?>

<!-- view -->
<div id="com-jkit-gallery">
    
    <!-- header -->
    <?php if ($this->params->get('title', 1)): ?>
        <div class="page-header">
            <h1><?php echo $this->lists->title . $subheading; ?></h1>
        </div>
    <?php endif; ?>
    <!-- /header -->

    <!-- tags -->
    <?php if ($this->item->tags): ?>
        <div class="jkit-gallery-filter">
            <div class="btn-group option-set" data-filter-group="tag">
                <a class="btn active" href="javascript:;" data-filter-value=""><?php echo JText::_('COM_JKIT_ANY_ALL'); ?></a>
                <?php foreach ($this->item->tags as $tag): ?>
                    <a class="btn" href="javascript:;" data-filter-value=".image-tag-<?php echo $tag->id; ?>"><?php echo $tag->title; ?></a>
                <?php endforeach; ?>
            </div>
        </div>
    <?php endif; ?>
    <!-- /tags -->

    <!-- images -->
    <div id="jkit-gallery">

        <?php if ($this->item->images): ?>

            <div class="jkit-gallery-images row">

                <?php foreach ($this->item->images as $image): ?>

                    <?php
                    // tags
                    $tags = '';
                    if (isset($image->params->tags)) {
                        if ($image->params->tags) {
                            $tags = 'image-tag-' . implode(' image-tag-', $image->params->tags);
                        }
                    }
                    ?>

                    <div class="jkit-gallery-image <?php echo $tags; ?>">
                        <div class="jkit-gallery-img jkit-overlay">
                            <div class="jkit-overlay-bg" style="background-image: url(<?php echo $path_images . $image->id; ?>-med.jpg);"></div>
                            <div class="jkit-overlay-top">
                                <a class="jkit-gallery-imagelightbox" title="<?php echo $image->title; ?>" href="<?php echo $path_images . $image->id; ?>-big.jpg"><i class="icon-zoom-in"></i></a>
                            </div>
                        </div>
                        <div class="jkit-gallery-text">
                            <span class="jkit-gallery-text-title"><?php echo $image->title; ?></span>
                            <?php if ($image->info): ?>
                                <span class="jkit-gallery-text-info"><?php echo $image->info; ?></span>
                            <?php endif; ?>
                        </div>
                    </div>

                <?php endforeach; ?>

            </div>
        <?php endif; ?>

    </div>
    <!-- /images -->

</div>
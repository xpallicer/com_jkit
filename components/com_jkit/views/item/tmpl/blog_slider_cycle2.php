<?php
/**
 * @package     JKit
 * @subpackage  com_jkit
 * @copyright   Copyright (C) 2013 - 2014 CloudHotelier. All rights reserved.
 * @license     GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 * @link        http://www.cloudhotelier.com
 * @author      Xavier Pallicer <xpallicer@gmail.com>
 */
// No direct access
defined('_JEXEC') or die;

$path_images = JURI::root() . 'images/jkit/images/';
?>

<div class="thumbnail">
    <div 
        class="cycle-slideshow jkit-cycle2-slider" 
        data-cycle-fx=scrollHorz
        data-cycle-timeout=5000
        data-cycle-center-horz=true
        data-cycle-swipe=true
        data-cycle-pager-template="<span>&nbsp;</span>"
        >
        <div class="cycle-prev"></div>
        <div class="cycle-next"></div>
        <div class="cycle-pager"></div>
        <?php
        foreach ($this->item->images as $i => $image) {
            echo '<img src="' . $path_images . $image->id . '-screen.jpg' . '" alt="">';
        }
        ?>
    </div>
</div>
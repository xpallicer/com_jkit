<?php
/**
 * @package     JKit
 * @subpackage  com_jkit
 * @copyright   Copyright (C) 2013 - 2014 CloudHotelier. All rights reserved.
 * @license     GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 * @link        http://www.cloudjkitier.com
 * @author      Xavier Pallicer <xpallicer@gmail.com>
 */
// No direct access
defined('_JEXEC') or die;

// load assets
JKitHelper::loadAssets($this->params);

// images
$path_items = JURI::root() . 'images/jkit/items/';
?>

<article class="jkit-item">

    <header>

        <div class="page-header">
            <h1><?php echo $this->item->title; ?></h1>
        </div>

        <?php if ($this->item->images): ?>
            <?php echo $this->loadTemplate('slider_' . $this->params->get('slider', 'cycle2')); ?>
        <?php endif; ?>

        <?php if ($this->item->image && !$this->item->images): ?>
            <p class="thumbnail">
                <img src="<?php echo $path_items . $this->item->id; ?>-screen.jpg" alt="<?php echo $this->item->title; ?>" />
            </p>
        <?php endif; ?>

    </header>

    <?php if ($this->item->info): ?>
        <p class="lead"><?php echo $this->item->info; ?></p>
    <?php endif; ?>

    <?php echo $this->item->intro; ?>

    <?php echo $this->item->body; ?>

    <p class="meta muted">
        <?php if ($this->item->user): ?>
            <?php echo JText::_('COM_JKIT_ANY_WRITTEN'); ?> 
            <a href="<?php echo JRoute::_('index.php?option=com_jkit&view=items&author=' . $this->item->user_alias); ?>">
                <?php echo $this->item->user; ?>
            </a>
        <?php endif; ?>
        <?php echo JHtml::date($this->item->created); ?>
    </p>
    <hr>

    <?php if ($this->item->tags): ?>
        <div class="jkit-item-tags">
            <p>
                <?php echo JText::_('COM_JKIT_ANY_TAGGED'); ?>:
                <?php foreach ($this->item->tags as $tag): ?>
                    <a class="item-tag" href="<?php echo JRoute::_('index.php?option=com_jkit&view=items&tag=' . $tag->alias); ?>"><?php echo $tag->title; ?></a>
                <?php endforeach; ?>
            </p>
        </div>
    <?php endif; ?>

    <?php if ($this->params->get('twitter', 1) || $this->params->get('plusone', 1) || $this->params->get('fblike', 1)): ?>
        <hr>
        <div class="jkit-item-socialbuttons">
            <?php if ($this->params->get('twitter', 1)): ?><div><a href="//twitter.com/share" class="twitter-share-button" data-url="<?php echo JUri::current(); ?>" data-count="none" data-lang="en_GB"></a></div><?php endif; ?>
            <?php if ($this->params->get('plusone', 1)): ?><div><div class="g-plusone" data-href="<?php echo JUri::current(); ?>" data-size="medium" data-annotation="none"></div></div><?php endif; ?>
            <?php if ($this->params->get('fblike', 1)): ?><div><div class="fb-like" data-href="<?php echo JUri::current(); ?>" data-send="false" data-layout="button_count" data-width="100" data-show-faces="false" data-action="like" data-colorscheme="light"></div></div><?php endif; ?>
        </div>
    <?php endif; ?>

    <?php if ($this->params->get('disqus') && $this->params->get('disqus_name')): ?>
        <hr>   
        <div id="disqus_thread"></div>
        <script>
            var disqus_shortname = '<?php echo $this->params->get('disqus_name'); ?>';
            var disqus_config = function() {
                this.language = '<?php echo str_replace('-', '_', JFactory::getLanguage()->getTag()); ?>';
            };
            (function() {
                var dsq = document.createElement('script');
                dsq.type = 'text/javascript';
                dsq.async = true;
                dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
                (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
            })();
        </script>
    <?php endif; ?>        

</article>
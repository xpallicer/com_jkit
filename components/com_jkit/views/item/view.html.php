<?php

/**
 * @package     JKit
 * @subpackage  com_jkit
 * @copyright   Copyright (C) 2013 - 2014 CloudHotelier. All rights reserved.
 * @license     GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 * @link        http://www.cloudhotelier.com
 * @author      Xavier Pallicer <xpallicer@gmail.com>
 */
// No direct access
defined('_JEXEC') or die;

/**
 * Item View
 */
class JKitViewItem extends JViewLegacy {

    /**
     * Display the view
     */
    function display($tpl = null) {

        // load app
        $app = JFactory::getApplication();

        // get the item
        $this->item = $this->get('item');
        
        // lists
        $this->lists = new stdClass();

        // params
        $this->params = JComponentHelper::getParams('com_jkit');
        $app_params = $app->getParams();
        $this->params->merge($app_params);

        // layout
        $layout = $this->params->get('jkitlayout');
        if ($layout) {
            $this->setLayout($layout);
        }

        // document metadata
        $this->prepareDocument();

        // display template
        parent::display($tpl);
    }

    /**
     * Document metadata
     */
    protected function prepareDocument() {

        // load app and menu
        $app = JFactory::getApplication();
        $menu = $app->getMenu()->getActive();

        // default title
        $title = $this->params->get('page_title', '');
        
        // item title
        if(!$title){
            $title = $this->item->title;
        }

        // blog style item title
        if ($menu) {
            if ($menu->query['view'] == 'items') {
                $title = $this->item->title . ' - ' . $title;
            }
        }
        
        $this->lists->title = $title;
        
        if (!$title) {
            $title = $app->getCfg('sitename');
        } elseif ($app->getCfg('sitename_pagetitles', 0) == 1) {
            $title = JText::sprintf('JPAGETITLE', $app->getCfg('sitename'), $title);
        } elseif ($app->getCfg('sitename_pagetitles', 0) == 2) {
            $title = JText::sprintf('JPAGETITLE', $title, $app->getCfg('sitename'));
        }
        
        // set the title
        $this->document->setTitle($title);

        if ($this->params->get('menu-meta_description')) {
            $this->document->setDescription($this->params->get('menu-meta_description'));
        }

        if ($this->params->get('menu-meta_keywords')) {
            $this->document->setMetadata('keywords', $this->params->get('menu-meta_keywords'));
        }

        // keywords
        if ($this->item->tags) {
            $tags = array();
            foreach ($this->item->tags as $tag) {
                $tags[] = $tag->title;
            }
            $kw = $this->params->get('menu-meta_keywords') ? $this->params->get('menu-meta_keywords') . ', ' . implode(', ', $tags) : implode(', ', $tags);
            $this->document->setMetadata('keywords', $kw);
        }

        // robots
        if ($this->params->get('robots')) {
            $this->document->setMetadata('robots', $this->params->get('robots'));
        }

        // pathway
        if ($menu->query['view'] == 'items') {
            $app->getPathway()->addItem($this->item->title);
        }
    }

}

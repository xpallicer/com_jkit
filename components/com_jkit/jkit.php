<?php

/**
 * @package     JKit
 * @subpackage  com_jkit
 * @author      Xavier Pallicer <xpallicer@gmail.com>
 * @copyright   Copyright (C) 2013 - 2014 CloudHotelier. All rights reserved.
 * @license     GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 */
// No direct access
defined('_JEXEC') or die;

// Load language files
JFactory::getLanguage()->load('com_jkit', null, 'en-GB', true);
JFactory::getLanguage()->load('com_jkit');

// Load helpers
require_once JPATH_COMPONENT . '/helpers/jkit.php';
require_once JPATH_COMPONENT_ADMINISTRATOR . '/helpers/image.php';

// Load and execute the controller
$controller = JControllerLegacy::getInstance('JKit');
$controller->execute(JRequest::getCmd('task'));
$controller->redirect();

<?php

/**
 * @package     JKit
 * @subpackage  com_jkit
 * @author      Xavier Pallicer <xpallicer@gmail.com>
 * @copyright   Copyright (C) 2013 - 2014 CloudHotelier. All rights reserved.
 * @license     GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 */
// No direct access
defined('_JEXEC') or die;

/**
 * JKit Controller
 */
class JKitController extends JControllerLegacy {

    /**
     * Display the view
     */
    public function display($cachable = false, $urlparams = false) {

        // set the default view
        JRequest::setVar('view', JRequest::getCmd('view', 'items'));

        if (!in_array(JRequest::getString('view'), array('item', 'items'))) {
            JError::raiseError(404, JText::_('COM_JKIT_ERROR_404'));
        }

        // display the view
        parent::display($cachable, $urlparams);
    }

}

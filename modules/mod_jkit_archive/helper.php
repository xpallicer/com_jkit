<?php

/**
 * @package     JKit
 * @author   	Xavier Pallicer
 * @copyright	Copyright (C) 2013 CloudHotelier. All rights reserved.
 * @license	GNU GPLv3 <http://www.gnu.org/licenses/gpl.html>
 * @link	http://www.cloudhotelier.com
 */
// no direct access
defined('_JEXEC') or die;

/**
 * JKit Mod Archive Helper
 */
abstract class modJKitArchiveHelper {

    /**
     * Get the Items
     */
    public static function getItems($params) {

        // check cache
        $app = JFactory::getApplication();
        $user = JFactory::getUser();
        $cache = JFactory::getCache('mod_jkit_tags', '');

        // cache key
        $menu = $app->getMenu();
        $active = ($menu->getActive()) ? $menu->getActive() : $menu->getDefault();
        $levels = $user->getAuthorisedViewLevels();
        asort($levels);
        $key = 'menu_items' . $params . implode(',', $levels) . '.' . $active->id;

        // try to get items from cache
        $items = $cache->get($key);

        // no cache found, query for items
        if (!$items) {

            if (!count($params->get('categories'))) {
                $items = array();
                $cache->store($items, $key);
                return $items;
            }

            // items query
            $db = JFactory::getDbo();
            $query = $db->getQuery(true)->select('created')->from('#__jkit_items')->where('state = 1')->order('created DESC');

            // category
            $categories = implode(',', $params->get('categories'));
            $query->where("catid IN ($categories)");
            
            // get items dates
            $items_dates = $db->setQuery($query)->loadColumn();

            // no items
            if (!count($items_dates)) {
                $items = array();
                $cache->store($items, $key);
                return $items;
            }

            // get items months
            $items_array = array();
            foreach ($items_dates as $date) {
                $month = substr($date, 0, 7);
                if (!isset($items_array[$month])) {
                    $item = new stdClass();
                    $item->month = $month;
                    $item->title = JFactory::getDate($month . '-01')->format('F Y');
                    $item->count = 1;
                    $items_array[$month] = $item;
                } else {
                    $items_array[$month]->count++;
                }
            }
            $items = array_values($items_array);

            // store items to cache
            $cache->store($items, $key);
        }

        return $items;
    }

}

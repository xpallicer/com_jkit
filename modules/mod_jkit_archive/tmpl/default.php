<?php
/**
 * @package     JKit
 * @subpackage  com_jkit
 * @copyright   Copyright (C) 2013 - 2014 CloudHotelier. All rights reserved.
 * @license     GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 * @link        http://www.cloudhotelier.com
 * @author      Xavier Pallicer <xpallicer@gmail.com>
 */
// no direct access
defined('_JEXEC') or die;

$itemId = $params->get('Itemid');
?>

<div class="mod-jkit-archive">

    <?php if (count($items)): ?>
        <ul class="nav nav-list">
            <?php foreach ($items as $item): ?>
                <?php $link = JRoute::_('index.php?option=com_jkit&view=items&archive=' . $item->month . '&Itemid=' . $itemId); ?>
                <li><a href="<?php echo $link; ?>"><?php echo $item->title; ?> (<?php echo $item->count; ?>)</a></li>
            <?php endforeach; ?>
        </ul>
    <?php endif; ?>

</div>

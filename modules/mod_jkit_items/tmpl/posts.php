<?php
/**
 * @package     JKit
 * @subpackage  com_jkit
 * @copyright   Copyright (C) 2013 - 2014 CloudHotelier. All rights reserved.
 * @license     GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 * @link        http://www.cloudhotelier.com
 * @author      Xavier Pallicer <xpallicer@gmail.com>
 */
// no direct access
defined('_JEXEC') or die;

$itemid = $params->get('Itemid');
$img_path = JURI::root() . 'images/jkit/items/';
?>

<div class="mod-jkit-items-posts">

    <?php foreach ($items as $item): ?>

        <?php
        $link = JRoute::_('index.php?option=com_jkit&view=item&id=' . $item->id . ':' . $item->alias . "&Itemid=$itemid");
        $img = $img_path . $item->id . '-tiny.jpg';
        $date = JFactory::getDate($item->created)->format(JText::_('DATE_FORMAT_LC3') . ' H:i');
        ?>

        <div class="media mod-jkit-items-post">
            <?php if ($item->image): ?>
                <a class="pull-left thumbnail" href="<?php echo $link; ?>">
                    <img class="media-object" alt="<?php echo $item->title; ?>" src="<?php echo $img; ?>" />
                </a>
            <?php endif; ?>
            <div class="media-body">
                <h4 class="media-heading"><a href="<?php echo $link; ?>"><?php echo $item->title; ?></a></h4>
                <small class="muted"><?php echo $date; ?></small>
                <p><?php echo $item->info; ?></p>
                <a class="btn btn-mini" href="<?php echo $link; ?>"><?php echo JText::_('TPL_JSTRAP_CONTINUE_READING'); ?> <i class="icon-angle-right"></i></a>
            </div>
        </div>

    <?php endforeach; ?>

</div>
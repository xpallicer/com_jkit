<?php

/**
 * @package     JKit
 * @subpackage  com_jkit
 * @copyright   Copyright (C) 2013 - 2014 CloudHotelier. All rights reserved.
 * @license     GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 * @link        http://www.cloudhotelier.com
 * @author      Xavier Pallicer <xpallicer@gmail.com>
 */
// no direct access
defined('_JEXEC') or die;

/**
 * JKit Mod Items Helper
 */
abstract class modJKitItemsHelper {

    /**
     * Get the JKit Items
     */
    public static function getItems($params) {

        // check cache
        $app = JFactory::getApplication();
        $user = JFactory::getUser();
        $cache = JFactory::getCache('mod_jkit_items', '');

        // cache key
        $menu = $app->getMenu();
        $active = ($menu->getActive()) ? $menu->getActive() : $menu->getDefault();
        $levels = $user->getAuthorisedViewLevels();
        asort($levels);
        $key = 'menu_items' . $params . implode(',', $levels) . '.' . $active->id;

        // try to get items from cache
        $items = $cache->get($key);

        // no cache found, query for items
        if (!$items) {

            // empty categories
            if (!count($params->get('categories'))) {
                $items = array();
                $cache->store($items, $key);
                return $items;
            }

            // get items using component model
            $model = new JKitModelItems();
            $items = $model->getItems($params->get('categories'), $params->get('order', 'new'), $params->get('limit', '5'));

            // no items found
            if (!count($items)) {
                $items = array();
                $cache->store($items, $key);
                return $items;
            }

            $cache->store($items, $key);
        }

        return $items;
    }

    /**
     * Truncate text length
     * @param string $string
     * @param type $limit
     * @param type $break
     * @param type $pad
     * @return string
     */
    static function truncateText($string, $limit = 80, $break = ' ', $pad = '...') {
        if (strlen($string) <= $limit) {
            return $string;
        }
        if (false !== ($breakpoint = strpos($string, $break, $limit))) {
            if ($breakpoint < strlen($string) - 1) {
                $string = substr($string, 0, $breakpoint) . $pad;
            }
        }
        return $string;
    }

}

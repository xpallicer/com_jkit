<?php

/**
 * @package     JKit
 * @subpackage  com_jkit
 * @copyright   Copyright (C) 2013 - 2014 CloudHotelier. All rights reserved.
 * @license     GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 * @link        http://www.cloudhotelier.com
 * @author      Xavier Pallicer <xpallicer@gmail.com>
 */
// no direct access
defined('_JEXEC') or die;

// load language files
$lang = JFactory::getLanguage();
$lang->load('mod_jkit_tags', JPATH_SITE, 'en-GB', true);
$lang->load('mod_jkit_tags', JPATH_SITE, null, true);

// load component language files
$cpath = JPATH_BASE . '/components/com_jkit';
$lang->load('com_jkit', $cpath, 'en-GB', true);
$lang->load('com_jkit', $cpath, null, true);

// helpers
require_once $cpath . '/helpers/jkit.php';
require_once 'helper.php';

// get the items
$items = modJKitTagsHelper::getItems($params);

if (!$items) {
    return false;
}

// display
require JModuleHelper::getLayoutPath('mod_jkit_tags', $params->get('layout', 'default'));

<?php
/**
 * @package     JKit
 * @subpackage  com_jkit
 * @copyright   Copyright (C) 2013 - 2014 CloudHotelier. All rights reserved.
 * @license     GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 * @link        http://www.cloudhotelier.com
 * @author      Xavier Pallicer <xpallicer@gmail.com>
 */
// no direct access
defined('_JEXEC') or die;
?>

<div class="mod-jkit-tags">

    <?php if (count($items)): ?>
        <?php foreach ($items as $tag): ?>
            <?php $link = JRoute::_('index.php?option=com_jkit&view=items&tag=' . $tag->alias . '&Itemid=' . $params->get('Itemid')); ?>
            <a class="item-tag tag-count-<?php echo $tag->count; ?>" href="<?php echo $link; ?>"><?php echo $tag->title; ?></a>
        <?php endforeach; ?>
    <?php endif; ?>

</div>

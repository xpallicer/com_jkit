<?php

/**
 * @package     JKit
 * @subpackage  com_jkit
 * @copyright   Copyright (C) 2013 - 2014 CloudHotelier. All rights reserved.
 * @license     GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 * @link        http://www.cloudhotelier.com
 * @author      Xavier Pallicer <xpallicer@gmail.com>
 */
// no direct access
defined('_JEXEC') or die;

/**
 * JKit Mod Archive Helper
 */
abstract class modJKitTagsHelper {

    /**
     * Get the Items
     */
    public static function getItems($params) {

        // check cache
        $app = JFactory::getApplication();
        $user = JFactory::getUser();
        $cache = JFactory::getCache('mod_jkit_tags', '');

        // cache key
        $menu = $app->getMenu();
        $active = ($menu->getActive()) ? $menu->getActive() : $menu->getDefault();
        $levels = $user->getAuthorisedViewLevels();
        asort($levels);
        $key = 'menu_items' . $params . implode(',', $levels) . '.' . $active->id;

        // try to get items from cache
        $items = $cache->get($key);

        // no cache found, query for items
        if (!$items) {

            if (!count($params->get('categories'))) {
                $items = array();
                $cache->store($items, $key);
                return $items;
            }

            // items query
            $db = JFactory::getDbo();
            $query = $db->getQuery(true)->select('id')->from('#__jkit_items')->where('state = 1');

            // category
            $categories = implode(',', $params->get('categories'));
            $query->where("catid IN ($categories)");

            // get items ids
            $ids = $db->setQuery($query)->loadColumn();

            // no items
            if (!count($ids)) {
                $items = array();
                $cache->store($items, $key);
                return $items;
            }

            // get items tags
            $qids = implode(',', $ids);
            $query_items_tags = $db->getQuery(true)->select('a.tag_id')->from('#__jkit_tags_item AS a')->where("a.item_id IN($qids)");
            $items_tags = $db->setQuery($query_items_tags)->loadColumn();

            // no tags
            if (!count($items_tags)) {
                $items = array();
                $cache->store($items, $key);
                return $items;
            }

            // prepare ids
            // count tags
            $t_ids = array();
            $t_count = array();
            foreach ($items_tags as $i => $tag_id) {
                if (!isset($t_count[$tag_id])) {
                    $t_count[$tag_id] = 1;
                } else {
                    $t_count[$tag_id] ++;
                }
                $t_ids[] = $tag_id;
            }
            $tags_ids = array_unique($t_ids);

            // get tags
            $tids = implode(',', $tags_ids);
            $query_tags = $db->getQuery(true)->select('a.id, a.title, a.alias')->from('#__jkit_tags AS a')->where("a.id IN($tids)")->order('a.title');
            $items = $db->setQuery($query_tags)->loadObjectList('id');

            if (count($items)) {

                // get translations
                $lang = $db->quote(JFactory::getLanguage()->getTag());
                $query_translations = $db->getQuery(true)->select('*')->from('#__jkit_translations')->where("ref_table = 'tags'")->where("lang = $lang");
                $query_translations->where("ref_id IN ($tids)");
                $translations = $db->setQuery($query_translations)->loadObjectList();

                // apply translations and count
                foreach ($items as $tag) {
                    if (count($translations)) {
                        foreach ($translations as $translation) {
                            if ($translation->ref_id == $tag->id) {
                                $tag->title = $translation->title;
                                $tag->alias = $translation->alias;
                            }
                        }
                    }
                    $tag->count = $t_count[$tag->id];
                }
            }

            // reorder tags
            usort($items, array('modJKitTagsHelper', 'reorderTags'));

            // store items to cache
            $cache->store($items, $key);
        }

        return $items;
    }

    /**
     * Reorder transaltion tags
     */
    static function reorderTags($a, $b) {
        return strcmp($b->count, $a->count);
    }

}

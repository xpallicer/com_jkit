<?php

/**
 * @package     JKit
 * @subpackage  com_jkit
 * @copyright   Copyright (C) 2013 - 2014 CloudHotelier. All rights reserved.
 * @license     GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 * @link        http://www.cloudhotelier.com
 * @author      Xavier Pallicer <xpallicer@gmail.com>
 */
// no direct access
defined('_JEXEC') or die;

/**
 * JKit Mod Item Helper
 */
abstract class modJKitItemHelper {

    /**
     * Get the JKit Item
     */
    public static function getItem($params) {

        // check cache
        $app = JFactory::getApplication();
        $user = JFactory::getUser();
        $cache = JFactory::getCache('mod_jkit_item', '');

        // cache key
        $menu = $app->getMenu();
        $active = ($menu->getActive()) ? $menu->getActive() : $menu->getDefault();
        $levels = $user->getAuthorisedViewLevels();
        asort($levels);
        $key = 'menu_item' . $params . implode(',', $levels) . '.' . $active->id;

        // try to get item from cache
        $item = $cache->get($key);

        // no cache found, query for item
        if (!$item) {

            // require model
            $model = new JKitModelItem();
            $item = $model->getItem((int) $params->get('id'), $params->get('prepare_content', 0));

            // store item to cache
            $cache->store($item, $key);
        }

        return $item;
    }

}

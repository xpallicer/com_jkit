<?php
/**
 * @package     JKit
 * @subpackage  com_jkit
 * @copyright   Copyright (C) 2013 - 2014 CloudHotelier. All rights reserved.
 * @license     GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 * @link        http://www.cloudhotelier.com
 * @author      Xavier Pallicer <xpallicer@gmail.com>
 */
// no direct access
defined('_JEXEC') or die;

// load assets
JKitHelper::loadAssets();

$path_images = JURI::root() . 'images/jkit/images/';
$slider_id = 'jkit-item-slider-' . $item->id;
?>

<?php if ($item->images): ?>
    <div class="jkit-item-slider">
        <div id="<?php echo $slider_id; ?>" class="carousel slide">
            <ol class="carousel-indicators">
                <?php foreach ($item->images as $i => $image): ?>
                    <li data-target="#<?php echo $slider_id; ?>" data-slide-to="<?php echo $i; ?>" <?php echo $i > 0 ? '' : 'class="active"'; ?>></li>
                <?php endforeach; ?>
            </ol>
            <div class="carousel-inner">
                <?php foreach ($item->images as $i => $image): ?>
                    <div class="item<?php echo $i > 0 ? '' : ' active'; ?>">
                        <img src="<?php echo $path_images . $image->id . '-screen.jpg'; ?>">
                        <div class="carousel-caption">
                            <h4><?php echo $image->title; ?></h4>
                            <p><?php echo $image->info; ?></p>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
            <!-- Carousel nav -->
            <a class="carousel-control left" href="#<?php echo $slider_id; ?>" data-slide="prev">&lsaquo;</a>
            <a class="carousel-control right" href="#<?php echo $slider_id; ?>" data-slide="next">&rsaquo;</a>
        </div>
    </div>
<?php endif; ?>
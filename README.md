# JKit
** Multi-language Content Construction Kit (CCK) for Joomla!**

Copyright © 2011 - 2014 [CloudHotelier](http://www.cloudhotelier.com). All rights reserved.

- License: GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
- Author: Xavier Pallicer <xpallicer@gmail.com>

### CHANGELOG

The Changelog is provided in reverse chronological order.
Legend: # Bug fix     + Addition     - Feature removal     ~ Change    ! Critical bug fix     ^ Minor edit

#### Version 2.2.2

	~ Using Input instead on JRequest on dispatcher to prevent K2 plugin errors
	+ Save keyboard shortcut on edit item
	^ code style

#### Version 2.2.1

	~ Allow translation of gallery images links
	# Installer warnings in J3.4 caused by missing client="site" in modules manifest

#### Version 2.2.0

	~ Updated image gallery to add arrows navigation and also image captions
	~ filter="safehtml" for some fields, so now you can use html
	~ assets moved to media folder
	+ Missing Joomla 3.4 admnistrator menu
	

#### Version 2.1.2

	# image info translation allow raw
	^ string fixes

#### Version 2.1.1
	
	# creation date overrided no more when item has user
	~ gallery: update imagelightbox on filtering
	^ minify assets
	^ Better SEO titles
	^ Better 404 error pages

#### Version 2.1.0
	
	~ refinished types (administrator)

#### Version 2.0.1
	
	~ changed layout param name to avoid problems with com_menus
	~ always display the title in blog view (item)
	# unable to change published state on types view

#### Version 2.0.0 - Initial release for Joomla! 3.x series
	
	See v.1.x series changelog for further information about previous versions
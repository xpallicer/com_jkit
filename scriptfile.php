<?php
/**
 * @package     JKit
 * @subpackage  com_jkit
 * @copyright   Copyright (C) 2013 - 2014 CloudHotelier. All rights reserved.
 * @license     GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 * @link        http://www.cloudhotelier.com
 * @author      Xavier Pallicer <xpallicer@gmail.com>
 */
// no direct access
defined('_JEXEC') or die;

JLoader::import('joomla.filesystem.folder');
JLoader::import('joomla.filesystem.file');

class Com_JKitInstallerScript {

    /** @var array The list of extra modules and plugins to install */
    private $installation_queue = array(
        'modules' => array(
            'admin' => array(),
            'site' => array(
                'jkit_archive' => array('', 0),
                'jkit_item' => array('', 0),
                'jkit_items' => array('', 0),
                'jkit_tags' => array('', 0)
            )
        ),
        'plugins' => array()
    );

    /**
     * Postflight
     */
    function postflight($type, $parent) {

        // Install subextensions
        $status = $this->_installSubextensions($parent);

        // render html
        ?>
        <h1><?php echo JText::_('COM_JKIT'); ?></h1>
        <h2><?php echo JText::_('COM_JKIT_INSTALL'); ?></h2>
        <table class="adminlist table table-striped" width="100%">
            <thead>
                <tr>
                    <th colspan="2"><?php echo JText::_('COM_JKIT_INSTALL_EXTENSION'); ?></th>
                    <th width="30%"><?php echo JText::_('COM_JKIT_INSTALL_STATUS'); ?></th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <td colspan="3"></td>
                </tr>
            </tfoot>
            <tbody>
                <tr>
                    <td colspan="2"><?php echo JText::_('COM_JKIT_INSTALL_COMPONENT'); ?></td>
                    <td><strong><?php echo JText::_('COM_JKIT_INSTALL_INSTALLED'); ?></strong></td>
                </tr>
                <?php if (count($status->modules)): ?>
                    <tr>
                        <th><?php echo JText::_('COM_JKIT_INSTALL_MODULE'); ?></th>
                        <th><?php echo JText::_('COM_JKIT_INSTALL_CLIENT'); ?></th>
                        <th></th>
                    </tr>
                    <?php foreach ($status->modules as $module): ?>
                        <tr>
                            <td><?php echo $module['name']; ?></td>
                            <td><?php echo ucfirst($module['client']); ?></td>
                            <td><strong><?php echo ($module['result']) ? JText::_('COM_JKIT_INSTALL_INSTALLED') : JText::_('COM_JKIT_INSTALL_NOT_INSTALLED'); ?></strong></td>
                        </tr>
                    <?php endforeach; ?>
                <?php endif; ?>
                <?php if (count($status->plugins)): ?>
                    <tr>
                        <th><?php echo JText::_('COM_JKIT_INSTALL_PLUGIN'); ?></th>
                        <th><?php echo JText::_('COM_JKIT_INSTALL_GROUP'); ?></th>
                        <th></th>
                    </tr>
                    <?php foreach ($status->plugins as $plugin): ?>
                        <tr>
                            <td><?php echo $plugin['name']; ?></td>
                            <td><?php echo ucfirst($plugin['group']); ?></td>
                            <td><strong><?php echo ($plugin['result']) ? JText::_('COM_JKIT_INSTALL_INSTALLED') : JText::_('COM_JKIT_INSTALL_NOT_INSTALLED'); ?></strong></td>
                        </tr>
                    <?php endforeach; ?>
                <?php endif; ?>
            </tbody>
        </table>

        <?php
    }

    /**
     * Uninstall
     */
    function uninstall($parent) {

        // Uninstall subextensions
        $status = $this->_uninstallSubextensions($parent);

        // render html
        ?>
        <h1><?php echo JText::_('COM_JKIT'); ?></h1>
        <h3><?php echo JText::_('COM_JKIT_INSTALL_UNINSTALL'); ?></h3>
        <table class="adminlist table table-striped" width="100%">
            <thead>
                <tr>
                    <th colspan="2"><?php echo JText::_('COM_JKIT_INSTALL_EXTENSION'); ?></th>
                    <th width="30%"><?php echo JText::_('COM_JKIT_INSTALL_STATUS'); ?></th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <td colspan="3"></td>
                </tr>
            </tfoot>
            <tbody>
                <tr>
                    <td colspan="2"><?php echo JText::_('COM_JKIT_INSTALL_COMPONENT'); ?></td>
                    <td><strong><?php echo JText::_('COM_JKIT_INSTALL_UNINSTALLED'); ?></strong></td>
                </tr>
                <?php if (count($status->modules)): ?>
                    <tr>
                        <th><?php echo JText::_('COM_JKIT_INSTALL_MODULE'); ?></th>
                        <th><?php echo JText::_('COM_JKIT_INSTALL_CLIENT'); ?></th>
                        <th></th>
                    </tr>
                    <?php foreach ($status->modules as $module): ?>
                        <tr>
                            <td><?php echo $module['name']; ?></td>
                            <td><?php echo ucfirst($module['client']); ?></td>
                            <td><strong><?php echo ($module['result']) ? JText::_('COM_JKIT_INSTALL_UNINSTALLED') : JText::_('COM_JKIT_INSTALL_NOT_UNINSTALLED'); ?></strong></td>
                        </tr>
                    <?php endforeach; ?>
                <?php endif; ?>
                <?php if (count($status->plugins)): ?>
                    <tr>
                        <th><?php echo JText::_('COM_JKIT_INSTALL_PLUGIN'); ?></th>
                        <th><?php echo JText::_('COM_JKIT_INSTALL_GROUP'); ?></th>
                        <th></th>
                    </tr>
                    <?php foreach ($status->plugins as $plugin): ?>
                        <tr>
                            <td><?php echo $plugin['name']; ?></td>
                            <td><?php echo ucfirst($plugin['group']); ?></td>
                            <td><strong><?php echo ($plugin['result']) ? JText::_('COM_JKIT_INSTALL_UNINSTALLED') : JText::_('COM_JKIT_INSTALL_NOT_UNINSTALLED'); ?></strong></td>
                        </tr>
                    <?php endforeach; ?>
                <?php endif; ?>
            </tbody>
        </table>
        <?php
    }

    /**
     * Installs subextensions (modules, plugins) bundled with the main extension
     *
     * @param JInstaller $parent
     *
     * @return JObject The subextension installation status
     */
    private function _installSubextensions($parent) {

        $src = $parent->getParent()->getPath('source');

        $db = JFactory::getDbo();

        $status = new JObject();
        $status->modules = array();
        $status->plugins = array();

        // Modules installation
        if (count($this->installation_queue['modules'])) {
            foreach ($this->installation_queue['modules'] as $folder => $modules) {
                if (count($modules)) {
                    foreach ($modules as $module => $modulePreferences) {
                        // Install the module
                        if (empty($folder)) {
                            $folder = 'site';
                        }

                        $path = "$src/modules/$folder/$module";

                        if (!is_dir($path)) {
                            $path = "$src/modules/$folder/mod_$module";
                        }

                        if (!is_dir($path)) {
                            $path = "$src/modules/$module";
                        }

                        if (!is_dir($path)) {
                            $path = "$src/modules/mod_$module";
                        }

                        if (!is_dir($path)) {
                            continue;
                        }

                        // Was the module already installed?
                        $sql = $db->getQuery(true)
                                ->select('COUNT(*)')
                                ->from('#__modules')
                                ->where($db->qn('module') . ' = ' . $db->q('mod_' . $module));
                        $db->setQuery($sql);

                        try {
                            $count = $db->loadResult();
                        } catch (Exception $exc) {
                            $count = 0;
                        }

                        $installer = new JInstaller;
                        $result = $installer->install($path);
                        $status->modules[] = array(
                            'name' => 'mod_' . $module,
                            'client' => $folder,
                            'result' => $result
                        );

                        // Modify where it's published and its published state
                        if (!$count) {
                            // A. Position and state
                            list($modulePosition, $modulePublished) = $modulePreferences;

                            if ($modulePosition == 'cpanel') {
                                $modulePosition = 'icon';
                            }

                            $sql = $db->getQuery(true)
                                    ->update($db->qn('#__modules'))
                                    ->set($db->qn('position') . ' = ' . $db->q($modulePosition))
                                    ->where($db->qn('module') . ' = ' . $db->q('mod_' . $module));

                            if ($modulePublished) {
                                $sql->set($db->qn('published') . ' = ' . $db->q('1'));
                            }

                            $db->setQuery($sql);

                            try {
                                $db->execute();
                            } catch (Exception $exc) {
                                // Nothing
                            }

                            // B. Change the ordering of back-end modules to 1 + max ordering
                            if ($folder == 'admin') {
                                try {
                                    $query = $db->getQuery(true);
                                    $query->select('MAX(' . $db->qn('ordering') . ')')
                                            ->from($db->qn('#__modules'))
                                            ->where($db->qn('position') . '=' . $db->q($modulePosition));
                                    $db->setQuery($query);
                                    $position = $db->loadResult();
                                    $position++;

                                    $query = $db->getQuery(true);
                                    $query->update($db->qn('#__modules'))
                                            ->set($db->qn('ordering') . ' = ' . $db->q($position))
                                            ->where($db->qn('module') . ' = ' . $db->q('mod_' . $module));
                                    $db->setQuery($query);
                                    $db->execute();
                                } catch (Exception $exc) {
                                    // Nothing
                                }
                            }

                            // C. Link to all pages
                            try {
                                $query = $db->getQuery(true);
                                $query->select('id')->from($db->qn('#__modules'))
                                        ->where($db->qn('module') . ' = ' . $db->q('mod_' . $module));
                                $db->setQuery($query);
                                $moduleid = $db->loadResult();

                                $query = $db->getQuery(true);
                                $query->select('*')->from($db->qn('#__modules_menu'))
                                        ->where($db->qn('moduleid') . ' = ' . $db->q($moduleid));
                                $db->setQuery($query);
                                $assignments = $db->loadObjectList();
                                $isAssigned = !empty($assignments);
                                if (!$isAssigned) {
                                    $o = (object) array(
                                                'moduleid' => $moduleid,
                                                'menuid' => 0
                                    );
                                    $db->insertObject('#__modules_menu', $o);
                                }
                            } catch (Exception $exc) {
                                // Nothing
                            }
                        }
                    }
                }
            }
        }

        // Plugins installation
        if (count($this->installation_queue['plugins'])) {
            foreach ($this->installation_queue['plugins'] as $folder => $plugins) {
                if (count($plugins)) {
                    foreach ($plugins as $plugin => $published) {
                        $path = "$src/plugins/$folder/$plugin";

                        if (!is_dir($path)) {
                            $path = "$src/plugins/$folder/plg_$plugin";
                        }

                        if (!is_dir($path)) {
                            $path = "$src/plugins/$plugin";
                        }

                        if (!is_dir($path)) {
                            $path = "$src/plugins/plg_$plugin";
                        }

                        if (!is_dir($path)) {
                            continue;
                        }

                        // Was the plugin already installed?
                        $query = $db->getQuery(true)
                                ->select('COUNT(*)')
                                ->from($db->qn('#__extensions'))
                                ->where($db->qn('element') . ' = ' . $db->q($plugin))
                                ->where($db->qn('folder') . ' = ' . $db->q($folder));
                        $db->setQuery($query);

                        try {
                            $count = $db->loadResult();
                        } catch (Exception $exc) {
                            $count = 0;
                        }

                        $installer = new JInstaller;
                        $result = $installer->install($path);

                        $status->plugins[] = array('name' => 'plg_' . $plugin, 'group' => $folder, 'result' => $result);

                        if ($published && !$count) {
                            $query = $db->getQuery(true)
                                    ->update($db->qn('#__extensions'))
                                    ->set($db->qn('enabled') . ' = ' . $db->q('1'))
                                    ->where($db->qn('element') . ' = ' . $db->q($plugin))
                                    ->where($db->qn('folder') . ' = ' . $db->q($folder));
                            $db->setQuery($query);

                            try {
                                $db->execute();
                            } catch (Exception $exc) {
                                // Nothing
                            }
                        }
                    }
                }
            }
        }

        return $status;
    }

    /**
     * Uninstalls subextensions (modules, plugins) bundled with the main extension
     *
     * @param JInstaller $parent
     *
     * @return JObject The subextension uninstallation status
     */
    private function _uninstallSubextensions($parent) {

        JLoader::import('joomla.installer.installer');

        $db = JFactory::getDBO();

        $status = new JObject();
        $status->modules = array();
        $status->plugins = array();

        $src = $parent->getParent()->getPath('source');

        // Modules uninstallation
        if (count($this->installation_queue['modules'])) {
            foreach ($this->installation_queue['modules'] as $folder => $modules) {
                if (count($modules)) {
                    foreach ($modules as $module => $modulePreferences) {
                        // Find the module ID
                        $sql = $db->getQuery(true)
                                ->select($db->qn('extension_id'))
                                ->from($db->qn('#__extensions'))
                                ->where($db->qn('element') . ' = ' . $db->q('mod_' . $module))
                                ->where($db->qn('type') . ' = ' . $db->q('module'));
                        $db->setQuery($sql);

                        try {
                            $id = $db->loadResult();
                        } catch (Exception $exc) {
                            $id = 0;
                        }

                        // Uninstall the module
                        if ($id) {
                            $installer = new JInstaller;
                            $result = $installer->uninstall('module', $id, 1);
                            $status->modules[] = array(
                                'name' => 'mod_' . $module,
                                'client' => $folder,
                                'result' => $result
                            );
                        }
                    }
                }
            }
        }

        // Plugins uninstallation
        if (count($this->installation_queue['plugins'])) {
            foreach ($this->installation_queue['plugins'] as $folder => $plugins) {
                if (count($plugins)) {
                    foreach ($plugins as $plugin => $published) {
                        $sql = $db->getQuery(true)
                                ->select($db->qn('extension_id'))
                                ->from($db->qn('#__extensions'))
                                ->where($db->qn('type') . ' = ' . $db->q('plugin'))
                                ->where($db->qn('element') . ' = ' . $db->q($plugin))
                                ->where($db->qn('folder') . ' = ' . $db->q($folder));
                        $db->setQuery($sql);

                        try {
                            $id = $db->loadResult();
                        } catch (Exception $exc) {
                            $id = 0;
                        }

                        if ($id) {
                            $installer = new JInstaller;
                            $result = $installer->uninstall('plugin', $id, 1);
                            $status->plugins[] = array(
                                'name' => 'plg_' . $plugin,
                                'group' => $folder,
                                'result' => $result
                            );
                        }
                    }
                }
            }
        }

        return $status;
    }

}
